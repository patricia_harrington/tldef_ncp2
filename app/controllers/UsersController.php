<?php
/**
 * Filename: UsersController.php
 * Author: Patricia Harrington
 * Created: 1/8/13 12:26 PM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */
class UsersController extends BaseController {

    public function getIndex() {
        return $this->getList();
    }

    public function getList() {
        $results = array();
        $users = User::all();
        foreach ($users as $user) {
            $record = array();
            $record['id'] = $user->id;
            $record['name'] = trim($user->username);
            $results[] = $record;
        }
        return Response::json(array('users' => $results));
    }

    public function getId($id) {
        $result = null;
        $record = User::find($id);
        if (!is_null($record)) {
            $result = $record->toArray();
        }
        return Response::json(array('user' => $result));
    }

    public function postDelete() {
        $status = 0;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $id = $input['id'];
            $record = User::find($id);
            if (!is_null($record)) {
                $status = $record->delete();
            }
        }
        return Response::json(array('status' => $status));
    }

    public function postUpdate() {
        $result = null;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $id = $input['id'];
            $record = User::find($id);
            if (!is_null($record)) {
                if ($input['password']) {
                    $input['password'] = Hash::make($input['password']);
                }
                if ($record->update($input) != 0) {
                    $record = User::find($id);
                    $result = $record->toArray();
                }
            }
        }
        return Response::json(array('user' => $result));
    }

    public function postAdd() {
        $result = null;
        $input = Input::get();
        $user = new User();
        if ($input['password']) {
            $input['password'] = Hash::make($input['password']);
        }
        $record = $user->create($input);
        if ($record) {
            $result = $record->toArray();
            $result['created_at'] = $record->created_at->format('Y/m/d H:i:s');
            $result['updated_at'] = $record->updated_at->format('Y/m/d H:i:s');
        }
        return Response::json(array('user' => $result));
    }

}