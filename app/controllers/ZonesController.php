<?php
/**
 * Filename: ZonesController.php
 * Author: Patricia Harrington
 * Created: 8/14/13 4:49 PM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */

class ZonesController extends BaseController {

    public function getIndex() {
        return $this->getList();
    }

    public function getList() {
        $result = array();
        $zones = Zone::where('active', '=', 1)->orderBy('name')->get();
        foreach ($zones as $zone) {
            $record = array();
            $record['id'] = $zone->id;
            $record['name'] = $zone->name;
            $record['state_code'] = $zone->state_code;
            $result[] = $record;
        }
        return Response::json(array('zones' => $result));
    }

}