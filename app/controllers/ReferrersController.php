<?php
/**
 * Filename: ReferrersController.php
 * Author: Patricia Harrington
 * Created: 12/21/12 2:32 PM
 * Copyright 2012 Transgender Legal Defense & Education Fund, Inc.
 */
class ReferrersController extends BaseController {

    public function getIndex() {
        return $this->getList();
    }

    public function getList() {
        $results = array();
        $referrers = Referrer::all();
        foreach ($referrers as $referrer) {
            $record = array();
            $record['id'] = $referrer->id;
            $record['name'] = trim($referrer->name);
            $results[] = $record;
        }
        return Response::json(array('referrers' => $results));
    }

    public function getSortedList() {
        $results = array();
        $referrers = Referrer::orderBy('name', 'asc')->get();
        foreach ($referrers as $referrer) {
            $record = array();
            $record['id'] = $referrer->id;
            $record['name'] = trim($referrer->name);
            $results[] = $record;
        }
        return Response::json(array('referrers' => $results));
    }

    public function getId($id) {
        $result = null;
        $record = Referrer::find($id);
        if (!is_null($record)) {
            $result = $record->toArray();
        }
        return Response::json(array('referrer' => $result));
    }

    public function postDelete() {
        $status = 0;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $record = Referrer::find($input['id']);
            if (!is_null($record)) {
                $status = $record->delete();
            }
        }
        return Response::json(array('status' => $status));
    }

    public function postUpdate() {
        $result = null;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $id = $input['id'];
            $record = Referrer::find($id);
            if (!is_null($record)) {
                if ($record->update($input) != 0) {
                    $record = Referrer::find($id);
                    $result = $record->toArray();
                }
            }
        }
        return Response::json(array('referrer' => $result));
    }

    public function postAdd() {
        $result = null;
        $input = Input::get();
        $referrer = new Referrer();
        $record = $referrer->create($input);
        if ($record) {
            $result = $record->toArray();
            $result['created_at'] = $record->created_at->format('Y/m/d H:i:s');
            $result['updated_at'] = $record->updated_at->format('Y/m/d H:i:s');
        }
        return Response::json(array('referrer' => $result));
    }

}
