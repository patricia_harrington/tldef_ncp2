<?php
/**
 * Filename: AssignmentsController.php
 * Author: Patricia Harrington
 * Created: 1/5/13 11:10 PM
 * Copyright 2012 Transgender Legal Defense & Education Fund, Inc.
 */

class AssignmentsController extends BaseController {

    public function postAdd() {
        $status = 0;
        $input = Input::get();
        if (array_key_exists('client_id', $input) and array_key_exists('attorney_id', $input)) {
            $client = Client::find($input['client_id']);
            if (!is_null($client)) {
                $status = $client->attorney()->attach($input['attorney_id']);
            }
        }
        return Response::json(array('status' => $status));
    }

    public function postDelete() {
        $status = 0;
        $input = Input::get();
        if (array_key_exists('client_id', $input) and array_key_exists('attorney_id', $input)) {
            $client = Client::find($input['client_id']);
            if (!is_null($client)) {
                $status = $client->attorney()->detach($input['attorney_id']);
            }
        }
        return Response::json(array('status' => $status));
    }

    public function postUpdate() {
        $status = 0;
        $input = Input::get();
        if (array_key_exists('client_id', $input)) {
            $client = Client::find($input['client_id']);
            if (!is_null($client)) {
                if (array_key_exists('add', $input)) {
                    foreach ($input['add'] as $id) {
                        $status += $client->attorney()->attach($id);
                    }
                }
                if (array_key_exists('remove', $input)) {
                    foreach ($input['remove'] as $id) {
                        $status += $client->attorney()->detach($id);
                    }
                }
            }
        }
        return Response::json(array('status' => $status));
    }

}