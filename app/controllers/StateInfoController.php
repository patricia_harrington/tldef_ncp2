<?php
/**
 * Filename: StateInfoController.php
 * Author: Patricia Harrington
 * Created: 5/31/13 12:07 AM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */

class StateInfoController  extends BaseController {

    public function getIndex() {
        return $this->getList();
    }

    public function getList() {
        $result = array();
        $states = StateName::all();
        foreach ($states as $state) {
            $record = array();
            $record['state_code'] = $state->state_code;
            $record['state_name'] = $state->state_name;
            $result[] = $record;
        }
        return Response::json(array('states' => $result));
    }

    public function getCounties($state_code) {
        $result = array();
        $counties = StateCounty::where('state_code','=',$state_code)->get(array('county_name'));
        foreach ($counties as $county) {
            $result[] = $county->county_name;
        }
        return Response::json(array('counties' => $result));
    }

    public function getZones($state_code = 'NY', $county_name = 'New York County') {
        $result = array();
        $items = StateCounty::where('state_code', '=', $state_code)->where('county_name', '=', $county_name)->get(array('zone_id'));
        foreach ($items as $item) {
            $zone = Zone::where('id','=', $item->zone_id)->first();
            $result['id'] = $zone['id'];
            $result['name'] = $zone['name'];
        }
        return Response::json(array('zones' => $result));
    }

    public function getCountiesForZone($zone_id) {
        $result = array();
        $result[] = 'All';
        $counties = StateCounty::where('zone_id','=',$zone_id)->get(array('county_name'));
        foreach ($counties as $county) {
            $result[] = $county->county_name;
        }
        Log::info(Response::json(array('counties' => $result)));
        return Response::json(array('counties' => $result));
    }

}