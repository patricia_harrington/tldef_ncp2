<?php
/**
 * Filename: AgreementController.php
 * Author: Patricia Harrington
 * Created: 11/12/13 5:12 PM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */

class AgreementController  extends BaseController {

    public function getEmail($id) {
        $status = 0;
        $client = Client::find($id);
        if (!is_null($client) && !is_null($client->email)) {
            $data['guid'] = Client::guid();
            $data['client_id'] = $client->id;
            $data['date_sent'] = date('Y-m-d H:i:s');
            $agreement = new Agreement($data);
            if ($agreement) {
                $client->clientAgreement()->save($agreement);
                $inputs = array();
                $inputs['address'] = $client->email;
                $inputs['name'] = $client->name();
                $inputs['subject'] = 'TLDEF Client Agreement';
                $inputs['guid'] = $agreement->guid;
                $inputs['first_name'] = $client->preferred_first_name;
                $inputs['interviewer'] = $client->interviewer;
                $inputs['reg_server'] = 'reg.transgenderlegal' . substr ( $_SERVER['SERVER_NAME'] , strrpos ( $_SERVER['SERVER_NAME'] , '.') );
                Mail::send('emails.agreement', $inputs, function($message) use($inputs)
                {
                    $message->to($inputs['address'], $inputs['name'])->subject($inputs['subject']);
                });
                $status = 1;
            }
        }
        return Response::json(array('status' => $status));
    }

} 