<?php
/**
 * Filename: StateDatabaseController.php
 * Author: Patricia Harrington
 * Created: 9/12/13 3:18 PM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */

class StateDataBaseController extends BaseController {


    public function getIndex() {

    }

    // State documentation functions

    public function postStateDocumentation() {
        $status = 0;
        $data = array();
        $input = Input::get();
        if (array_key_exists('all', $input)) {
            $states = State::all();
            if (!is_null($states)) {
                foreach ($states as $state) {
                    $data[] = $state->getData();
                    $status++;
                }
            }
        } elseif (array_key_exists('ids', $input)) {
            foreach ($input['ids'] as $id) {
                $state = State::find($id);
                if (!is_null($state)) {
                    $data[] = $state->getData();
                    $status++;
                }
            }
        }
        $result = array('status' => $status, 'data' => $data);
        return Response::json($result);
    }

    public function postUpdateStateDocumentation() {
        $status = 0;
        $data = array();
        $input = Input::get();
        if (array_key_exists('statedata', $input)) {
            foreach ($input['statedata'] as $statedata) {
                if (array_key_exists('id', $statedata)) {
                    $state = State::find($statedata['id']);
                    if (!is_null($state)) {
                        $state->addData($statedata);
                        $data[] = $state->getData();
                        $status++;
                    }
                }
            }
        }
        $result = array('status' => $status, 'data' => $data);
        return Response::json($result);
    }

    // State Editor operations

    public function getId($id) {
        $response = Response::json(array('id' => 0));
        if (!is_null($id)) {
            $state = State::find($id);
            if (!is_null($state)) {
                $response = Response::json($state);
            }
        }
        return $response;
    }

    public function postUpdate() {
        $timestamp = null;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $state = State::find($input['id']);
            if (!is_null($state)) {
                $new = $state->addData($input);
                $timestamp = $new->updated_at;
            }
        }
        return Response::json(array('timestamp' => $timestamp));
    }

    public function getHtml ($id) {
        $str = '';
        $state = State::find($id);
        if (!is_null($state)) {
            $str = $state->getHtml(null);
        }
        return Response::json(array('html' => $str, 'id' => $id ));
    }

    public function getSection ($id, $selector) {
        $str = '';
        $state = State::find($id);
        if (!is_null($state)) {
            $str = $state->getHtml($selector);
        }
        return Response::json(array('html' => $str, 'id' => $id, 'selector' => $selector));
    }

    public function getHtmlString ($id) {
        $str = '';
        $state = State::find($id);
        if (!is_null($state)) {
            $str = $state->getHtml(null);
        }
        return $str;
    }

    public function getSections($selector) {
        $str = '';
        $states = State::all();
        if (!is_null($states)) {
            foreach ($states as $state) {
                $state_name = $state->state_name;
                $img_file_name = str_ireplace(" ", "_", strtolower($state_name));
                $str .= "<div class='state_header'>\n";
                $str .= "<h1><img class='state_seal' src='http://namechange.transgenderlegal.dev/img/states/" . $img_file_name . ".png' height='120' width='120' alt='" . $state_name . " state seal' /> "  . $state_name . "</h1>\n";
                $str .= "</div>\n";
                $str .= $state->getHtmlSection($selector);
            }
        }
        return Response::json(array('html' => $str));
    }

}