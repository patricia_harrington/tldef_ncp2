<?php
/**
 * Filename: JudgesController.php
 * Author: Patricia Harrington
 * Created: 2/13/14 11:20 AM
 * Copyright 2014 Transgender Legal Defense & Education Fund, Inc.
 */

class JudgesController extends BaseController {

    public function getIndex() {
        return $this->getList();
    }

    public function getList() {
        $results = array();
        $judges = Judge::all();
        foreach ($judges as $judge) {
            $record = array();
            $record['id'] = $judge->id;
            $record['name'] = trim($judge->name);
            $results[] = $record;
        }
        return Response::json(array('judges' => $results));
    }

    public function getSortedList() {
        $results = array();
        $judges = Judge::orderBy('name', 'asc')->get();
        foreach ($judges as $judge) {
            $record = array();
            $record['id'] = $judge->id;
            $record['name'] = trim($judge->name);
            $results[] = $record;
        }
        return Response::json(array('judges' => $results));
    }

    public function getId($id) {
        $result = null;
        $record = Judge::find($id);
        if (!is_null($record)) {
            $result = $record->toArray();
        }
        return Response::json(array('judge' => $result));
    }

    public function postDelete() {
        $status = 0;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $record = Judge::find($input['id']);
            if (!is_null($record)) {
                $status = $record->delete();
            }
        }
        return Response::json(array('status' => $status));
    }

    public function postUpdate() {
        $result = null;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $id = $input['id'];
            $record = Judge::find($id);
            if (!is_null($record)) {
                if ($record->update($input) != 0) {
                    $record = judge::find($id);
                    $result = $record->toArray();
                }
            }
        }
        return Response::json(array('judge' => $result));
    }

    public function postAdd() {
        $result = null;
        $input = Input::get();
        $judge = new Judge();
        $record = $judge->create($input);
        if ($record) {
            $result = $record->toArray();
            $result['created_at'] = $record->created_at->format('Y/m/d H:i:s');
            $result['updated_at'] = $record->updated_at->format('Y/m/d H:i:s');
        }
        return Response::json(array('judge' => $result));
    }


} 