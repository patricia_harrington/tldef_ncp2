<?php
/**
 * Filename: ClientsController.php
 * Author: Patricia Harrington
 * Created: 12/20/12 2:04 PM
 * Copyright 2012 Transgender Legal Defense & Education Fund, Inc.
 */

class ClientsController extends BaseController {

    public function getIndex() {
        return $this->getList();
    }

    public function getSince($since) {
        return $this->getListData($since);
    }

    public function getList() {
        return $this->getListData(null);
    }

    private function getListData($since) {
        $result = array();
        $fields = array
        (   'id',
            'preferred_first_name',
            'preferred_middle_name',
            'preferred_last_name',
            'legal_first_name',
            'legal_last_name',
            'phone1',
            'phone2',
            'is_closed',
            'updated_at'
        );
        if (is_null($since)) {
            $clients = Client::orderBy('updated_at','desc')->get($fields);
        } else {
            $clients = Client::orderBy('updated_at','desc')->where('updated_at','>=',$since)->get($fields);
        }
        foreach ($clients as $client) {
            $result[] = $this->getClientFields($client);
        }
        return Response::json(array('clients' => $result));
    }

    public function getList2() {
        $result = array();
        $fields = array
        (   'id',
            'preferred_first_name',
            'preferred_middle_name',
            'preferred_last_name',
            'place_of_birth',
            'has_medicaid',
            'county',
            'state',
            'referrer_id',
            'updated_at'
        );
        $clients = Client::orderBy('updated_at','desc')->get($fields);
        foreach ($clients as $client) {
            $record = array();
            $record['id'] = $client->id;
            $last_name = trim($client->preferred_last_name);
            $first_name = trim($client->preferred_first_name);
            $middle_name = trim($client->preferred_middle_name);
            $record['preferred_name'] = trim($last_name . ', ' . $first_name . ' ' . $middle_name);
            $record['has_medicaid'] = $client->has_medicaid;
            $record['county'] = $client->county;
            $record['state'] = $client->state;
            $record['place_of_birth'] = $client->place_of_birth;
            $record['referrer'] = '';
            if (! is_null($client->referrer_id) ) {
                $referrer = Referrer::find($client->referrer_id, array('name'));
                if (!is_null($referrer)) {
                    $record['referrer'] = trim($referrer->name);
                }
            }
            $record['updated_at'] = $client->updated_at;
            $result[] = $record;
        }
        return Response::json(array('clients' => $result));
    }

    public function getCaseList() {
        $result = array();
        $cases = ClientCase::orderBy('date_of_hearing','desc')->get();
        $client_fields = array('preferred_last_name','preferred_first_name','preferred_middle_name');
        foreach ($cases as $case) {
            $client = $case->client()->first($client_fields);
            $record['id'] = $case->client_id;
            $last_name = trim($client->preferred_last_name);
            $first_name = trim($client->preferred_first_name);
            $middle_name = trim($client->preferred_middle_name);
            $record['preferred_name'] = trim($last_name . ', ' . $first_name . ' ' . $middle_name);
            $record['date_of_filing'] = $case->date_of_filing;
            $record['court_index_number'] = $case->court_index_number;
            $record['date_of_hearing'] = $case->date_of_hearing;
            $record['judge'] = $case->judge;
            $record['notes_hearing'] = $case->notes_hearing;
            $record['updated_at'] = $case->updated_at;
            $result[] = $record;
        }
        return Response::json(array('clients' => $result));
    }

    private function getZoneDataFor($zone_id) {
        $result = array();
        $result['zone_id'] = $zone_id;
        $zone = Zone::where('id', '=', $zone_id)->first();
        $result['state_code'] = $zone->state_code;
        $counties = StateCounty::where('zone_id', '=', $zone_id)->get();
        $county_names = array();
        foreach ($counties as $county) {
            $county_names[] = $county->county_name;
        }
        $result['county_names'] = $county_names;
        return $result;
    }

    public function getIntakeList($zone) {
        $result = array();
        $fields = array
        (
            'id',
            'preferred_first_name',
            'preferred_middle_name',
            'preferred_last_name',
            'legal_first_name',
            'legal_last_name',
            'phone1',
            'phone2',
            'date_intake_scheduled',
            'updated_at'
        );
        if ($zone == 0) {
            $clients = Client::where('is_accepted', '=', 'Yes')
                ->where('is_closed', '=', 'No')
                ->whereNull('date_intake_completed')
                ->orderBy('updated_at', 'asc')
                ->get($fields);
        } else {
            $zone = $this->getZoneDataFor($zone);
            $clients = Client::where('is_accepted', '=', 'Yes')
                ->where('is_closed', '=', 'No')
                ->where('state', '=', $zone['state_code'])
                ->whereIn('county', $zone['county_names'])
                ->whereNull('date_intake_completed')
                ->orderBy('updated_at', 'asc')
                ->get($fields);
        }
        foreach ($clients as $client) {
            $record = array();
            $record['id'] = $client->id;
            $last_name = trim($client->preferred_last_name);
            $first_name = trim($client->preferred_first_name);
            $middle_name = trim($client->preferred_middle_name);
            $record['preferred_name'] = trim($last_name . ', ' . $first_name . ' ' . $middle_name);
            $last_name = trim($client->legal_last_name);
            $first_name = trim($client->legal_first_name);
            if ($first_name == '' and $last_name == '') {
                $record['legal_name'] = '';
            } else {
                $record['legal_name'] = $last_name . ', ' . $first_name;
            }
            $record['phone1'] = $client->phone1;
            $record['phone2'] = $client->phone2;
            $record['date_intake_scheduled'] = $client->date_intake_scheduled;
            $record['updated_at'] = $client->updated_at;
            $result[] = $record;
        }
        return Response::json(array('clients' => $result));
    }

    public function getWaiting($zone,$county,$hold) {
        $result = array();
        $fields = array
        (
            'id',
            'preferred_first_name',
            'preferred_middle_name',
            'preferred_last_name',
            'legal_first_name',
            'legal_last_name',
            'phone1',
            'phone2',
            'date_intake_completed',
            'county',
            'state',
            'has_birth_certificate',
            'payment_ability',
            'updated_at'
        );
        if ($zone == 0) {
            $clients = Client::where('is_accepted', '=', 'Yes')
                                ->where('is_closed', '=', 'No')
                                ->whereNotNull('date_intake_completed')
                                ->where('on_hold', '=', $hold)
                                ->whereNull('date_of_assignment')
                                ->orderBy('date_intake_completed', 'asc')
                                ->get($fields);
        } else {
            $zone = $this->getZoneDataFor($zone);
            if ($county == 'All') {
                $clients = Client::where('is_accepted', '=', 'Yes')
                    ->where('is_closed', '=', 'No')
                    ->where('state', '=', $zone['state_code'])
                    ->whereIn('county', $zone['county_names'])
                    ->whereNotNull('date_intake_completed')
                    ->where('on_hold', '=', $hold)
                    ->whereNull('date_of_assignment')
                    ->orderBy('date_intake_completed', 'asc')
                    ->get($fields);
            } else {
                $clients = Client::where('is_accepted', '=', 'Yes')
                    ->where('is_closed', '=', 'No')
                    ->where('state', '=', $zone['state_code'])
                    ->where('county', '=', $county)
                    ->whereNotNull('date_intake_completed')
                    ->where('on_hold', '=', $hold)
                    ->whereNull('date_of_assignment')
                    ->orderBy('date_intake_completed', 'asc')
                    ->get($fields);
            }
        }
        $index = 0;
        foreach ($clients as $client) {
            $record = array();
            $index = $index + 1;
            $record['index'] = $index;
            $record['id'] = $client->id;
            $last_name = trim($client->preferred_last_name);
            $first_name = trim($client->preferred_first_name);
            $middle_name = trim($client->preferred_middle_name);
            $record['preferred_name'] = trim($last_name . ', ' . $first_name . ' ' . $middle_name);
            $last_name = trim($client->legal_last_name);
            $first_name = trim($client->legal_first_name);
            if ($first_name == '' and $last_name == '') {
                $record['legal_name'] = '';
            } else {
                $record['legal_name'] = $last_name . ', ' . $first_name;
            }
            $record['phone1'] = $client->phone1;
            $record['phone2'] = $client->phone2;
            $record['date_intake_completed'] = $client->date_intake_completed;
            $record['county'] = $client->county;
            $record['state'] = $client->state;
            $record['has_birth_certificate'] = $client->has_birth_certificate;
            $record['payment_ability'] = $client->payment_ability;
            $record['updated_at'] = $client->updated_at;
            $result[] = $record;
        }
        return Response::json(array('clients' => $result));
    }

    public function getInProgress($zone_id,$firm_id) {
        $fields = array
        (
            't1.id',
            't1.preferred_first_name',
            't1.preferred_middle_name',
            't1.preferred_last_name',
            't1.legal_first_name',
            't1.legal_last_name',
            't1.phone1',
            't1.phone2',
            't1.date_intake_completed',
            't1.date_of_assignment',
            't1.updated_at',
            't4.date_of_filing',
            't4.date_of_hearing'
        );
        $result = array();
        $query = 'select distinct ' . implode($fields,', ') . ' from clients as t1 join client_firm as t2 on t1.id = t2.client_id join firms as t3 on t2.firm_id = t3.id ';
        $query .= 'left join client_cases as t4 on t1.id = t4.client_id ';
        $query .= 'where is_closed = "No" and date_intake_completed is not null and date_of_assignment is not null ';
        if ( $zone_id != 0 ) {
            $zone = $this->getZoneDataFor($zone_id);
            $query .= 'and state = "' . $zone['state_code'] . '" and county in ("' . implode($zone['county_names'], '", "') . '") ';
        }
        if ( $firm_id != 0 ) {
            $query .= 'and t3.id = ' . $firm_id . ' ';
        }
        $query .= 'order by updated_at asc;';
        // Log::info($query);
        $clients = DB::select($query);
        foreach ($clients as $client) {
            $record = array();
            $record['id'] = $client->id;
            $last_name = trim($client->preferred_last_name);
            $first_name = trim($client->preferred_first_name);
            $middle_name = trim($client->preferred_middle_name);
            $record['preferred_name'] = trim($last_name . ', ' . $first_name . ' ' . $middle_name);
            $last_name = trim($client->legal_last_name);
            $first_name = trim($client->legal_first_name);
            if ($first_name == '' and $last_name == '') {
                $record['legal_name'] = '';
            } else {
                $record['legal_name'] = $last_name . ', ' . $first_name;
            }
            $record['phone1'] = $client->phone1;
            $record['phone2'] = $client->phone2;
            $record['date_intake_completed'] = $client->date_intake_completed;
            $record['date_of_assignment'] = $client->date_of_assignment;
            $record['date_of_filing'] = $client->date_of_filing;
            $record['date_of_hearing'] = $client->date_of_hearing;
            $record['updated_at'] = $client->updated_at;
            $result[] = $record;
        }
        return Response::json(array('clients' => $result));
    }

    private function inZone($client,$zone) {
        $result = false;
        if ($zone['zone_id'] == 0 ) {
            $result = true;
        } else if (($client->state == $zone['state_code']) ) {
            foreach ( $zone['county_names'] as $county_name ) {
                if (strcasecmp($client->county, $county_name) == 0 ) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }

    private function packageForFirm($clients,$zone_id) {
        $result = array();
        $zone = $this->getZoneDataFor($zone_id);
        foreach ($clients as $client) {
            $record = array();
            if ($this->inZone($client,$zone)) {
                $record['id'] = $client->id;
                $last_name = trim($client->preferred_last_name);
                $first_name = trim($client->preferred_first_name);
                $middle_name = trim($client->preferred_middle_name);
                $record['preferred_name'] = trim($last_name . ', ' . $first_name . ' ' . $middle_name);
                $last_name = trim($client->legal_last_name);
                $first_name = trim($client->legal_first_name);
                if ($first_name == '' and $last_name == '') {
                    $record['legal_name'] = '';
                } else {
                    $record['legal_name'] = $last_name . ', ' . $first_name;
                }
                $record['name_change_completed'] = $client->name_change_completed;
                $record['is_closed'] = $client->is_closed;
                $record['date_intake_completed'] = $client->date_intake_completed;
                $record['date_of_assignment'] = $client->date_of_assignment;
                $record['updated_at'] = $client->updated_at;
                $record['attorneys'] = $this->getAttorneysFor($client);
                $result[] = $record;
            }
        }
        return $result;
    }

    public function getForAttorney($attorney_id) {
        $result = array();
        $attorney = Attorney::find($attorney_id);
        if (!is_null($attorney)) {
            $clients = $attorney->client;
            $result = $this->packageForFirm($clients,0);
        }
        return Response::json(array('clients' => $result));
    }

    public function getForFirm($firm_id,$zone_id) {
        $result = array();
        $firm = Firm::find($firm_id);
        if (!is_null($firm)) {
            $clients = $firm->client;
            if (!is_null($clients)) {
                $result = $this->packageForFirm($clients,$zone_id);
            }
        }
        return Response::json(array('clients' => $result));
    }

    public function getClosed($completed,$accepted,$intake,$assigned) {
        $fields = array
        (   'id',
            'preferred_first_name',
            'preferred_middle_name',
            'preferred_last_name',
            'date_intake_completed',
            'date_of_assignment',
            'name_change_completed',
            'is_accepted',
            'reasons_for_rejection',
            'updated_at'
        );
        if ($intake == 'No' and $assigned == 'No') {
            $clients = Client::where('is_closed', '=', 'Yes')
                                ->where('name_change_completed', '=', $completed)
                                ->where('is_accepted', '=', $accepted)
                                ->whereNull('date_intake_completed')
                                ->whereNull('date_of_assignment')
                                ->orderBy('updated_at', 'desc')
                                ->get($fields);
        } else if ($intake == 'No' and $assigned == 'Yes') {
            $clients = Client::where('is_closed', '=', 'Yes')
                                ->where('name_change_completed', '=', $completed)
                                ->where('is_accepted', '=', $accepted)
                                ->whereNull('date_intake_completed')
                                ->whereNotNull('date_of_assignment')
                                ->orderBy('updated_at', 'desc')
                                ->get($fields);
        } else if ($intake == 'Yes' and $assigned == 'No') {
            $clients = Client::where('is_closed', '=', 'Yes')
                                ->where('name_change_completed', '=', $completed)
                                ->where('is_accepted', '=', $accepted)
                                ->whereNotNull('date_intake_completed')
                                ->whereNull('date_of_assignment')
                                ->orderBy('updated_at', 'desc')
                                ->get($fields);
        } else if ($intake == 'Yes' and $assigned == 'Yes') {
            $clients = Client::where('is_closed', '=', 'Yes')
                                ->where('name_change_completed', '=', $completed)
                                ->where('is_accepted', '=', $accepted)
                                ->whereNotNull('date_intake_completed')
                                ->whereNotNull('date_of_assignment')
                                ->orderBy('updated_at', 'desc')
                                ->get($fields);
        } else {
            $clients = Client::where('is_closed', '=', 'Yes')
                                ->orderBy('updated_at', 'desc')
                                ->get($fields);
        }
        $result = array();
        foreach ($clients as $client) {
            $record = array();
            $record['id'] = $client->id;
            $last_name = trim($client->preferred_last_name);
            $first_name = trim($client->preferred_first_name);
            $middle_name = trim($client->preferred_middle_name);
            $record['preferred_name'] = trim($last_name . ', ' . $first_name . ' ' . $middle_name);
            $record['date_intake_completed'] = $client->date_intake_completed;
            $record['date_of_assignment'] = $client->date_of_assignment;
            $record['name_change_completed'] = $client->name_change_completed;
            $record['is_accepted'] = $client->is_accepted;
            $record['reasons_for_rejection'] = $client->reasons_for_rejection;
            $record['updated_at'] = $client->updated_at;
            $result[] = $record;
        }
        return Response::json(array('clients' => $result));
    }
    public function getClosedList() {
        $result = array();
        $fields = array
        (
            'id',
            'preferred_first_name',
            'preferred_middle_name',
            'preferred_last_name',
            'date_intake_completed',
            'date_of_assignment',
            'name_change_completed',
            'is_accepted',
            'reasons_for_rejection',
            'updated_at'
        );
        $clients = Client::where('is_closed', '=', 'Yes')->orderBy('updated_at', 'desc')->get($fields);
        foreach ($clients as $client) {
            $record = array();
            $record['id'] = $client->id;
            $last_name = trim($client->preferred_last_name);
            $first_name = trim($client->preferred_first_name);
            $middle_name = trim($client->preferred_middle_name);
            $record['preferred_name'] = trim($last_name . ', ' . $first_name . ' ' . $middle_name);
            $record['date_intake_completed'] = $client->date_intake_completed;
            $record['date_of_assignment'] = $client->date_of_assignment;
            $record['name_change_completed'] = $client->name_change_completed;
            $record['is_accepted'] = $client->is_accepted;
            $record['reasons_for_rejection'] = $client->reasons_for_rejection;
            $record['updated_at'] = $client->updated_at;
            $result[] = $record;
        }
        return Response::json(array('clients' => $result));
    }

    private function getFirmsFor($client) {
        $records = $client->firm()->get();
        $firms = array();
        foreach ($records as $record) {
            $firm = array();
            $firm['id'] = $record->id;
            $firm['name'] = $record->full_name;
            $firms[] = $firm;
        }
        return $firms;
    }

    private function getAttorneysFor($client) {
        $records = $client->attorney()->get();
        $attorneys = array();
        foreach ($records as $record) {
            $attorney = array();
            $attorney['id'] = $record->id;
            $attorney['name'] = $record->fullName();
            $attorney['phone'] = $record->phone;
            $attorney['email'] = $record->email;
            $attorney['firm_id'] = $record->firm_id;
            $firm = $record->firm()->first();
            $attorney['firm_name'] = $firm->full_name;
            $attorneys[] = $attorney;
        }
        return $attorneys;
    }

    //TODO: Move this to the model
    private function getEthnicityFor($client) {
        $results = '';
        $first = true;
        $records = $client->ethnicity()->get();
        foreach ($records as $record) {
            if ($first) {
                $results .= $record->name;
                $first = false;
            } else {
                $results .= "," . $record->name;
            }
        }

        return $results;
    }

    private function getMailingAddressFor($client) {
        $result = '';
        if (! is_null($client->address_attention)) {
            $result .= $client->address_attention . "\n";
        }
        else {
            $result .= $client->preferred_first_name . ' ' . $client->preferred_last_name . "\n";
        }
        $result .= $client->address_street . "\n";
        $result .= $client->address_city . ', ' . $client->state . ' ' . $client->address_zip_code . "\n";
        return $result;
    }

    private function getAttorneyNamesFor($client) {
        $records = $client->attorney()->get();
        $attorneys = array();
        foreach ($records as $record) {
            $attorneys[] = $record->Name();
        }
        return implode('; ', $attorneys);
    }

    private function getFirmNamesFor($client) {
        $records = $client->firm()->get();
        $firms = array();
        foreach ($records as $record) {
            $firms[] = $record->full_name;
        }
        return implode('; ', $firms);
    }

    private function getFirmNameFor($firm_id) {
        $result = '';
        $firm = Firm::find($firm_id);
        if (!is_null($firm)) {
            $result = $firm->full_name;
        }
        return $result;
    }

    private function calculateAgeOn($birth_date,$on_date) {
        if (is_null($birth_date) || is_null($on_date)) {
            $result = null;
        } else {
            $day1 = new DateTime($birth_date);
            $day2 = new DateTime($on_date);
            $diff = $day2->diff($day1);
            $result = $diff->y;
        }
        return $result;
    }

    private function getCaseFor($client) {
        $result = null;
        $record = $client->clientCase()->first();
        if (!is_null($record)) {
            $result = array();
            $result['case_id'] = $record->case_id;
            $result['date_of_filing'] = $record->date_of_filing;
            $result['court_index_number'] = $record->court_index_number;
            $result['date_of_hearing'] = $record->date_of_hearing;
            $result['judge'] = $record->judge;
            $result['notes_hearing'] = $record->notes_hearing;
        }
        return $result;
    }

    private function getDetailsFor($client) {
        $result = array();
        $records = $client->clientDetail()->get();
        foreach ($records as $record) {
            $detail = array();
            $detail['id'] = $record->id;
            $detail['field_name'] = $record->field_name;
            $detail['field_text'] = $record->field_text;
            $result[] = $detail;
        }
        return $result;
    }

    public function getId($id) {
        $result = null;
        $record = Client::find($id);
        if (!is_null($record)) {
            //TODO:  Move this to the model.
            $record->ethnicity = $this->getEthnicityFor($record);
            $result = array();
            $result['id'] = $id;
            $result['client'] = $record->toArray();
            $result['case'] = $this->getCaseFor($record);
            $result['details'] = $this->getDetailsFor($record);
            $result['firms'] = $this->getFirmsFor($record);
            $result['attorneys'] = $this->getAttorneysFor($record);
        }
        return Response::json($result);
    }

    private function convertDate($date_str) {
        $result = '';
        if (! is_null($date_str)) {
            $result =  date('F d, Y', strtotime($date_str));
        }
        return $result;
    }

    public function getReport($id) {
        $result = null;
        $remap = array(
            'has_property',
            'has_warrants',
            'has_convictions',
            'is_student',
            'is_registered_sex_offender',
            'is_on_parole',
            'is_licensed_professional',
            'has_notified_licensing_board',
            'has_bankruptcies',
            'has_judgments',
            'has_marriage',
            'has_legal_proceedings',
            'has_previous_request',
            'no_publication'
        );
        $fields = array(
            'attorney' => 'Attorney(s)',
            'firm_name' => 'Firm(s)',
            'date_intake_completed' => 'Date Intake Completed',
            'date_of_assignment' => 'Date Assigned To Attorney',
            'preferred_first_name' => 'Preferred First Name(s)',
            'preferred_middle_name' => 'Preferred Middle Name(s)',
            'preferred_last_name' => 'Preferred Last Name(s)',
            'legal_first_name' => 'Legal First Name(s)',
            'legal_middle_name' => 'Legal Middle Name(s)',
            'legal_last_name' => 'Legal Last Name(s)',
            'aliases' => 'Aliases',
            'preferred_pronoun' => 'What pronouns do you prefer?',
            'preferred_pronoun_notes' => '',
            'phone1' => 'Phone 1',
            'phone1_type' => 'Type',
            'phone1_notes' => 'Phone 1 Notes',
            'phone2' => 'Phone 2',
            'phone2_type' => 'Type',
            'phone2_notes' => 'Phone 2 Notes',
            'email' => 'Email',
            'alternate_contact' => 'Alternate Contact',
            'age' => 'Age',
            'date_of_birth' => 'Date Of Birth',
            'place_of_birth' => 'Place of Birth',
            'state_of_birth' => 'State of Birth',
            'has_birth_certificate' => 'Has Birth Certificate?',
            'birth_certificate_number' => 'Birth Certificate Number',
            'current_address' => 'Current Address',
            'previous_addresses' => 'Previous Addresses',
            'type_of_housing' => 'Type of Housing',
            'resides_with' => 'Resides With',
            'citizenship_status' => 'Citizenship/Immigration Status',
            'details_immigration' => '',
            'marital_status' => 'Marital Status',
            'has_marriage' => 'Have you ever been married or in a civil union?',
            'details_marriage' => '',
            'has_children' => 'Do you have any children?',
            'details_children' => '',
            'has_warrants' => 'Do you have any unpaid tickets or warrants?',
            'details_warrants' => '',
            'has_convictions' => 'Have you ever been arrested or convicted of a crime?',
            'details_convictions' => '',
            'is_registered_sex_offender' => 'Are you a registered sex offender in Wisconsin?',
            'details_sex_offender' => '',
            'is_on_parole' => 'Are you currently on parole for a previously committed crime?',
            'details_parole' => '',
            'is_licensed_professional' => 'Do you work in a job for which a license has been required by any state, or is your only professional license to teach in the public schools in Wisconsin?',
            'has_notified_licensing_board' => 'Have you notified the state board or commission for your profession and do they have no objection to your proposed name change?',
            'has_bankruptcies' => 'Have you ever declared bankruptcy?',
            'details_bankruptcies' => '',
            'has_judgments' => 'Do you owe anyone money? Are there any judgments or liens against you?',
            'details_judgments' => '',
            'has_legal_proceedings' => 'Are you currently a party to any legal proceedings?',
            'details_legal_proceedings' => '',
            'has_previous_request' => 'Has your name ever been changed before?',
            'details_previous_request' => '',
            'reasons_for_name_change' => 'Describe the reason for your name change?',
            'no_publication' => "Do you feel that publication in the newspaper or on the court's website would threaten your safety?",
            'details_publication_waiver' => '',
            'details_no_court_listing' => '',
            'gender_transition' => 'Gender Transition',
            'gender_identity' => 'Gender Identity',
            'ethnicity' => 'Race/Ethnicity',
            'education' => 'Education',
            'is_student' => 'Are you currently a student?',
            'details_student' => '',
            'employment_values' => 'Are you employed?',
            'employer' => 'Employer',
            'job_title' => 'Job Title',
            'payment_ability' => 'Can you pay the fees and costs of the name change on your own?',
            'has_public_assistance' => 'Do you receive any form of Public Assistance (i.e. food stamps, AFDC, TANF, W-2, SSI)?',
            'has_medicaid' => 'Do you use Medicaid?',
            'income' => 'Please list your income',
            'expenses' => 'Please list your expenses',
            'has_property' => 'Do you own any property?',
            'details_property' => '',
            'notes_intake' => 'Intake Notes',
        );

        //$properties->setCreator('TLDEF');
        //$properties->setCompany('Transgender Legal Defense And Education Fund');
        //$properties->setCategory('TLDEF Name Change Project Client Intake Report');

        $filename = 'report' . $id . '.pdf';

        $client_record = Client::find($id);
        if (!is_null($client_record)) {

            $client_name = $client_record->preferred_first_name . ' ' . $client_record->preferred_last_name;
            $title = $client_name . ' Name Change Intake Report';
            $filename = $title . '.pdf';

            $result = $client_record->toArray();
            $detail_records = $client_record->clientDetail()->get();
            foreach ($detail_records as $item) {
                $result[$item->field_name] = $item->field_text;
            }
            $result['current_address'] = $this->getMailingAddressFor($client_record);
            $result['ethnicity'] = $this->getEthnicityFor($client_record);
            $result['attorney'] = $this->getAttorneyNamesFor($client_record);
            $result['firm_name'] = $this->getFirmNamesFor($client_record);
            //$result['firm_name'] = $this->getFirmNameFor($client_record->firm_id);
            $result['age'] = $this->calculateAgeOn($client_record->date_of_birth,'00:00:00');
            $field_names = array_keys($fields);
            $data = array();
            if (!is_null($result['has_state_id'])){
                $has_state_id = $result['has_state_id'];
            } else {
                $has_state_id = '';
            }
            if (!is_null($result['state'])) {
                $state_id_label = 'Do you have a ' . $result['state'] . ' state ID ?';
            } else {
                $state_id_label = 'Do you have a NY state ID ?';
            }
            $data['has_state_id'] = array($state_id_label,$has_state_id);
            $row = null;
            foreach ($field_names as $field_name) {
                if ($fields[$field_name] != '') {
                    // new field
                    if (!is_null($row)) {
                        $data[$row['field_name']] = array($row['label'], $row['value']);
                    }
                    $row = array();
                    $row['label'] = $fields[$field_name];
                    $row['value'] = '';
                    $row['field_name'] = $field_name;
                }
                if (array_key_exists($field_name, $result)) {
                    $value = $result[$field_name];
                    if (in_array($field_name, $remap)) {
                        if ($value == '0') {
                            $value = 'No';
                        } else if ($value == '1') {
                            $value = 'Yes';
                        } else {
                            $value = ' ';
                        }
                    }
                    if ( $row['value'] == '') {
                        $row['value'] = $value;
                    } else if (( $row['value'] == 'Yes') || ($row['value'] == 'No')) {
                        $row['value'] = $row['value'] . '. ' . $value;
                    } else {
                        $row['value'] = $row['value'] . ' ' . $value;
                    }
                }
            }
            if (!is_null($row)) {
                $data[$row['field_name']] = array($row['label'], $row['value']);
            }

        }

        $pdf = new libraries\tldefNCP\tldefPDF\tldefPDF();
        $pdf->SetHeaderTitle($title);
        $pdf->AliasNbPages();
        $pdf->AddPage();


        $pdf->SetFillColor(153, 214, 213);
        $pdf->SetLineWidth(.2);
        $pdf->SetTextColor(0);
        $pdf->SetFont('Times', '', 10);

        $pdf->singleColumnBox($data['attorney']);
        $pdf->singleColumnBox($data['firm_name']);

        $data['date_intake_completed'][1] = $this->convertDate($data['date_intake_completed'][1]);
        $data['date_of_assignment'][1] = $this->convertDate($data['date_of_assignment'][1]);
        $column_data = array();
        $column_data[] = $pdf->makeColumn($data['date_intake_completed'],60);
        $column_data[] = $pdf->makeColumn($data['date_of_assignment'],60);
        $pdf->multiColumnBox($column_data);

        $column_data = array();
        $column_data[] = $pdf->makeColumn($data['legal_first_name'],60);
        $column_data[] = $pdf->makeColumn($data['legal_middle_name'],60);
        $column_data[] = $pdf->makeColumn($data['legal_last_name'],60);
        $pdf->multiColumnBox($column_data);

        $pdf->singleColumnBox($data['aliases']);

        $column_data = array();
        $column_data[] = $pdf->makeColumn($data['preferred_first_name'],60);
        $column_data[] = $pdf->makeColumn($data['preferred_middle_name'],60);
        $column_data[] = $pdf->makeColumn($data['preferred_last_name'],60);
        $pdf->multiColumnBox($column_data);

        if ($data['preferred_pronoun'][1] == 'male') {
            $data['preferred_pronoun'][1] = 'He';
        } else if ($data['preferred_pronoun'][1] == 'female' ) {
            $data['preferred_pronoun'][1] = 'She';
        }
        $pdf->inlineBox($data['preferred_pronoun'],60,120);

        $column_data = array();
        $column_data[] = $pdf->makeColumn($data['phone1'],60);
        $column_data[] = $pdf->makeColumn($data['phone1_type'],60);
        $pdf->multiColumnBox($column_data);

        $pdf->singleColumnBox($data['phone1_notes']);

        $column_data = array();
        $column_data[] = $pdf->makeColumn($data['phone2'],60);
        $column_data[] = $pdf->makeColumn($data['phone2_type'],60);
        $pdf->multiColumnBox($column_data);

        $pdf->singleColumnBox($data['phone2_notes']);
        $pdf->singleColumnBox($data['email']);
        $pdf->singleColumnBox($data['alternate_contact']);

        $data['date_of_birth'][1] = $this->convertDate($data['date_of_birth'][1]);
        if (!is_null($data['state_of_birth'][1])) {
            $data['place_of_birth'][1] .= ', ' . $data['state_of_birth'][1];
        }
        $column_data = array();
        $column_data[] = $pdf->makeColumn($data['age'],20);
        $column_data[] = $pdf->makeColumn($data['date_of_birth'],40);
        $column_data[] = $pdf->makeColumn($data['place_of_birth'],120);
        $pdf->multiColumnBox($column_data);

        $pdf->inlineBox($data['has_birth_certificate'],60,60);
        $pdf->singleColumnBox($data['birth_certificate_number']);
        $pdf->singleColumnBox($data['current_address']);

        // For Pennsylvania only
        if ($result['state'] == 'PA') {
            $pdf->singleColumnBox($data['previous_addresses']);
        }

        $column_data = array();
        $column_data[] = $pdf->makeColumn($data['type_of_housing'],90);
        $column_data[] = $pdf->makeColumn($data['resides_with'],90);
        $pdf->multiColumnBox($column_data);

        $pdf->singleColumnBox($data['citizenship_status']);
        $pdf->singleColumnBox($data['has_marriage']);
        $pdf->singleColumnBox($data['has_children']);
        $pdf->singleColumnBox($data['has_warrants']);
        $pdf->singleColumnBox($data['has_convictions']);

        // For Wisconsin only
        if ($result['state'] == 'WI') {
            $pdf->singleColumnBox($data['is_registered_sex_offender']);
            $pdf->singleColumnBox($data['is_on_parole']);
            $pdf->singleColumnBox($data['job_title']);
            $pdf->singleColumnBox($data['is_licensed_professional']);
            if ($result['is_licensed_professional'] == 1) {
                $pdf->singleColumnBox($data['has_notified_licensing_board']);
            }
        }

        $pdf->singleColumnBox($data['has_bankruptcies']);
        $pdf->singleColumnBox($data['has_judgments']);
        $pdf->singleColumnBox($data['has_legal_proceedings']);
        $pdf->singleColumnBox($data['has_previous_request']);
        $pdf->singleColumnBox($data['reasons_for_name_change']);
        $pdf->singleColumnBox($data['no_publication']);

        $column_data = array();
        $column_data[] = $pdf->makeColumn($data['gender_transition'],60);
        $column_data[] = $pdf->makeColumn($data['gender_identity'],120);
        $pdf->multiColumnBox($column_data);

        $pdf->inlineBox($data['ethnicity'],60,120);
        // For Pennsylvania only
        if ($result['state'] == 'PA') {
            $pdf->singleColumnBox($data['is_student']);
        }
        $pdf->inlineBox($data['education'],60,120);
        $pdf->singleColumnBox($data['payment_ability']);
        $pdf->inlineBox($data['has_public_assistance'],140,40);
        $pdf->inlineBox($data['has_medicaid'],140,40);
        $pdf->singleColumnBox($data['income']);
        $pdf->singleColumnBox($data['expenses']);
        $pdf->singleColumnBox($data['has_property']);
        $pdf->inlineBox($data['has_state_id'],120,60);
        $pdf->singleColumnBox($data['notes_intake']);

        $pdf->Output($filename, 'I');


    }


    public function postDelete() {
        $status = 0;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $record = Client::find($input['id']);
            if (!is_null($record)) {
                $status = $record->delete();
            }
        }
        return Response::json(array('status' => $status));
    }

    public function postClearFirm() {
        $status = 0;
        $inputs = Input::get();
        if (array_key_exists('client_id',$inputs) and array_key_exists('firm_id',$inputs)) {
            $client = Client::find($inputs['client_id']);
            if (!is_null($client)) {
                $attorneys = $client->attorney()->where('firm_id', '=', $inputs['firm_id'])->get();
                foreach ($attorneys as $attorney) {
                    $status += $client->attorney()->detach($attorney->id);
                }
                $firms = $client->firm()->where('firm_id', '=', $inputs['firm_id'])->get();
                foreach ($firms as $firm) {
                    $status += $client->firm()->detach($firm->id);
                }
            }
        }
        return Response::json(array('status' => $status));
    }

    private function calculateAgeAtIntake($data) {
        if (array_key_exists('date_of_birth', $data) && ! is_null($data['date_of_birth'])
            && array_key_exists('date_intake_completed', $data) && ! is_null($data['date_intake_completed'])) {
            $result = $this->calculateAgeOn($data['date_of_birth'], $data['date_intake_completed']);
        } else {
            $result = null;
        }
        return $result;
    }

    private function updateClientRecord($client, $data) {
        if (array_key_exists('ethnicity', $data)) {
            $this->updateEthnicity($client, $data['ethnicity']);
            array_forget($data,'ethnicity');
        }
        $data['age_at_intake'] = $this->calculateAgeAtIntake($data);
        return $client->update($data);
    }

    private function updateCaseRecord($client, $data) {
        if (!is_null($data)) {
            if (array_key_exists('case_id', $data) and !is_null($data['case_id'])) {
                $record = $client->clientCase();
                $record->update($data);
            } else {
                $case = new ClientCase($data);
                $client->clientCase()->save($case);
            }
        }
        return;
    }

    private function updateDetailRecords($client, $details) {
        $update_details = array();
        $add_details = array();
        foreach ($details as $detail) {
            if (array_key_exists('id', $detail)) {
                if (!is_null($detail['id'])) {
                    $update_details[$detail['id']] = $detail;
                } else {
                    unset($detail['id']);
                    $add_details[] = $detail;
                }
            }
        }
        $ids = array();
        $records = $client->clientDetail()->get();
        foreach ($records as $record) {
            if (array_key_exists($record->id, $update_details)) {
                if ($record->update($update_details[$record->id]) != 0) {
                    $ids[] = $record->id;
                }
            }
        }
        foreach ($add_details as $data) {
            $detail = new ClientDetail($data);
            if ($client->clientDetail()->save($detail)) {
                $ids[] = $detail->id;
            }
        }
        return;
    }

    private function addEthnicity($client, $list) {
        $status = 0;
        $names = explode(',', $list);
        $records = Ethnicity::all();
        $ids = array();
        foreach ($records as $record) {
            if ( in_array($record->name,$names) ) {
                $ids[] = $record->id;
            }
        }
        foreach ($ids as $id) {
            $status += $client->ethnicity()->attach($id);
        }
        return $status;
    }

    private function updateEthnicity($client, $list) {
        $status = 0;
        $names = explode(',', $list);
        $records = Ethnicity::all();
        $new = array();
        foreach ($records as $record) {
            if ( in_array($record->name, $names) ) {
                $new[] = $record->id;
            }
        }
        $old = array();
        $records = $client->ethnicity()->get();
        foreach ($records as $record) {
            $old[] = $record->id;
        }
        // Ignore ids in intersection(old, new)
        $unchanged = array_intersect($old, $new);
        // Add ids returned by difference(new, unchanged)
        $adds = array_diff($new, $unchanged);
        foreach ($adds as $id) {
            $status += $client->ethnicity()->attach($id);
        }
        // Remove ids returned by difference(unchanged, old)
        $removes = array_diff($old, $unchanged);
        foreach ($removes as $id) {
            $status += $client->ethnicity()->detach($id);
        }
        return $status;
    }

    private function updateAttorneys($client, $ids) {
        $status = 0;
        if (array_key_exists('add', $ids)) {
            foreach ($ids['add'] as $id) {
                $status += $client->attorney()->attach($id);
            }
        }
        if (array_key_exists('remove', $ids)) {
            foreach ($ids['remove'] as $id) {
                $status += $client->attorney()->detach($id);
            }
        }
        return $status;
    }

    private function updateFirms($client, $ids) {
        $status = 0;
        if (array_key_exists('add', $ids)) {
            foreach ($ids['add'] as $id) {
                $status += $client->firm()->attach($id);
            }
        }
        if (array_key_exists('remove', $ids)) {
            foreach ($ids['remove'] as $id) {
                $status += $client->firm()->detach($id);
            }
        }
        return $status;
    }

    public function postUpdate() {
        $result = null;
        $input = Input::get();
        if (array_key_exists('id', $input) and array_key_exists('client', $input)) {
            $id = $input['id'];
            $client = Client::find($id);
            if (!is_null($client)) {
                // Need to update client, case, details and assignments records separately.
                $this->updateClientRecord($client, $input['client']);
                if (array_key_exists('case', $input) && $input['case']) {
                    $this->updateCaseRecord($client, $input['case']);
                }
                if (array_key_exists('details', $input)) {
                    $this->updateDetailRecords($client, $input['details']);
                }
                if (array_key_exists('attorneys', $input)) {
                    $this->updateAttorneys($client, $input['attorneys']);
                }
                if (array_key_exists('firms', $input)) {
                    $this->updateFirms($client, $input['firms']);
                }
            }
            $result = $this->getId($id);
        }
        return $result;
    }

    public function postAdd() {
        $result = null;
        $input = Input::get();
        if (array_key_exists('client', $input)) {
            $client = new Client();
            $client_data = $input['client'];
            if (array_key_exists('ethnicity', $client_data)) {
                $ethnicity = $client_data['ethnicity'];
                array_forget($client_data,'ethnicity');
            } else {
                $ethnicity = '';
            }
            $client_data['age_at_intake'] = $this->calculateAgeAtIntake($client_data);
            $client_record = $client->create($client_data);
            $data = array();
            if ($client_record) {
                $data['id'] = strval($client_record->id);
                $data['client'] = $client_record->toArray();
                // Add ethnicity records for this client.
                if ($this->addEthnicity($client_record,$ethnicity) > 0) {
                    $data['client']['ethnicity'] = $ethnicity;
                }
                $data['client']['created_at'] = $client_record->created_at->format('Y/m/d H:i:s');
                $data['client']['updated_at'] = $client_record->updated_at->format('Y/m/d H:i:s');
            }
            if (!is_null($data)) {
                $result = array();
                $result['id'] = $data['id'];
                $result['client'] = $data['client'];
                // Add case record for this client.
                if (array_key_exists('case', $input) && $input['case']) {
                    $result['case'] = array();
                    $case_record = new ClientCase($input['case']);
                    if ($case_record) {
                        $client_record->clientCase()->save($case_record);
                        $result['case'] = $case_record->toArray();
                    }
                }
                // Add detail records for this client.
                if (array_key_exists('details', $input)) {
                    $details = array();
                    foreach ($input['details'] as $data) {
                        $detail_record = new ClientDetail($data);
                        if ($detail_record) {
                            $client_record->clientDetail()->save($detail_record);
                            $details[] = $detail_record->toArray();
                        }
                    }
                    $result['details'] = $details;
                }
                // Add attorney records for this client.
                if (array_key_exists('attorneys', $input)) {
                    $status = 0;
                    $ids = $input['attorneys'];
                    if (array_key_exists('add', $ids)) {
                        foreach ($ids['add'] as $id) {
                            $status += $client_record->attorney()->attach($id);
                        }
                    }
                    //$result['attorneys'] = $status;
                }
            }
        }
        return Response::json($result);
    }

    private function parseNcpFilter($group) {
        $str = '';
        $first = true;
        foreach ($group['conditions'] as $condition) {
            if ($first) {
                $str .= $this->getConditionClause($condition);
                $first = false;
            } else {
                $str .= ' ' . $group['op'] . ' ' . $this->getConditionClause($condition);
            }
        }
        if (count($group['groups']) > 0) {
            foreach ($group['groups'] as $item) {
                $str .= ' ' . $group['op'] . ' ( ' . $this->parseNcpFilter($item) . ' )';
            }
        }
        return $str;
    }

    public function postSearch() {
        $inputs = Input::get();
        $fields = '`id`,`preferred_first_name`,`preferred_middle_name`,`preferred_last_name`, `legal_first_name`,`legal_last_name`,`phone1`,`phone2`,`is_closed`,`updated_at`';
        $query = 'SELECT ' . $fields . ' FROM clients WHERE ' . $this->parseNcpFilter($inputs) . ' ORDER BY updated_at DESC;';
        Log::info($query);
        return $this->getFilteredList($query);
    }

    private function getEmailList($query, $separator) {
        $results = array();
        $records = DB::select($query);
        foreach ($records as $record) {
            if (!is_null($record->email)) {
                $results[] = $record->email;
            }
        }
        return implode($separator, $results);
    }

    public function getEmails($filter) {
        $filename = 'emails.txt';
        $inputs = json_decode($filter,true);
        $query = 'SELECT email FROM clients WHERE ' . $this->parseNcpFilter($inputs) . ' ORDER BY email ASC;';
        $emails = $this->getEmailList($query,"\n");
        return Response::make($emails, 200, array(
            'Content-Description'       => 'File Transfer',
            'Content-Type'              => 'text/html',
            'Content-Disposition'       => 'attachment; filename="' . $filename . '"',
            'Content-Transfer-Encoding' => 'binary',
            'Expires'                   => 0,
            'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma'                    => 'public',
            'Content-Length'            => mb_strlen($emails)
        ));
    }

    private function getConditionClause($input) {
        switch($input['table']) {
            case 'client_cases':
                $result = $this->getCaseConditionClause($input);
                break;
            case 'client_details':
                $result = $this->getDetailConditionClause($input);
                break;
            case 'clients':
            default:
                $result = $this->getClientConditionClause($input);
                break;
        }
        return $result;
    }

    private function getCaseConditionClause($input) {
        $prefix = 'clients.id in (select clients.id from clients join client_cases on clients.id = client_cases.client_id where ';
        $condition = $this->getSimpleConditionClause($input['field'],$input);
        $result = $prefix . $condition . ') ';
        return $result;
    }

    private function getDetailConditionClause($input) {
        $prefix = 'clients.id in (select clients.id from clients join client_details on clients.id = client_details.client_id where ';
        $condition1 = 'client_details.field_name = "' . $input['field'] . '" and ';
        $condition2 = $this->getSimpleConditionClause('client_details.field_text',$input);
        $result = $prefix . $condition1 . $condition2 . ') ';
        return $result;
    }

    private function getClientConditionClause($input) {
        switch ($input['field']) {
            case 'phone':
                if ($input['option'] == 'is empty'){
                    $op = 'AND';
                } else {
                    $op = 'OR';
                }
                $result = $this->getCompoundConditionClause(array('phone1','phone2'),$input,$op);
                break;
//            case 'email':
//                if (($input['option'] == 'is empty') or ($input['option'] == 'is empty')){
//                    $op = 'AND';
//                } else {
//                    $op = 'OR';
//                }
//                $result = $this->getCompoundConditionClause(array('email1','email2'),$input, $op);
//                break;
            default:
                $result = $this->getSimpleConditionClause($input['field'],$input);
                break;
        }
        return $result;
    }

    private function getCompoundConditionClause($fields, $input, $operator) {
        $table = $input['table'];
        $keyField = $table . '.' . $this->getPrimaryKeyField($table);
        $args = array($table,$keyField);
        $clause = vsprintf('%2$s in (select %2$s from %1$s where ', $args);
        $first = true;
        foreach ($fields as $field) {
            if ($first) {
                $clause = $clause . $this->getSimpleConditionClause($field,$input);
                $first = false;
            } else {
                $clause = $clause . ' ' . $operator . ' ' . $this->getSimpleConditionClause($field,$input);
            }
        }
        $clause = $clause . ')';
        return $clause;
    }


    private function getSimpleConditionClause($field,$input) {
        $formats = array(
            'is' => '%1$s = "%2$s"',
            'is not' => '%1$s <> "%2$s"',
            'is empty' => '%1$s IS NULL',
            'is not empty' => '%1$s IS NOT NULL',
            'true' => '%1$s = 1',
            'false' => '%1$s = 0',
            'yes' => '%1$s = "Yes"',
            'no' => '%1$s = "No"',
            'begins with' => '%1$s LIKE "%2$s%%"',
            'ends with' => '%1$s LIKE "%%%2$s"',
            'contains' => '%1$s LIKE "%%%2$s%%"',
            'does not contain' => '%1$s NOT LIKE "%%%2$s%%"',
            'is less than' => '%1$s < "%2$s"',
            'is greater than' => '%1$s > "%2$s"',
            'is before' => '%1$s < "%2$s"',
            'is after' => '%1$s > "%2$s"',
            'is in the range' => '%5$s in (select %5$s from %4$s where %1$s >= "%2$s" AND %1$s <= "%3$s")'
        );
        $table = $input['table'];
        $keyField = $table . '.' . $this->getPrimaryKeyField($table);
        $args = array($field,$input['value1'],$input['value2'],$table,$keyField);
        return vsprintf($formats[$input['option']],$args);
    }

    private function getPrimaryKeyField($table) {
        switch($table) {
            case 'client_cases':
                $result = 'case_id';
                break;
            default:
                $result = 'id';
        }
        return $result;
    }

    private function getFilteredList($query) {
        $results = array();
        $records = DB::select($query);
        foreach ($records as $record) {
            $results[] = $this->getClientFields($record);
        }
        return Response::json(array('clients' => $results));
    }


    private function getClientFields($client) {
        $record = array();
        $record['id'] = $client->id;
        $last_name = trim($client->preferred_last_name);
        $first_name = trim($client->preferred_first_name);
        $middle_name = trim($client->preferred_middle_name);
        $record['preferred_name'] = trim($last_name . ', ' . $first_name . ' ' . $middle_name);
        $last_name = trim($client->legal_last_name);
        $first_name = trim($client->legal_first_name);
        if ($first_name == '' and $last_name == '') {
            $record['legal_name'] = '';
        } else {
            $record['legal_name'] = $last_name . ', ' . $first_name;
        }
        $record['phone1'] = $client->phone1;
        $record['phone2'] = $client->phone2;
        $record['is_closed'] = $client->is_closed;
        $record['updated_at'] = $client->updated_at;
        return $record;
    }

    /// Maintenance function

//    public function getPoke() {
//        $values = array();
//        $records = Client::where('ethnicity','<>', '')->get(array('id','ethnicity'));
//        $prefix  = "INSERT INTO client_ethnicity (`client_id`, `ethnicity_id`, `created_at`, `updated_at`) VALUES ";
//        foreach ( $records as $record ) {
//            $items = explode(',',$record->ethnicity);
//            foreach ($items as $item) {
//                $eth = Ethnicity::where('name', '=', $item)->get(array('id','name'));
//                if ($eth) {
//                    $values[] = $prefix . '(' . $record->id . ', ' . $eth[0]->id .', NOW(), NOW());';
//                }
//            }
//        }
//        $result = implode("\n", $values);
//        return Response::make($result, 200, array(
//            'Content-Description'       => 'File Transfer',
//            'Content-Type'              => 'text/html',
//            'Content-Disposition'       => 'attachment; filename="' . 'poke.txt' . '"',
//            'Content-Transfer-Encoding' => 'binary',
//            'Expires'                   => 0,
//            'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
//            'Pragma'                    => 'public',
//            'Content-Length'            => mb_strlen($result)
//        ));
//    }

      public function getPoke() {
          $values = array();
          $records = Client::whereNotNull('date_intake_completed')->get();
          foreach ( $records as $record ) {
              $data = array();
              $data['date_of_birth'] = $record->date_of_birth;
              $data['date_intake_completed'] = $record->date_intake_completed;
              $age_at_intake = $this->calculateAgeAtIntake($data);
              if (!is_null($age_at_intake) && $age_at_intake > 0) {
                  $values[] = "UPDATE clients SET age_at_intake = " . $age_at_intake . " WHERE id = " . $record->id . ";";
              }
          }
          $result = implode("\n", $values);
          return Response::make($result, 200, array(
              'Content-Description'       => 'File Transfer',
              'Content-Type'              => 'text/html',
              'Content-Disposition'       => 'attachment; filename="' . 'poke.txt' . '"',
              'Content-Transfer-Encoding' => 'binary',
              'Expires'                   => 0,
              'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
              'Pragma'                    => 'public',
              'Content-Length'            => mb_strlen($result)
          ));
      }
}
