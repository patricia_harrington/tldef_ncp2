<?php
/**
 * Filename: ClientFieldsController.php 
 * Author: Patricia Harrington
 * Created: 9/1/13 4:22 PM
 */

class ClientFieldsController extends BaseController {

    public function getIndex()
    {
        return $this->getList();
    }

    public function getList()
    {
        $results = array();
        $records = ClientField::all();
        foreach ($records as $record) {
            $data = $this->getData($record);
            // Add edit specific items
            $data['prompt'] = $record->input_prompt;
            $results[] = $data;
        }
        return Response::json(array('fields' => $results));
    }


    public function getFilters()
    {
        $results = array();
        $records = ClientField::all();
        foreach ($records as $record) {
            if ($record->is_filter == 1) {
                $data = $this->getData($record);
                // Add filter specific items
                $data['filterInput'] = $record->filter_input;
                // Filter alias overrides field name.
                if (!is_null($record->filter_name)) {
                    $data['name'] = $record->filter_name;
                }
                if (!is_null($record->filter_label)) {
                    $data['label'] = $record->filter_label;
                }
                $results[] = $data;
            }
        }
        return Response::json(array('fields' => $results));
    }

    private function getData($record) {
        $data = array();
        $data['name'] = $record->field_name;
        $data['label'] = $record->input_label;
        $data['table'] = $record->table_name;
        $data['type'] = $record->input_type;
        if ($record->input_type == 'select' || $record->input_type == 'checkbox' ) {
            $data['options'] = $this->getOptions($record->field_name);
        }
        return $data;
    }

    private function getOptions($field_name) {
        $options = array();
        if ($field_name == 'state') {
            $records = StateName::all();
            foreach ($records as $record) {
                //$data = array();
                //$data['state_code'] = $record->state_code;
                //$data['state_name'] = $record->state_name;
                //$options[] = $data;
                $options[] = $record->state_code;
            }
        } else {
            if (($field_name == 'phone1_type') || ($field_name == 'phone2_type')) {
                $name = 'phone_type';
            } else {
                $name = $field_name;
            }
            $records = ClientOption::where('field_name','=', $name)->where('is_active', '=', '1')->get();
            foreach ($records as $record) {
                $options[] = $record->option_value;
            }
        }
        return $options;
    }

}