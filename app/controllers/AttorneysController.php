<?php
/**
 * Filename: AttorneysController.php
 * Author: Patricia Harrington
 * Created: 12/27/12 5:15 PM
 * Copyright 2012 Transgender Legal Defense & Education Fund, Inc.
 */
class AttorneysController extends BaseController {

    public function getIndex() {
        return $this->getList();
    }

    private function getFirmNames() {
        $results = array();
        $firms = Firm::all();
        foreach ($firms as $firm) {
            $results[$firm->id] = $firm->full_name;
        }
        return $results;
    }

    private function getZoneNames() {
        $results = array();
        $zones = Zone::all();
        foreach ($zones as $zone) {
            $results[$zone->id] = $zone->name;
        }
        return $results;
    }

    public function getList() {
        $results = array();
        $firmNames = $this->getFirmNames();
        $zoneNames = $this->getZoneNames();
        $attorneys = Attorney::all();
        foreach ($attorneys as $attorney) {
            $record = array();
            $record['id'] = $attorney->id;
            $record['last_name'] = $attorney->last_name;
            $record['first_name'] = $attorney->first_name;
            $record['firm_id'] = $attorney->firm_id;
            $record['firm_name'] = ($attorney->firm_id > 0 ) ? $firmNames[$attorney->firm_id] : '';
            $record['is_active'] = ($attorney->is_active > 0 ) ? 'Yes' : 'No';
            $record['location'] = ($attorney->zone_id > 0 ) ? $zoneNames[$attorney->zone_id] : '';
            $record['zone_id'] = $attorney->zone_id;
            $record['email'] = $attorney->email;
            $results[] = $record;
        }
        return Response::json(array('attorneys' => $results));
    }

    public function getForFirm($firm_id = '0',$zone_id = '0') {
        $results = array();
        $firm = Firm::where('id', '=', $firm_id)->first();
        $firm_name = is_null($firm) ? '' : $firm->full_name;
        $zoneNames = $this->getZoneNames();
        if ($zone_id == 0) {
            $attorneys = Attorney::where('firm_id', '=', $firm_id)->orderBy('last_name', 'asc')->get();
        } else {
            $attorneys = Attorney::where('firm_id', '=', $firm_id)->where('zone_id', '=', $zone_id)->orderBy('last_name', 'asc')->get();
        }
        foreach ($attorneys as $attorney) {
                $record = array();
                $record['id'] = $attorney->id;
                $record['name'] = $attorney->fullName();
                $record['last_name'] = $attorney->last_name;
                $record['first_name'] = $attorney->first_name;
                $record['phone'] = $attorney->phone;
                $record['email'] = $attorney->email;
                $record['firm_id'] = $attorney->firm_id;
                $record['firm_name'] = $firm_name;
                $record['is_active'] = ($attorney->is_active > 0 ) ? 'Yes' : 'No';
                $record['location'] = ($attorney->zone_id > 0 ) ? $zoneNames[$attorney->zone_id] : '';
                $record['zone_id'] = $attorney->zone_id;
                $record['email'] = $attorney->email;
                $results[] = $record;
        }
        return Response::json(array('attorneys' => $results));
    }

    public function getId($id) {
        $result = null;
        $record = Attorney::find($id);
        if (!is_null($record)) {
            $result = $record->toArray();
            $firm = $record->firm()->first();
            $result['firm_name'] = is_null($firm) ? '' : $firm->full_name;
        }
        return Response::json(array('attorney' => $result));
    }

    public function postDelete() {
        $status = 0;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $record = Attorney::find($input['id']);
            if (!is_null($record)) {
                $status = $record->delete();
            }
        }
        return Response::json(array('status' => $status));
    }

    public function postUpdate() {
        $result = null;
        $input = Input::get();
        if (array_key_exists('id', $input)){
            $id = $input['id'];
            $record = Attorney::find($id);
            if (!is_null($record)) {
                unset($input['firm_name']);
                if ( $record->update($input) != 0 ){
                    $record = Attorney::find($id);
                    $result = $record->toArray();
                    $firm = $record->firm()->first();
                    $result['firm_name'] = is_null($firm) ? '' : $firm->full_name;
                }
            }
        }
        return Response::json(array('attorney' => $result));
    }

    public function postAdd() {
        $result = null;
        $input = Input::get();
        $attorney = new Attorney();
        $record = $attorney->create($input);
        if ($record){
            $result = $record->toArray();
            $firm = $record->firm()->first();
            $result['firm_name'] = is_null($firm) ? '' : $firm->full_name;
            $result['created_at'] = $record->created_at->format('Y/m/d H:i:s');
            $result['updated_at'] = $record->updated_at->format('Y/m/d H:i:s');
        }
        return Response::json(array('attorney' => $result));
    }
}
