<?php
/**
 * Filename: reports.php
 * Author: Patricia Harrington
 * Created: 5/29/13 4:09 PM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */

class ReportsController extends BaseController {

    public function getIndex() {
        return $this->getSummary();
    }

    public function getSummary() {
        $result = array();
        $result['total_clients'] = DB::table('clients')->count();
        $result['total_firms'] = DB::table('firms')->count();
        $result['total_attorneys'] = DB::table('attorneys')->count();

        $firm_query = 'SELECT t1.full_name as name, count(*) as total_clients FROM firms AS t1 INNER JOIN client_firm AS t2 ON t1.id=t2.firm_id group by t1.full_name';
        $firms = DB::select($firm_query);
        $result['firms'] = $firms;

        $referrer_query = 'SELECT t1.name as name, count(*) as total_clients FROM referrers AS t1 INNER JOIN clients AS t2 ON t1.id=t2.referrer_id group by t1.name';
        $referrers = DB::select($referrer_query);
        $result['referrers'] = $referrers;

        $result['total_intakes'] = DB::table('clients')
            ->whereNotNull('date_intake_completed')
            ->count();
        $result['total_waiting'] = DB::table('clients')
            ->where('is_accepted', '=', 'Yes')->where('is_closed', '=', 'No')->where('on_hold', '=', '0')
            ->whereNotNull('date_intake_completed')->whereNull('date_of_assignment')
            ->count();
        $result['total_active'] = DB::table('clients')->where('is_closed', '=', 'No')
            ->whereNotNull('date_intake_completed')->whereNotNull('date_of_assignment')
            ->count();
        $result['total_finished'] = DB::table('clients')->where('name_change_completed', '=', 'Yes')
            ->where('is_accepted', '=', 'Yes')->where('is_closed', '=', 'Yes')
            ->whereNotNull('date_of_assignment')
            ->count();


        $interviewer_query = 'SELECT interviewer, count(*) as total_clients FROM clients where date_intake_completed is not null group by interviewer';
        $interviewers = DB::select($interviewer_query);
        foreach ($interviewers as $interviewer) {
            if (is_null($interviewer->interviewer)) {
                $interviewer->interviewer = 'No Entry';
            }
        }
        $result['interviewers'] = $interviewers;

        $county_query = 'SELECT state, county, count(*) as total_clients FROM clients group by state, county';
        $counties = DB::select($county_query);
        foreach ($counties as $county) {
            if (is_null($county->county)) {
                $county->county = 'No Entry';
            }
        }
        $result['counties'] = $counties;

        $gender_transition_query = 'SELECT gender_transition, count(*) as total_clients FROM clients group by gender_transition';
        $gender_transitions = DB::select($gender_transition_query);
        foreach ($gender_transitions as $gender_transition) {
            if (is_null($gender_transition->gender_transition)) {
                $gender_transition->gender_transition = 'No Entry';
            }
        }
        $result['gender_transitions'] = $gender_transitions;

        return Response::json(array('summary' => $result));
    }


    // Sample web service call http://namechange.transgenderlegal.dev/reports/status/2014-01-01/2014-08-04

    private function getZoneName($state, $county) {
        $zone_name = "New York City";
        if (! is_null($county) ) {
            $state_county = StateCounty::where('state_code','=',$state)->where('county_name','=',$county)->get();
            $zone_id = $state_county[0]->zone_id;
            $zone = Zone::find($zone_id);
            $zone_name = $zone->name;
        }
        return $zone_name;
    }

    private function getZoneInfo($zone_name) {
        $zones = Zone::where('name','=', $zone_name)->get();
        $results = array();
        foreach ($zones as $zone) {
            $results['state_code'] = $zone->state_code;
            $results['num_counties'] = 0;
            $results['counties'] = array();
            $state_counties = StateCounty::where('zone_id', '=', $zone->id)->get();
            foreach ($state_counties as $item) {
                $results['counties'][] = $item['county_name'];
                $results['num_counties'] += 1;
            }
        }
        return $results;
    }

    public function getStatus($start_date = null,$end_date = null,$output='html',$mail_to_address = 'namechange@transgenderlegal.org', $mail_to_name = 'Intake Coordinator') {
        $regional_statistics = array();
        $firm_statistics = array();
        $totals = array();
        // The reporting interval is from the start_date through the end_date inclusive.
        // The default will be year-to-date, where the start_date is January 1st and the end_date is today.
        if (is_null($start_date)) {
            $start_date = date('Y') .'-01-01';
        }
        if (is_null($end_date)) {
            $end_date = date('Y-m-d');
        }
        $start_time = $start_date . "00:00:00";
        $end_time = $end_date . "23:59:99";

        // Display the following information for the reporting interval:
        // - Number of clients who contacted us during the reporting interval - Total and broken out by region.

        $contacts = Client::where('created_at','>=',$start_time)->where('created_at','<=',$end_time)->get();
        // sum and bin contacts by region
        $totals['contacts'] = 0;
        foreach ($contacts as $contact) {
            $zone_name = $this->getZoneName($contact->state,$contact->county);
            if (! array_key_exists($zone_name,$regional_statistics)) {
                $regional_statistics[$zone_name] = array('contacts' => 0, 'intakes' => 0, 'assignments' => 0, 'in progress' => 0, 'waiting' => 0 );
            }
            $regional_statistics[$zone_name]['contacts'] = $regional_statistics[$zone_name]['contacts'] + 1;
            $totals['contacts'] = $totals['contacts'] + 1;
        }
        // - Number of intakes completed during the reporting interval - Total and broken out by region.
        $intakes = Client::where('date_intake_completed','>=',$start_date)->where('date_intake_completed','<=',$end_date)->get();
        // sum and bin intakes by region
        $totals['intakes'] = 0;
        foreach ($intakes as $intake) {
            $zone_name = $this->getZoneName($intake->state,$intake->county);
            if (! array_key_exists($zone_name,$regional_statistics)) {
                $regional_statistics[$zone_name] = array('contacts' => 0, 'intakes' => 0, 'assignments' => 0, 'in progress' => 0, 'waiting' => 0 );
            }
            $regional_statistics[$zone_name]['intakes'] = $regional_statistics[$zone_name]['intakes'] + 1;
            $totals['intakes'] = $totals['intakes'] + 1;
        }

        // - Number of clients assigned - Total and then broken out by region and then by firm in each region.
        $assignments = Client::where('date_of_assignment','>=',$start_date)->where('date_of_assignment','<=',$end_date)->get();
        // sum and bin assignments by region
        $totals['assignments'] = 0;
        foreach ($assignments as $assignment) {
            $zone_name = $this->getZoneName($assignment->state,$assignment->county);
            if (! array_key_exists($zone_name,$regional_statistics)) {
                $regional_statistics[$zone_name] = array('contacts' => 0, 'intakes' => 0, 'assignments' => 0, 'in progress' => 0, 'waiting' => 0);
            }
            $regional_statistics[$zone_name]['assignments'] = $regional_statistics[$zone_name]['assignments'] + 1;
            if (! array_key_exists($zone_name,$firm_statistics)) {
                $firm_statistics[$zone_name] = array();
            }
            $firms = $assignment->firm;
            foreach ($firms as $firm) {
                $firm_name = $firm->full_name;
                if (!array_key_exists($firm_name,$firm_statistics[$zone_name])) {
                    $firm_statistics[$zone_name][$firm_name] = array('assignments' => 0, 'in progress' => 0);
                }
                $firm_statistics[$zone_name][$firm_name]['assignments'] = $firm_statistics[$zone_name][$firm_name]['assignments'] + 1;
            }

            $totals['assignments'] = $totals['assignments'] + 1;
        }

        // Display the following information independent of the reporting interval because
        // there may be name changes in progress and clients who were/are waiting for assignment
        // outside of an arbitrary reporting interval.

        // -  Number of name changes in progress - Total and then broken out by region and then by firm in each region.
        $cases_in_progress = Client::where('is_accepted', '=', 'Yes')->where('is_closed', '=', 'No')->whereNotNull('date_intake_completed')->whereNotNull('date_of_assignment')->get();
        // sum and bin clients in progress by region
        $totals['in progress'] = 0;
        foreach ($cases_in_progress as $case) {
            $zone_name = $this->getZoneName($case->state,$case->county);
            if (! array_key_exists($zone_name,$regional_statistics)) {
                $regional_statistics[$zone_name] = array('contacts' => 0, 'intakes' => 0, 'assignments' => 0, 'in progress' => 0, 'waiting' => 0 );
            }
            $regional_statistics[$zone_name]['in progress'] = $regional_statistics[$zone_name]['in progress'] + 1;
            if (! array_key_exists($zone_name,$firm_statistics)) {
                $firm_statistics[$zone_name] = array();
            }
            $firms = $case->firm;
            foreach ($firms as $firm) {
                $firm_name = $firm->full_name;
                if (!array_key_exists($firm_name,$firm_statistics[$zone_name])) {
                    $firm_statistics[$zone_name][$firm_name] = array('assignments' => 0, 'in progress' => 0);
                }
                $firm_statistics[$zone_name][$firm_name]['in progress'] = $firm_statistics[$zone_name][$firm_name]['in progress'] + 1;
            }
            $totals['in progress'] = $totals['in progress'] + 1;
        }


        // -  Number of clients on the waiting list - Total and then broken out by region.
        $clients_waiting = Client::where('is_accepted', '=', 'Yes')->where('is_closed', '=', 'No')->where('on_hold','=','0')->whereNotNull('date_intake_completed')->whereNull('date_of_assignment')->get();
        // sum and bin intakes by region
        $totals['waiting'] = 0;
        foreach ($clients_waiting as $client) {
            $zone_name = $this->getZoneName($client->state,$client->county);
            if (! array_key_exists($zone_name,$regional_statistics)) {
                $regional_statistics[$zone_name] = array('contacts' => 0, 'intakes' => 0, 'assignments' => 0, 'in progress' => 0, 'waiting' => 0 );
            }
            $regional_statistics[$zone_name]['waiting'] = $regional_statistics[$zone_name]['waiting'] + 1;
            $totals['waiting'] = $totals['waiting'] + 1;
        }

        ksort($regional_statistics);
        ksort($firm_statistics);
        foreach ($firm_statistics as $key => $value) {
            ksort($firm_statistics[$key]);
        }

        $report_time = date('g:i A F j, Y');
        $date = new DateTime($start_date);
        $from_date = $date->format('F j, Y');
        $date = new DateTime($end_date);
        $to_date = $date->format('F j, Y');
        $data = array(
            'title' => 'TLDEF Name Change Project Regional Statistics Report',
            'start date' => $from_date,
            'end date' => $to_date,
            'regional statistics' => $regional_statistics,
            'firm statistics' => $firm_statistics,
            'totals' => $totals,
            'report time' => $report_time
        );

        $filename = 'Regional-Statistics-Report-' . $from_date . '-' . $to_date .  '.pdf';

        switch ($output)
        {
            case "pdf":
                $pdf = $this->makePDF($data);
                $pdf->Output($filename, 'I');
                break;
            case "email":

                $description  = 'TLDEF Name Change Project Regional Statistics from ' . $from_date . ' through ' . $to_date;
                $pdf = $this->makePDF($data);
                $pdf_data = $pdf->Output('', 'S');

                // Create the Transport
                $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                    ->setUsername('namechange@transgenderlegal.org')
                    ->setPassword('nyfxlgpdpexklytn');

                // Create the Mailer using your created Transport
                $mailer = Swift_Mailer::newInstance($transport);

                // Create a message
                $message = Swift_Message::newInstance($data['title'])
                    ->setFrom(array('namechange@transgenderlegal.org' => 'Intake Coordinator'))
                    ->setTo(array($mail_to_address => $mail_to_name))
                    ->setBody('Attached is the file: ' . $filename . ' which contains ' . $description)
                ;

                // Create the attachment with your data
                $attachment = Swift_Attachment::newInstance($pdf_data, $filename, 'application/pdf');

                $message->attach($attachment);

                // Send the message
                $numSent = $mailer->send($message);
                return Response::json(array('status' => $numSent));
                break;
            case "html":
            default:
                return View::make('status-report')->with('data',$data);
        }
    }

    private function makeBins($input) {
        $bins = array();
        if (!is_null($input)) {
            $items = explode(',', trim($input,"'"));
            foreach ($items as $item) {
                $bins[] = intval($item);
            }
        }
        return $bins;
    }

    private function addZoneCondition($zone_name) {
        $result = '';
        $zone_info = $this->getZoneInfo($zone_name);
        if ($zone_info['num_counties'] == 1) {
            $result .= " and state = '" . $zone_info['state_code']  . "'" . " and county = '" . $zone_info['counties'][0] . "'";
        } else if ($zone_info['num_counties'] > 1) {
            $counties = implode("','",$zone_info['counties']);
            $result .= " and state = '" . $zone_info['state_code']  . "'" . " and county in ('" . $counties . "')";
        }
        return $result;
    }

    private function addStartDateCondition($start_date) {
        return " and date_intake_completed >= '" . $start_date . "'";
    }

    private function addEndDateCondition($end_date){
        return " and date_intake_completed <= '" . $end_date  . "'";
    }

    private function computeAgeHistogram($input) {
        $histogram = null;
        if (array_key_exists('age_bins', $input)) {
            $bins = $this->makeBins($input['age_bins']);
            $histogram = new \libraries\tldefNCP\tldefHistogram\tldefHistogram($bins);
            $query = 'SELECT age_at_intake FROM clients WHERE age_at_intake IS NOT NULL';
            if (array_key_exists('start-date', $input)) {
                $query .= $this->addStartDateCondition($input['start-date']);
            }
            if (array_key_exists('end-date', $input)) {
                $query .= $this->addEndDateCondition($input['end-date']);;
            }
            if (array_key_exists('zone-name', $input)) {
                $query .= $this->addZoneCondition($input['zone-name']);
            }
            $results = DB::select($query);
            foreach ($results as $result) {
                $histogram->add(intval($result->age_at_intake));
            }
        }
        return $histogram;
    }

    private function computeAges($input) {
        $histogram = $this->computeAgeHistogram($input);
        if ($histogram) {
            $results = array('ages' => $histogram->getResults(), 'total' => $histogram->getTotal());
        } else {
            $results = array('ages' => 0, 'total' => 0);
        }
        return $results;
    }

    public function postAgeHistogram() {
        $input = Input::get();
        $results = $this->computeAges($input);
        return Response::json(array('ages' => $results['ages'], 'total' => $results['total'],
                'startDate' => $input['start-date'], 'endDate' => $input['end-date']));
    }

    private function computeIncomeHistogram($input) {
        $histogram = null;
        if (array_key_exists('income_bins', $input)) {
            $bins = $this->makeBins($input['income_bins']);
            $histogram = new \libraries\tldefNCP\tldefHistogram\tldefHistogram($bins);
            $query = 'SELECT annual_income FROM clients WHERE annual_income IS NOT NULL';
            if (array_key_exists('start-date', $input)) {
                $query .= $this->addStartDateCondition($input['start-date']);
            }
            if (array_key_exists('end-date', $input)) {
                $query .= $this->addEndDateCondition($input['end-date']);;
            }
            if (array_key_exists('zone-name', $input)) {
                $query .= $this->addZoneCondition($input['zone-name']);
            }
            $results = DB::select($query);
            foreach ($results as $result) {
                $value = intval(str_replace(',', '', $result->annual_income));
                $histogram->add($value);
            }
        }
        return $histogram;
    }

    private function computeIncomes($input) {
        $histogram = $this->computeIncomeHistogram($input);
        if ($histogram) {
            $results = array('incomes' => $histogram->getResults(), 'total' => $histogram->getTotal());
        } else {
            $results = array('incomes' => 0, 'total' => 0);
        }
        return $results;
    }

    public function postIncomeHistogram() {
        $input = Input::get();
        $results = $this->computeIncomes($input);
        return Response::json(array('incomes' => $results['incomes'], 'total' => $results['total'],
                'startDate' => $input['start-date'], 'endDate' => $input['end-date']));
    }

    private function computeEthnicity($input){
        $query1 = 'SELECT id FROM clients WHERE date_intake_completed IS NOT NULL ';
        if (array_key_exists('start-date', $input)) {
            $query1 .= $this->addStartDateCondition($input['start-date']);
        }
        if (array_key_exists('end-date', $input)) {
            $query1 .= $this->addEndDateCondition($input['end-date']);;
        }
        if (array_key_exists('zone-name', $input)) {
            $query1 .= $this->addZoneCondition($input['zone-name']);
        }
        $bins = array();
        $labels = ClientOption::where('field_name','=', 'ethnicity')->where('is_active', '=', '1')->orderBy('id')->get();
        foreach ($labels as $label) {
            $bins[$label->option_value] = 0;
        }
        $count = 0;
        $clients = DB::select($query1);
        foreach ($clients as $client) {
            $query2 = 'SELECT t2.name FROM client_ethnicity as t1 join ethnicities as t2 on t1.ethnicity_id  = t2.id where client_id = "' . $client->id . '"';
            $ethnicities = DB::select($query2);
            foreach ( $ethnicities as $ethnicity) {
                if (!array_key_exists($ethnicity->name, $bins)) {
                    $bins[$ethnicity->name] = 1;
                } else {
                    $bins[$ethnicity->name] += 1;
                }
            }
            $count += 1;
        }
        $items = array();
        foreach ($bins as $key => $value ) {
            $percent = Round((($value / $count) * 100),1);
            $items[] = array('label' => $key, 'value' => $value, 'percent' =>  $percent);
        }
        $results = array('items' => $items, 'count' => $count );
        return $results;
    }

    public function postEthnicity() {
        $input = Input::get();
        $results = $this->computeEthnicity($input);
        return Response::json(array('ethnicity' => $results['items'], 'total' => $results['count'],
            'startDate' => $input['start-date'], 'endDate' => $input['end-date']));
    }

    private function computeEducation($input) {
        $query = 'SELECT education FROM clients WHERE education IS NOT NULL';
        if (array_key_exists('start-date', $input)) {
            $query .= $this->addStartDateCondition($input['start-date']);
        }
        if (array_key_exists('end-date', $input)) {
            $query .= $this->addEndDateCondition($input['end-date']);;
        }
        if (array_key_exists('zone-name', $input)) {
            $query .= $this->addZoneCondition($input['zone-name']);
        }
        $bins = array();
        $labels = ClientOption::where('field_name','=', 'education')->where('is_active', '=', '1')->orderBy('id')->get();
        foreach ($labels as $label) {
            $bins[$label->option_value] = 0;
        }
        $count = 0;
        $records = DB::select($query);
        foreach ($records as $record) {
            if (!array_key_exists($record->education, $bins)) {
                $bins[$record->education] = 1;
            } else {
                $bins[$record->education] += 1;
            }
            $count += 1;

        }
        $items = array();
        foreach ($bins as $key => $value ) {
            $percent = Round((($value / $count) * 100),1);
            $items[] = array('label' => $key, 'value' => $value, 'percent' =>  $percent );
        }
        $results = array('items' => $items, 'count' => $count );
        return $results;
    }

    public function postEducation() {
        $input = Input::get();
        $results = $this->computeEducation($input);
        return Response::json(array('education' => $results['items'], 'total' => $results['count'],
            'startDate' => $input['start-date'], 'endDate' => $input['end-date']));
    }

    private function computeSexAssignedAtBirth($input){
        $query = 'SELECT sex_assigned_at_birth FROM clients WHERE sex_assigned_at_birth IS NOT NULL';
        if (array_key_exists('start-date', $input)) {
            $query .= $this->addStartDateCondition($input['start-date']);
        }
        if (array_key_exists('end-date', $input)) {
            $query .= $this->addEndDateCondition($input['end-date']);;
        }
        if (array_key_exists('zone-name', $input)) {
            $query .= $this->addZoneCondition($input['zone-name']);
        }
        $bins = array();
        $count = 0;
        $records = DB::select($query);
        foreach ($records as $record) {
            if (!array_key_exists($record->sex_assigned_at_birth, $bins)) {
                $bins[$record->sex_assigned_at_birth] = 1;
            } else {
                $bins[$record->sex_assigned_at_birth] += 1;
            }
            $count += 1;

        }
        $items = array();
        foreach ($bins as $key => $value ) {
            $percent = Round((($value / $count) * 100),1);
            $items[] = array('label' => $key, 'value' => $value, 'percent' =>  $percent );
        }
        $results = array('items' => $items, 'count' => $count);
        return $results;
    }

    public function postSexAssignedAtBirth() {
        $input = Input::get();
        $results = $this->computeSexAssignedAtBirth($input);
        return Response::json(array('items' => $results['items'], 'total' => $results['count'],
            'startDate' => $input['start-date'], 'endDate' => $input['end-date']));
    }

    private function computePercentage($query,$field) {
        $results = array('total' => 0, 'count' => 0, 'percent' => 0);
        $records = DB::select($query);
        foreach ($records as $record) {
            switch ($field) {
                case 'medicaid':
                    if ($record->has_medicaid == 'Yes') {
                        $results['count'] += 1;
                    }
                    $results['total'] += 1;
                    break;
                case 'public_assistance':
                    if ($record->has_public_assistance == 'Yes') {
                        $results['count'] += 1;
                    }
                    $results['total'] += 1;
                    break;
            }
        }
        if ($results['total'] > 0) {
            $results['percent'] = Round((($results['count'] / $results['total']) * 100),1);
        }
        return $results;
    }

    private function computeMedicaid($input) {
        $query = 'SELECT has_medicaid FROM clients WHERE has_medicaid IS NOT NULL';
        if (array_key_exists('start-date', $input)) {
            $query .= $this->addStartDateCondition($input['start-date']);
        }
        if (array_key_exists('end-date', $input)) {
            $query .= $this->addEndDateCondition($input['end-date']);;
        }
        if (array_key_exists('zone-name', $input)) {
            $query .= $this->addZoneCondition($input['zone-name']);
        }
        $results = $this->computePercentage($query,'medicaid');
        return $results;
    }

    public function postMedicaid() {
        $input = Input::get();
        $results = $this->computeMedicaid($input);
        return Response::json(array('count' => $results['count'],
            'percent' => $results['percent'], 'total' => $results['total'],
            'startDate' => $input['start-date'], 'endDate' => $input['end-date']));
    }

    private function computePublicAssistance($input) {
        $query = 'SELECT has_public_assistance FROM clients WHERE has_public_assistance IS NOT NULL';
        if (array_key_exists('start-date', $input)) {
            $query .= $this->addStartDateCondition($input['start-date']);
        }
        if (array_key_exists('end-date', $input)) {
            $query .= $this->addEndDateCondition($input['end-date']);;
        }
        if (array_key_exists('zone-name', $input)) {
            $query .= $this->addZoneCondition($input['zone-name']);
        }
        $results = $this->computePercentage($query,'public_assistance');
        return $results;
    }

    public function postPublicAssistance() {
        $input = Input::get();
        $results = $this->computePublicAssistance($input);
        return Response::json(array('count' => $results['count'],
            'percent' => $results['percent'], 'total' => $results['total'],
            'startDate' => $input['start-date'], 'endDate' => $input['end-date']));
    }


    private function numberOfContacts($input) {
        if (array_key_exists('start-date', $input)) {
            $start_date = $input['start-date'] . " 00:00:00";
        } else {
           $start_date = '2011-04-26 00:00:00';
        }

        if (array_key_exists('end-date', $input)) {
            $end_date = $input['end-date'];
        } else {
            $end_date = date('Y-m-d h:m:s');
        }
        $query = 'SELECT count(id) as total FROM clients WHERE created_at >= "' . $start_date  . '" and created_at <= "' . $end_date . '" ';
        if (array_key_exists('zone-name', $input)) {
            $query .= $this->addZoneCondition($input['zone-name']);
        }
        $records = DB::select($query);
        if ($records) {
            $result = $records[0]->total;
        } else {
            $result = 0;
        }
        return $result;
    }


    private function numberOfIntakes($input) {
        if (array_key_exists('start-date', $input)) {
            $start_date = $input['start-date'];
        } else {
            $start_date = '2011-04-26';
        }

        if (array_key_exists('end-date', $input)) {
            $end_date = $input['end-date'];
        } else {
            $end_date = date('Y-m-d');
        }
        $query = 'SELECT count(id) as total FROM clients WHERE date_intake_completed IS NOT NUll and date_intake_completed >= "' . $start_date  . '" and date_intake_completed <= "' . $end_date . '" ';
        if (array_key_exists('zone-name', $input)) {
            $query .= $this->addZoneCondition($input['zone-name']);
        }
        $records = DB::select($query);
        if ($records) {
            $result = $records[0]->total;
        } else {
            $result = 0;
        }
        return $result;
    }

    private function readDemographics($input) {
        $data = array();
        $data['contacts'] = $this->numberOfContacts($input);
        $data['intakes'] = $this->numberOfIntakes($input);
        $data['ages'] = $this->computeAges($input);
        $data['incomes'] = $this->computeIncomes($input);
        $data['ethnicity'] = $this->computeEthnicity($input);
        $data['education'] = $this->computeEducation($input);
        $data['public_assistance'] = $this->computePublicAssistance($input);
        $data['medicaid'] = $this->computeMedicaid($input);
        $data['sex'] = $this->computeSexAssignedAtBirth($input);
        return $data;
    }

    public function postDemographics() {
        $input = Input::get();
        $data = $this->readDemographics($input);
        return Response::json(array('data' => $data,
            'startDate' => $input['start-date'], 'endDate' => $input['end-date']));
    }

    public function getDemographicsPdf($start_date = null,$end_date = null,$age_bins,$income_bins,$zone_name) {
        // The reporting interval is from the start_date through the end_date inclusive.
        // The default will be year-to-date, where the start_date is January 1st and the end_date is today.
        if (is_null($start_date)) {
            $start_date = date('Y') .'-01-01';
        }
        if (is_null($end_date)) {
            $end_date = date('Y-m-d');
        }
        $start_time = $start_date . "00:00:00";
        $end_time = $end_date . "23:59:99";
        $report_time = date('g:i A F j, Y');
        $input = array('start-date' => $start_date, 'end-date' => $end_date, 'zone-name' => $zone_name, 'age_bins' => $age_bins, 'income_bins' => $income_bins);
        $data = $this->readDemographics($input);
        $title = 'TLDEF Name Change Project Demographic Statistics Report';
        $date = new DateTime($input['start-date']);
        $from_date = $date->format('F j, Y');
        $date = new DateTime($input['end-date']);
        $to_date = $date->format('F j, Y');
        $filename = 'Demographics-Statistics-Report-' . $from_date . '-' . $to_date .  '.pdf';
        $pdf = new libraries\tldefNCP\tldefPDF\tldefPDF();
        $pdf->SetHeaderTitle($title);
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetFillColor(153, 214, 213);
        $pdf->SetLineWidth(.2);
        $pdf->SetTextColor(0);
        $pdf->SetFont('Times', '', 9);
        $pdf->SetFont('', 'B');
        $pdf->Cell(180, 5, 'Reporting on data for the period beginning ' . $from_date . ' and ending on ' . $to_date, 0, 1, 'C');

        $pdf->Ln();
        $pdf->SetFont('', 'B');
        $pdf->Cell(120, 5, 'Clients By Sex Assigned At Birth', 0, 1, 'L');
        $pdf->setTableJustification(array('L','C','C'));
        $pdf->setTableWidth(array(60,30,30));
        $pdf->setTableFill(array(true,true,true));
        $pdf->makeTableRow(array('Sex Assigned At Birth','Number of Clients','Percent'));
        $pdf->SetFont('', '');
        $pdf->setTableFill(array(false,false,false));
        $items = $data['sex'];
        foreach ($items['items'] as $item) {
            $pdf->makeTableRow(array($item['label'],$item['value'],$item['percent']));
        }
        $pdf->SetFont('', 'B');
        $pdf->Cell(180, 5, 'Reporting on intake data for ' . $items['count'] . ' total clients.', 0, 1, 'L');

        $pdf->Ln();
        $pdf->SetFont('', 'B');
        $pdf->Cell(90, 5, 'Clients By Age', 0, 1, 'L');
        $pdf->setTableJustification(array('L','C','C'));
        $pdf->setTableWidth(array(30,30,30));
        $pdf->setTableFill(array(true,true,true));
        $pdf->makeTableRow(array('Age Group','Number of Clients','Percent'));
        $pdf->SetFont('', '');
        $pdf->setTableFill(array(false,false,false));
        $ages = $data['ages'];
        foreach ($ages['ages'] as $age) {
            $pdf->makeTableRow(array($age['label'],$age['value'],$age['percent']));
        }
        $pdf->SetFont('', 'B');
        $pdf->Cell(180, 5, 'Reporting on intake data for ' . $ages['total'] . ' total clients.', 0, 1, 'L');

        $pdf->Ln();
        $pdf->SetFont('', 'B');
        $pdf->Cell(90, 5, 'Clients By Income', 0, 1, 'L');
        $pdf->setTableJustification(array('L','C','C'));
        $pdf->setTableWidth(array(30,30,30));
        $pdf->setTableFill(array(true,true,true));
        $pdf->makeTableRow(array('Income Bracket','Number of Clients','Percent'));
        $pdf->SetFont('', '');
        $pdf->setTableFill(array(false,false,false));
        $incomes = $data['incomes'];
        foreach ($incomes['incomes'] as $income) {
            $pdf->makeTableRow(array($income['label'],$income['value'],$income['percent']));
        }
        $pdf->SetFont('', 'B');
        $pdf->Cell(180, 5, 'Reporting on intake data for ' . $incomes['total'] . ' total clients.', 0, 1, 'L');

        $pdf->Ln();
        $pdf->SetFont('', 'B');
        $pdf->Cell(90, 5, 'Clients By Ethnicity', 0, 1, 'L');
        $pdf->setTableJustification(array('L','C','C'));
        $pdf->setTableWidth(array(60,30,30));
        $pdf->setTableFill(array(true,true,true));
        $pdf->makeTableRow(array('Ethnicity','Number of Clients','Percent'));
        $pdf->SetFont('', '');
        $pdf->setTableFill(array(false,false,false));
        $ethnicities = $data['ethnicity'];
        foreach ($ethnicities['items'] as $ethnicity) {
            $pdf->makeTableRow(array($ethnicity['label'],$ethnicity['value'],$ethnicity['percent']));
        }
        $pdf->SetFont('', 'B');
        $pdf->Cell(180, 5, 'Reporting on intake data for ' . $ethnicities['count'] . ' total clients.', 0, 1, 'L');
        $pdf->Cell(180, 5, 'Note: Ethnicity percentages may sum to greater than 100 because some clients specify multiple ethnicities.', 0, 1, 'L');


        $pdf->AddPage($pdf->CurOrientation);

        $pdf->Ln();
        $pdf->SetFont('', 'B');
        $pdf->Cell(120, 5, 'Clients By Educational Levels', 0, 1, 'L');
        $pdf->setTableJustification(array('L','C','C'));
        $pdf->setTableWidth(array(60,30,30));
        $pdf->setTableFill(array(true,true,true));
        $pdf->makeTableRow(array('Education','Number of Clients','Percent'));
        $pdf->SetFont('', '');
        $pdf->setTableFill(array(false,false,false));
        $items = $data['education'];
        foreach ($items['items'] as $item) {
            $pdf->makeTableRow(array($item['label'],$item['value'],$item['percent']));
        }
        $pdf->SetFont('', 'B');
        $pdf->Cell(180, 5, 'Reporting on intake data for ' . $items['count'] . ' total clients.', 0, 1, 'L');

        $pdf->Ln();
        $pdf->SetFont('', 'B');
        $pdf->Cell(60, 5, 'Clients Receiving Public Assistance', 0, 1, 'L');
        $pdf->setTableJustification(array('C','C'));
        $pdf->setTableWidth(array(30,30));
        $pdf->setTableFill(array(true,true));
        $pdf->makeTableRow(array('Number of Clients','Percent'));
        $pdf->SetFont('', '');
        $pdf->setTableFill(array(false,false));
        $items = $data['public_assistance'];
        $pdf->makeTableRow(array($items['count'],$items['percent']));
        $pdf->SetFont('', 'B');
        $pdf->Cell(180, 5, 'Reporting on intake data for ' . $items['total'] . ' total clients.', 0, 1, 'L');

        $pdf->Ln();
        $pdf->SetFont('', 'B');
        $pdf->Cell(60, 5, 'Clients Using Medicaid', 0, 1, 'L');
        $pdf->setTableJustification(array('C','C'));
        $pdf->setTableWidth(array(30,30));
        $pdf->setTableFill(array(true,true));
        $pdf->makeTableRow(array('Number of Clients','Percent'));
        $pdf->SetFont('', '');
        $pdf->setTableFill(array(false,false));
        $items = $data['medicaid'];
        $pdf->makeTableRow(array($items['count'],$items['percent']));
        $pdf->SetFont('', 'B');
        $pdf->Cell(180, 5, 'Reporting on intake data for ' . $items['total'] . ' total clients.', 0, 1, 'L');

        $pdf->Output($filename, 'I');
    }

    private function makePDF($data) {

        $pdf = new libraries\tldefNCP\tldefPDF\tldefPDF();
        $pdf->SetHeaderTitle($data['title']);
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetFillColor(153, 214, 213);
        $pdf->SetLineWidth(.2);
        $pdf->SetTextColor(0);
        $pdf->SetFont('Times', '', 9);
        $pdf->Cell(180, 5, 'All Contacts, Intakes, and Assignments occurred during the period beginning ' . $data['start date'] . ' and ending on ' . $data['end date'],0, 1, 'L');
        $pdf->Cell(180, 5, 'The Open Cases and Clients Waiting statistics are current as of ' . $data['report time'] . '.',0, 1, 'L');

        $pdf->Ln();
        $pdf->SetFont('', 'B');
        $pdf->Cell(150, 5, 'Name Change Project Statistics By Region', 0, 1, 'C');
        $pdf->Ln();
        $pdf->setTableJustification(array('L','C','C','C','C'));
        $pdf->setTableWidth(array(30,30,30,30,30));
        $pdf->setTableFill(array(true,true,true,true,true));
        $pdf->makeTableRow(array('Region','Contacts','Intakes','Assignments','Clients Waiting'));
        $pdf->SetFont('', '');
        $pdf->setTableFill(array(false,false,false,false,false));
        foreach ($data['regional statistics'] as $key => $value) {
            $pdf->makeTableRow(array($key,$value['contacts'],$value['intakes'],$value['assignments'],$value['waiting']));
        }
        $pdf->SetFont('', 'B');
        $pdf->setTableFill(array(true,true,true,true,true));
        $pdf->makeTableRow(array('Totals',$data['totals']['contacts'],$data['totals']['intakes'],$data['totals']['assignments'],$data['totals']['waiting']));
        $pdf->AddPage($pdf->CurOrientation);
        $pdf->SetFont('', 'B');
        $pdf->Cell(150, 5, 'Name Change Project Statistics By Region And Firm', 0, 1, 'C');
        $pdf->Ln();
        $pdf->setTableJustification(array('L','L','C','C'));
        $pdf->setTableWidth(array(30,90,30,30));
        $pdf->setTableFill(array(true,true,true,true));
        $pdf->makeTableRow(array('Region','Firm','Assignments','Open Cases'));
        $pdf->setTableFill(array(false,false,false,false));
        $pdf->setTableJustification(array('L','L','C','C'));
        foreach ($data['firm statistics'] as $region => $firms) {
            $pdf->SetFont('', 'B');
            $pdf->makeTableRow(array($region,' ',' ',' '));
            $pdf->SetFont('', '');
            foreach ($firms as $firm_name => $values) {
                $pdf->makeTableRow(array(' ',$firm_name,$values['assignments'],$values['in progress']));
            }
        }
        $pdf->SetFont('', 'B');
        $pdf->setTableJustification(array('L','L','C','C'));
        $pdf->setTableWidth(array(30,90,30,30));
        $pdf->setTableFill(array(true,true,true,true));
        $pdf->makeTableRow(array('Totals',' ',$data['totals']['assignments'],$data['totals']['in progress']));
        $pdf->SetFont('', '');

        return $pdf;
    }
}