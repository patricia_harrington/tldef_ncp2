<?php
/**
 * Filename: FirmsController.php
 * Author: Patricia Harrington
 * Created: 12/21/12 1:55 PM
 * Copyright 2012 Transgender Legal Defense & Education Fund, Inc.
 */
class FirmsController extends BaseController {

    public function getIndex() {
        return $this->getList();
    }

    public function getList() {
        $results = array();
        $firms = Firm::all();
        foreach ($firms as $firm) {
            $record = array();
            $record['id'] = $firm->id;
            $record['name'] = trim($firm->full_name);
            $record['donor_level_id'] = $firm->donor_level_id;
            $donor_level = DonorLevel::Find($firm->donor_level_id);
            $record['donor_level_name'] = $donor_level->name;
            $record['is_active'] = $firm->is_active;
            $results[] = $record;
        }
        return Response::json(array('firms' => $results));
    }

    public function getSortedList() {
        $results = array();
        $firms = Firm::orderBy('full_name','asc')->get();
        foreach ($firms as $firm) {
            $record = array();
            $record['id'] = $firm->id;
            $record['name'] = trim($firm->full_name);
            $record['donor_level_id'] = $firm->donor_level_id;
            $donor_level = DonorLevel::Find($firm->donor_level_id);
            $record['donor_level_name'] = $donor_level->name;
            $record['is_active'] = $firm->is_active;
            $results[] = $record;
        }
        return Response::json(array('firms' => $results));
    }

    public function getId($id) {
        $result = null;
        $record = Firm::find($id);
        if (!is_null($record)) {
            $result = $record->toArray();
            $result['contacts'] = $this->getContactsArray($id);
        }
        return Response::json(array('firm' => $result));
    }

    private function getContactsArray($id) {
        $results = array();
        $contacts = Attorney::where('firm_id','=', $id)
            ->where('is_firm_contact','=','1')
            ->where('is_active','=','1')->get();
        if (!is_null($contacts)) {
            $i = 0;
            foreach ($contacts as $contact) {
                $results[] = $contact->toArray();
                $zone = $contact->zone()->first();
                if (!is_null($zone)) {
                    $results[$i]['zone_name'] = $zone->name;
                } else {
                    $results[$i]['zone_name'] = 'All';
                }
                $i = $i + 1;
            }
        }
        return $results;
    }

    public function getContacts($id) {
        return Response::json(array('firm-contacts' => $this->getContactsArray($id)));
    }

    public function postDelete() {
        $status = 0;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $record = Firm::find($input['id']);
            if (!is_null($record)) {
                $status = $record->delete();
            }
        }
        return Response::json(array('status' => $status));
    }

    public function postUpdate() {
        $result = null;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $id = $input['id'];
            $record = Firm::find($id);
            if (!is_null($record)) {
                unset($input['contacts']);
                if ($record->update($input) != 0) {
                    $record = Firm::find($id);
                    $result = $record->toArray();
                    $result['contacts'] = $this->getContactsArray($id);
                }
            }
        }
        return Response::json(array('firm' => $result));
    }

    public function postAdd() {
        $result = null;
        $input = Input::get();
        $firm = new Firm();
        $record = $firm->create($input);
        if ($record) {
            $result = $record->toArray();
            $result['contacts'] = $this->getContactsArray($record->id);
            $result['created_at'] = $record->created_at->format('Y/m/d H:i:s');
            $result['updated_at'] = $record->updated_at->format('Y/m/d H:i:s');
        }
        return Response::json(array('firm' => $result));
    }
}
