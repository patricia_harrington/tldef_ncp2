<?php

/**
 * Filename: DonorLevelsController.php
 * Author: Patricia Harrington
 * Created: 9/24/15 3:19 PM
 * Copyright 2015 Transgender Legal Defense & Education Fund, Inc.
 */
class DonorLevelsController extends BaseController {

    public function getIndex() {
        return $this->getList();
    }

    public function getList() {
        $results = array();
        $levels = DonorLevel::orderBy('rank', 'asc')->get();
        foreach ($levels as $level) {
            $record = array();
            $record['id'] = $level->id;
            $record['name'] = trim($level->name);
            $record['rank'] = $level->rank;
            $results[] = $record;
        }
        return Response::json(array('levels' => $results));
    }

    public function getId($id) {
        $result = null;
        $record = DonorLevel::find($id);
        if (!is_null($record)) {
            $result = $record->toArray();
        }
        return Response::json(array('level' => $result));
    }

    public function postDelete() {
        $status = 0;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $record = DonorLevel::find($input['id']);
            if (!is_null($record)) {
                $status = $record->delete();
            }
        }
        return Response::json(array('status' => $status));
    }

    public function postUpdate() {
        $result = null;
        $input = Input::get();
        if (array_key_exists('id', $input)) {
            $id = $input['id'];
            $record = DonorLevel::find($id);
            if (!is_null($record)) {
                if ($record->update($input) != 0) {
                    $record = DonorLevel::find($id);
                    $result = $record->toArray();
                }
            }
        }
        return Response::json(array('level' => $result));
    }

    public function postAdd() {
        $result = null;
        $input = Input::get();
        $level = new DonorLevel();
        $record = $level->create($input);
        if ($record) {
            $result = $record->toArray();
            $result['created_at'] = $record->created_at->format('Y/m/d H:i:s');
            $result['updated_at'] = $record->updated_at->format('Y/m/d H:i:s');
        }
        return Response::json(array('level' => $result));
    }



}