<?php
/**
 * Filename: Test.php
 * Author: Patricia Harrington
 * Created: 8/14/14 12:42 PM
 * Copyright 2014 Transgender Legal Defense & Education Fund, Inc.
 */

namespace libraries\tldefNCP\Test;


class Test {
    function helloWorld() {
        return "Hello World!";
    }
} 