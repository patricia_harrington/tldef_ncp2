<?php
/**
 * Filename: tldefPDF.php
 * Author: Patricia Harrington
 * Created: 8/12/14 3:35 PM
 * Copyright 2014 Transgender Legal Defense & Education Fund, Inc.
 */

namespace libraries\tldefNCP\tldefPDF;

class tldefPDF extends \fpdf\FPDF {

    var $headerTitle;

    var $tableFills;
    var $tableJustifications;
    var $tableWidths;

    function SetHeaderTitle($title) {
        $this->headerTitle = $title;
    }

    function NumberOfLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw =& $this->CurrentFont['cw'];
        if ($w == 0) {
            $w = $this->w - $this->rMargin - $this->x;
        }
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n") {
            $nb--;
        }
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ') {
                $sep = $i;
            }
            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j) {
                        $i++;
                    }
                } else {
                    $i = $sep + 1;
                }
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else {
                $i++;
            }
        }
        return $nl;
    }

    function CheckPageBreak($height)
    {
        //If the height would cause an overflow, add a new page immediately
        if($this->GetY()+$height>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    // Page header
    function Header() {
        $page_number = $this->PageNo();
        if ($page_number === 1) {
            // Logo
            $logo  = base_path() . '/tldef_logo.jpg';
            $this->Image($logo, 70, null, 60);
            $this->SetFont('Times', 'B', 12);
            // Title
            $this->Cell(180, 10, $this->headerTitle, 0, 0, 'C');
            $this->Ln(10);
        } else {
            $this->SetFont('Times', 'B', 10);
            $this->Cell(140, 10, $this->headerTitle, 0, 0, 'L');
            $this->Cell(40, 10, 'Page ' . $page_number . ' of {nb}', 0, 0, 'R');
            $this->Ln(10);
        }

    }

    // Page footer
    function Footer() {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Times', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . ' of {nb}', 0, 0, 'R');
    }

    // Inline label and single line text
    function inlineBox($data, $labelWidth, $textWidth) {
        $baseHeight = 5;
        $this->SetFont('', 'B');
        $this->Cell($labelWidth, $baseHeight, $data[0], 1, 0, 'L', true);
        $this->SetFont('', 'B');
        $this->Cell($textWidth, $baseHeight, $data[1], 1, 'J');
        $this->Ln();
        $this->Ln();
    }

    // Single column - Multiple lines of text
    function singleColumnBox($data) {
        $baseHeight = 5;
        $baseWidth = 180;
        $height = $baseHeight*($this->NumberOfLines(0, $data[1]) + 1);
        $this->CheckPageBreak($height);
        $this->SetFont('', 'B');
        $this->MultiCell($baseWidth, $baseHeight, $data[0], 1, 'L', true);
        $this->SetFont('', '');
        $this->MultiCell($baseWidth, $baseHeight, $data[1], 1, 'J');
        $this->Ln();
    }

    function multiColumnBox($data) {
        $baseHeight = 5;
        $this->CheckPageBreak($baseHeight*2);
        $this->SetFont('', 'B');
        $last_column = sizeof($data) - 1;
        for ($i = 0; $i < $last_column; $i++) {
            $this->Cell($data[$i][2], $baseHeight, $data[$i][0], 1, 0, 'L', true);
        }
        $this->Cell($data[$last_column][2], $baseHeight, $data[$last_column][0], 1, 1, 'L', true);
        $this->SetFont('', '');
        for ($i = 0; $i < $last_column; $i++) {
            $this->Cell($data[$i][2], $baseHeight, $data[$i][1], 1, 0, 'J');
        }
        $this->Cell($data[$last_column][2], $baseHeight, $data[$last_column][1], 1, 1, 'J');
        $this->Ln();
    }

    function makeColumn($data,$width) {
        $result = array();
        $result[] = $data[0];
        $result[] = $data[1];
        $result[] = $width;
        return $result;
    }

    function setTableJustification($data) {
        $this->tableJustifications = $data;
    }
    function setTableWidth($data) {
        $this->tableWidths = $data;
    }
    function setTableFill($data) {
        $this->tableFills = $data;
    }

    function makeTableRow($data) {
        $baseHeight = 5;
        $this->CheckPageBreak($baseHeight*2);
        $i = 0;
        foreach ($data as $datum) {
            $this->Cell($this->tableWidths[$i], $baseHeight, $datum, 1, 0, $this->tableJustifications[$i],$this->tableFills[$i]);
            $i = $i + 1;
        }
        $this->Ln();
    }
}
