<?php
/**
 * Filename: tldefHistogram.php
 * Author: Patricia Harrington
 * Created: 8/20/14 12:30 PM
 * Copyright 2014 Transgender Legal Defense & Education Fund, Inc.
 */

namespace libraries\tldefNCP\tldefHistogram;

class tldefHistogram
{
    protected $values;
    protected $bins;
    protected $labels;

    public function __construct($bins) {
        if (! is_array($bins)) {
            throw new Exception('tldefHistogram: Invalid argument, failed to initialize bin array.');
        } else {
            $this->values = array();
            $this->bins = array();
            foreach ($bins as $bin) {
                array_push($this->bins,$bin);
                $this->values[$bin] = 0;
            }
            $this->labels = $this->makeBinLabels($this->bins);
        }
    }

    private function makeBinLabels($bins) {
        $labels = array();
        if (!is_null($bins)) {
            $n = count($bins) - 1;
            if ($n > 0) {
                $labels[] = sprintf('%s and under', number_format($bins[0]));
                for ($i = 1; $i < $n; $i++) {
                    $labels[] = sprintf('%s-%s', number_format($bins[$i]), number_format($bins[$i+1]-1));
                }
                $labels[] = sprintf('%s and over', number_format($bins[$n]));
            }
        }
        return $labels;
    }

    public function add($value) {
        $bottom = $this->bins[0];
        if ($value < $bottom) {
            $this->values[$bottom]++;
        } else {
            $found = false;
            $end = count($this->bins) - 1;
            for ($i = 0; $i < $end; $i++ ) {
                $lower = $this->bins[$i];
                $upper = $this->bins[$i+1];
                if (($value >= $lower) and ($value < $upper)) {
                    $this->values[$lower]++;
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $top = $this->bins[$end];
                $this->values[$top]++;
            }
        }
    }
 
    public function addArray(array $array, $key = false) {
        if ($key) $array = array_filter(array_map(function($i)use($key){return isset($i[$key])?$i[$key]:false;},$array));
        array_walk($array,array($this,'add'));
    }
 
    public function addObjects(array $array, $prop) {
        $array = array_filter(array_map(function($i)use($prop){return isset($i->$prop)?$i->$prop:false;},$array));
        $this->addArray($array);
    }
 
    public function getValues() {
        return $this->values;
    }

    public function getTotal() {
        $total = 0;
        foreach ($this->values as $value) {
            $total += $value;
        }
        return $total;
    }

    public function getResults() {
        $results = array();
        $total = $this->getTotal();
        $i = 0;
        foreach ($this->values as $value) {
            if ($total > 0) {
                $percent = Round((($value / $total) * 100),1);
            } else {
                $percent = 0;
            }
            $results[] = array ('label' => $this->labels[$i], 'value' => $value, 'percent' => $percent);
            $i++;
        }
        return $results;
    }
 
    public function sort($inverse = false)
    {
        if ($inverse) arsort($this->values);
        else asort($this->values);
    }
}