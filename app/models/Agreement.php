<?php
/**
 * Filename: Agreement.php
 * Author: Patricia Harrington
 * Created: 3/31/14 11:49 AM
 * Copyright 2014 Transgender Legal Defense & Education Fund, Inc.
 */

class Agreement extends Eloquent {

    protected $guarded = array('id', 'created_at', 'updated_at');

    protected $touches = array('client');

    public function client()
    {
        return $this->belongsTo('Client');
    }

} 