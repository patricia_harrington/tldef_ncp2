<?php
/**
 * Filename: Client.php
 * Author: Patricia Harrington
 * Created: 12/19/12 12:51 PM
 * Copyright 2012 Transgender Legal Defense & Education Fund, Inc.
 */
class Client extends Eloquent {

    protected $guarded = array('id', 'created_at', 'updated_at');

    public function clientAgreement()
    {
        return $this->hasMany('Agreement');
    }
    public function clientDetail()
    {
        return $this->hasMany('ClientDetail');
    }
    public function clientCase()
    {
        return $this->hasOne('ClientCase');
    }
    public function ethnicity()
    {
        return $this->belongsToMany('Ethnicity')->withTimestamps();
    }
    public function firm()
    {
        return $this->belongsToMany('Firm')->withTimestamps();
    }
    public function attorney()
    {
        return $this->belongsToMany('Attorney')->withTimestamps();
    }
    public function referrer()
    {
        return $this->hasOne('Referrer');
    }
    public function name() {
        return rtrim(trim($this->preferred_first_name . ' ' . $this->preferred_last_name),' ');
    }

    static function guid() {
        $s = strtoupper(md5(uniqid(rand(),true)));
        $result =
            substr($s,0,8) . '-' .
            substr($s,8,4) . '-' .
            substr($s,12,4). '-' .
            substr($s,16,4). '-' .
            substr($s,20);
        return $result;
    }
}