<?php

/**
 * Filename: DonorLevel.php
 * Author: Patricia Harrington
 * Created: 9/24/15 3:04 PM
 * Copyright 2015 Transgender Legal Defense & Education Fund, Inc.
 */
class DonorLevel extends Eloquent {

    protected $table = 'donor_levels';

    protected $guarded = array('id', 'created_at', 'updated_at');

    public function Firm()
    {
        return $this->hasMany('Firm');
    }

}