<?php
/**
 * Filename: Firm.php
 * Author: Patricia Harrington
 * Created: 12/19/12 12:59 PM
 * Copyright 2012 Transgender Legal Defense & Education Fund, Inc.
 */
class Firm extends Eloquent {

    protected $guarded = array('id', 'created_at', 'updated_at');

    public function client() {
        return $this->belongsToMany('Client');
    }

    public function attorney() {
        return $this->hasMany('Attorney');
    }
    public function donorLevel()
    {
        return $this->belongsTo('DonorLevel');
    }

}