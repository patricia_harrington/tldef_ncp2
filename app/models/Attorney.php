<?php
/**
 * Filename: Attorney.php
 * Author: Patricia Harrington
 * Created: 12/19/12 1:16 PM
 * Copyright 2012 Transgender Legal Defense & Education Fund, Inc.
 */
class Attorney extends Eloquent {

    protected $guarded = array('id', 'created_at', 'updated_at');

    public function firm()
    {
        return $this->belongsTo('Firm');
    }
    public function client()
    {
        return $this->belongsToMany('Client')->withTimestamps();
    }
    public function fullName() {
        return rtrim(trim($this->last_name . ', ' . $this->first_name . ' ' . $this->middle_name),',');
    }
    public function Name() {
        return rtrim(trim($this->first_name . ' ' . $this->middle_name . ' ' . $this->last_name),' ');
    }
    public function zone()
    {
        return $this->belongsTo('Zone');
    }
}