<?php
/**
 * Filename: ClientDetail.php
 * Author: Patricia Harrington
 * Created: 12/19/12 2:11 PM
 * Copyright 2012 Transgender Legal Defense & Education Fund, Inc.
 */
class ClientDetail extends Eloquent {

    protected $table = 'client_details';

    protected $touches = array('client');

    protected $guarded = array('id', 'created_at', 'updated_at');

    public function client()
    {
        return $this->belongsTo('Client');
    }
}