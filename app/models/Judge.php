<?php
/**
 * Filename: Judge.php
 * Author: Patricia Harrington
 * Created: 2/13/14 11:18 AM
 * Copyright 2014 Transgender Legal Defense & Education Fund, Inc.
 */

class Judge extends Eloquent {

    protected $guarded = array('id', 'created_at', 'updated_at');

}

