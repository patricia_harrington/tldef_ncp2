<?php
/**
 * Filename: Zone.php
 * Author: Patricia Harrington
 * Created: 8/8/13 5:37 PM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */

class Zone extends Eloquent {

    protected $guarded = array('id', 'created_at', 'updated_at');

    public function attorney() {
        return $this->belongsToMany('Attorney');
    }

}