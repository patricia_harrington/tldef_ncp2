<?php
/**
 * Filename: State.php
 * Author: Patricia Harrington
 * Created: 11/4/12 1:59 PM
 * Copyright 2012 Transgender Legal Defense & Education Fund, Inc.
 */
include_once "markdown.php";
include_once "smartypants.php";


class State extends Eloquent {

    protected $table = 'state_procedures';

    protected $guarded = array('id', 'created_at', 'updated_at');

    public function addData($data)
    {
        if (array_key_exists('nc_prelude', $data)) {
            $this->nc_prelude = $data['nc_prelude'];
        }
        if (array_key_exists('nc_legal', $data)) {
            $this->nc_legal = $data['nc_legal'];
        }
        if (array_key_exists('nc_drivers_license', $data)) {
            $this->nc_drivers_license = $data['nc_drivers_license'];
        }
        if (array_key_exists('nc_birth_certificate', $data)) {
            $this->nc_birth_certificate = $data['nc_birth_certificate'];
        }
        if (array_key_exists('nc_postscript', $data)) {
            $this->nc_postscript = $data['nc_postscript'];
        }
        if (array_key_exists('gc_prelude', $data)) {
            $this->gc_prelude = $data['gc_prelude'];
        }
        if (array_key_exists('gc_legal', $data)) {
            $this->gc_legal = $data['gc_legal'];
        }
        if (array_key_exists('gc_drivers_license', $data)) {
            $this->gc_drivers_license = $data['gc_drivers_license'];
        }
        if (array_key_exists('gc_birth_certificate', $data)) {
            $this->gc_birth_certificate = $data['gc_birth_certificate'];
        }
        if (array_key_exists('gc_postscript', $data)) {
            $this->gc_postscript = $data['gc_postscript'];
        }
        $this->save();
        return $this;
    }

    public function getData() {
        $data = array(
            'id' => $this->id,
            'state_name' => $this->state_name,
            'nc_prelude' => $this->nc_prelude,
            'nc_legal' => $this->nc_legal,
            'nc_drivers_license' => $this->nc_drivers_license,
            'nc_birth_certificate' => $this->nc_birth_certificate,
            'nc_postscript' => $this->nc_postscript,
            'gc_prelude' => $this->gc_prelude,
            'gc_legal' => $this->gc_legal,
            'gc_drivers_license' => $this->gc_drivers_license,
            'gc_birth_certificate' => $this->gc_birth_certificate,
            'gc_postscript' => $this->gc_postscript,
            'updated_at' => $this->updated_at
        );
        return $data;
    }

    public function getHtmlSection($section) {
        switch ($section) {
            case "nc_prelude":
                $result = Markdown(SmartyPants($this->nc_prelude));
                break;
            case "nc_legal":
                $result = Markdown(SmartyPants($this->nc_legal));
                break;
            case "nc_birth_certificate":
                $result = Markdown(SmartyPants($this->nc_birth_certificate));
                break;
            case "nc_drivers_license":
                $result = Markdown(SmartyPants($this->nc_drivers_license));
                break;
            case "nc_postscript":
                $result = Markdown(SmartyPants($this->nc_postscript));
                break;
            case "gc_prelude":
                $result = Markdown(SmartyPants($this->gc_prelude));
                break;
            case "gc_legal":
                $result = Markdown(SmartyPants($this->gc_legal));
                break;
            case "gc_birth_certificate":
                $result = Markdown(SmartyPants($this->gc_birth_certificate));
                break;
            case "gc_drivers_license":
                $result = Markdown(SmartyPants($this->gc_drivers_license));
                break;
            case "nc_postscript":
                $result = Markdown(SmartyPants($this->nc_postscript));
                break;
            default:
                $result = '';
        }
        return $result;
    }

    public function getHtml($selector) {
        $include_non_birth_certificate = true;
        $include_birth_certificate = true;
        $include_name_change = true;
        $include_gender_change = true;
        if (!is_null($selector)) {
            if ( strcmp($selector, 'birth-certificate-only') == 0)  {
                $include_non_birth_certificate = false;
            } elseif ( strcmp($selector, 'no-birth-certificate') == 0)  {
                $include_birth_certificate = false;
            } elseif ( strcmp($selector, 'name-change-only') == 0)  {
                $include_gender_change = false;
            } elseif ( strcmp($selector, 'gender-change-only') == 0 ) {
                $include_name_change = false;
            }
        }
        $str = "";
        $state_name = $this->state_name;
        $img_file_name = str_ireplace(" ", "_", strtolower($state_name));
        $str .= "<div class='state_header'>\n";
        $str .= "<h1><img class='state_seal' src='http://www.transgenderlegal.org/states/img/" . $img_file_name . ".png' height='120' width='120' alt='" . $state_name . " state seal' /> "  . $state_name . "</h1>\n";
        $str .= "</div>\n";
        if ($include_name_change)  {
            $str .= "<div class='state name_change'>\n";
            $str .= "<h2>Name Change</h2>\n";
            $str .=  Markdown(SmartyPants($this->nc_prelude));
            if ($include_non_birth_certificate) {
                $str .=  Markdown(SmartyPants($this->nc_legal));
            }
            if ($include_birth_certificate) {
                $str .=  Markdown(SmartyPants($this->nc_birth_certificate));
            }
            if ($include_non_birth_certificate) {
                $str .=  Markdown(SmartyPants($this->nc_drivers_license));
            }
            $str .=  Markdown(SmartyPants($this->nc_postscript));
            $str .= "</div>";
        }
        if ($include_gender_change) {
            $str .= "<div class='state gender_change'>";
            $str .= "<h2>Sex Designation Change</h2>";
            $str .= Markdown(SmartyPants($this->gc_prelude));
            if ($include_non_birth_certificate) {
                $str .= Markdown(SmartyPants($this->gc_legal));
            }
            if ($include_birth_certificate) {
                $str .= Markdown(SmartyPants($this->gc_birth_certificate));
            }
            if ($include_non_birth_certificate) {
                $str .= Markdown(SmartyPants($this->gc_drivers_license));
            }
            $str .= Markdown(SmartyPants($this->gc_postscript));
            $str .= "</div>\n";
        }
        return $str;
    }
}