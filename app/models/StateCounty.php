<?php
/**
 * Filename: StateCounty.php
 * Author: Patricia Harrington
 * Created: 5/31/13 12:02 AM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */

class StateCounty extends Eloquent {

    protected $table = 'state_counties';

    protected $guarded = array('id', 'created_at', 'updated_at');

    public function zone() {
        return $this->hasOne('Zone');
    }
}