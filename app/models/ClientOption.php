<?php
/**
 * Filename: ClientOption.php
 * Author: Patricia Harrington
 * Created: 9/18/13 1:48 PM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */

class ClientOption extends Eloquent {

    protected $table = 'client_options';

    protected $guarded = array('id', 'created_at', 'updated_at');


}