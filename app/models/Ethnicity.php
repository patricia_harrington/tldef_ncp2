<?php
/**
 * Filename: Ethnicity.php
 * Author: Patricia Harrington
 * Created: 11/15/13 5:25 PM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */

class Ethnicity extends Eloquent {

    protected $table = 'ethnicities';

    protected $guarded = array('id', 'created_at', 'updated_at');

    public function client()
    {
        return $this->belongsToMany('Client')->withTimestamps();
    }

} 