<?php
/**
 * Filename: ClientField.php
 * Author: Patricia Harrington
 * Created: 9/18/13 1:47 PM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */

class ClientField extends Eloquent {

    protected $table = 'client_fields';

    protected $guarded = array('id', 'created_at', 'updated_at');

}