<?php
/**
 * Filename: ClientCase.php
 * Author: Patricia Harrington
 * Created: 12/19/12 1:44 PM
 * Copyright 2012 Transgender Legal Defense & Education Fund, Inc.
 */
class ClientCase extends Eloquent {

    protected $table = 'client_cases';

    protected $touches = array('client');

    protected $primaryKey = 'case_id';

    protected $guarded = array('case_id', 'created_at', 'updated_at');

    public function client()
 	{
 		return $this->belongsTo('Client');
 	}
}