<?php
/**
 * Filename: StateInfo.php
 * Author: Patricia Harrington
 * Created: 5/31/13 12:00 AM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */

class StateName extends Eloquent {

    protected $table = 'state_names';

    protected $guarded = array('id', 'created_at', 'updated_at');
}