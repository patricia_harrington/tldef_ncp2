<!DOCTYPE html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="http://www.transgenderlegal.org/favicon.ico" />
<!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css">-->
<link rel="stylesheet" href="/css/jquery-ui-1.10.1.custom.min.css"/>
<link rel="stylesheet" href="/css/bootstrap.min.css"/>
<link rel="stylesheet" href="/css/bootstrap-responsive.css">
<html lang="en">
<head>
<title>Regional Statistics Report</title>
</head>
<body class="container">
<h1 class="well" style="background-color: #e1fbfa;">TLDEF Name Change Project <small>Regional Statistics Report</small></h1>
<p>All Contacts, Intakes, and Assignments occurred during the period beginning <% $data['start date'] %> and ending on  <% $data['end date'] %>.</p>
<p>The Open Cases and Clients Waiting statistics are current as of <% $data['report time'] %>. </p>
<div class="row">
<div class="col-sm-6">
<table class="table table-bordered table-hover table-condensed">
<caption style="font-weight:bold;">Name Change Project Statistics By Region</caption>
<thead>
<tr>
    <th>Region</th>
    <th>Contacts</th>
    <th>Intakes</th>
    <th>Assignments</th>
    <th>Clients Waiting</th>
</tr>
</thead>
<tbody>
@foreach ($data['regional statistics'] as $key => $value)
<tr><td><% $key %></td>
    <td style="text-align:center;"><% $value['contacts'] %></td>
    <td style="text-align:center;"><% $value['intakes'] %></td>
    <td style="text-align:center;"><% $value['assignments'] %></td>
    <td style="text-align:center;"><% $value['waiting'] %></td>
</tr>
@endforeach
<tr style="font-weight:bold;">
    <td>Totals</td>
    <td style="text-align:center;"><% $data['totals']['contacts'] %></td>
    <td style="text-align:center;"><% $data['totals']['intakes'] %></td>
    <td style="text-align:center;"><% $data['totals']['assignments'] %></td>
    <td style="text-align:center;"><% $data['totals']['waiting'] %></td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="row">
    <div class="col-sm-10">
        <table class="table table-bordered table-hover table-condensed">
            <caption style="font-weight:bold;">Name Change Project Statistics By Region And Firm</caption>
            <thead>
            <tr>
                <th>Region</th>
                <th>Firm</th></th>
                <th>Assignments</th>
                <th>Open Cases</th></tr>
            </thead>
            <tbody>
            @foreach ($data['firm statistics'] as $region => $firms)
            <tr style="background-color: #e1fbfa;">
                <td><% $region %></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @foreach ($firms as $firm_name => $values)
            <tr>
                <td></td>
                <td><% $firm_name %></td>
                <td style="text-align:center;"><% $values['assignments'] %></td>
                <td style="text-align:center;"><% $values['in progress'] %></td>
            </tr>
            @endforeach
            @endforeach
            <tr style="font-weight:bold;">
                <td>Totals</td>
                <td></td>
                <td style="text-align:center;"><% $data['totals']['assignments'] %></td>
                <td style="text-align:center;"><% $data['totals']['in progress'] %></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>