<!doctype html>
<html lang="en" class="no-js" @yield('page-app')>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    @section('styles')
        <link rel="shortcut icon" href="http://www.transgenderlegal.org/favicon.ico"/>
        <!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css">-->
        <link rel="stylesheet" href="/css/jquery-ui-1.10.1.custom.min.css"/>
        <link rel="stylesheet" href="/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/css/ng-grid.min.css"/>
        <link rel="stylesheet" href="/css/app.css"/>
        @yield('styles')
        <script src="/js/lib/modernizr-2.6.2.min.js"></script>
</head>
<body ng-controller="PageCtrl" class="ng-cloak" ng-init="role='<%$role%>'" @yield('body-attributes')>
@section('header')
    <div class="page-header" ng-show="showHeader">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" ng-init="isCollapsed = true" ng-click="isCollapsed = !isCollapsed"
                            data-target="#navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse navbar-inner" ng-class="{collapse: isCollapsed}" id="navbar-collapse-1">
                    @if ($role != '')
                        <ul class="nav navbar-nav">
                            @if ($role == 'Administrator' || $role == 'User')
                                @if ($active == 'Clients')
                                    <li class="active"><a ng-href="/">Clients <span class="sr-only">(current)</span></a></li>
                                @else
                                    <li><a ng-href="/">Clients</a></li>
                                @endif
                                @if ($active == 'Firms')
                                    <li class="active"><a ng-href="/firm-editor">Firms</a></li>
                                @else
                                    <li><a ng-href="/firm-editor">Firms</a></li>
                                @endif
                                @if ($active == 'Attorneys')
                                    <li class="active"><a ng-href="/attorney-editor">Attorneys</a></li>
                                @else
                                    <li><a ng-href="/attorney-editor">Attorneys</a></li>
                                @endif
                                @if ($active == 'Judges')
                                    <li class="active"><a ng-href="/judge-editor">Judges</a></li>
                                @else
                                    <li><a ng-href="/judge-editor">Judges</a></li>
                                @endif
                                @if ($active == 'Referrers')
                                    <li class="active"><a ng-href="/referrer-editor">Referrers</a></li>
                                @else
                                    <li><a ng-href="/referrer-editor">Referrers</a></li>
                                @endif
                                @if ($active == 'Reports')
                                    <li class="active"><a ng-href="/reports-page">Reports</a></li>
                                @else
                                    <li><a ng-href="/reports-page">Reports</a></li>
                                @endif
                            @endif
                            @if ($role == 'Administrator' || $role == 'User' || $role == 'Editor' )
                                @if ($active == 'States')
                                    <li class="active"><a ng-href="/state-editor">States</a></li>
                                @else
                                    <li><a ng-href="/state-editor">States</a></li>
                                @endif
                            @endif
                            <%--
                            @if ($active == 'Preview')
                                <li class="active"><a ng-href="/states">Preview</a></li>
                            @else
                                <li><a ng-href="/states-preview">Preview</a></li>
                            @endif
                            --%>
                            @if ($role == 'Administrator')
                                @if ($active == 'Users')
                                    <li class="active"><a ng-href="/user-editor"">Users</a></li>
                                @else
                                    <li><a ng-href="/user-editor">Users</a></li>
                                @endif
                            @endif
                            <li><a ng-href="/logout">Logout</a></li>
                        </ul>
                    @endif
                </div>
            </div>
        </nav>
        @if ($active != 'States')
            <h1 class="hidden-xs">TLDEF Name Change Project
                <small>@yield('sub_title')</small>
            </h1>
            <p class="visible-xs"><strong>TLDEF Name Change Project</strong>  @yield('sub_title')</p>
        @endif
        @yield('header')
    </div>
    @yield('content')

@section('scripts')

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/lib/jquery-2.0.3.min.js"><\/script>')</script>
    <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
    <script>window.jQuery || document.write('<script src="/js/lib/jquery-ui-1.10.1.custom.min.js"><\/script>')</script>
    <script src="/js/lib/underscore.min.js"></script>

    <!--
   <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.1.4/angular.min.js"></script>
   <script>window.jQuery || document.write('<script src="/js/lib/angular-1.1.4/angular.min.js"><\/script>')</script>
   <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.1.4/angular-resource.min.js">
   <script>window.jQuery || document.write('<script src="/js/lib/angular-1.1.4/angular-resource.min.js"><\/script>')</script>
    -->

   <script src="/js/lib/angular-1.3.16/angular.min.js"></script>
   <script src="/js/lib/angular-1.3.16/angular-route.min.js"></script>
   <script src="/js/lib/angular-1.3.16/angular-resource.min.js"></script>

    <script src="/js/lib/angular-ui/ui-utils.min.js"></script>
    <script src="/js/lib/angular-ui/ui-date.min.js"></script>
    <script src="/js/lib/angular-ui/ui-event.min.js"></script>
    <script src="/js/lib/angular-ui/ui-bootstrap-tpls-0.12.1.min.js"></script>
    <script src="/js/lib/angular-ui/ng-grid-2.0.14.min.js"></script>
    <script src="/js/lib/angular-ui/plugins/ng-grid-layout.min.js"></script>
    <script src="/js/lib/angular-ui/ui-codemirror.min.js"></script>
    <script src="/js/lib/dangle.module.min.js"></script>
    <script src="/js/lib/moment.min.js"></script>

    <!--<script src="/js/lib/bootstrap.min.js"></script>-->

    <script src="/js/controllers.js"></script>
    <script src="/js/services.js"></script>
    <script src="/js/filters.js"></script>
    <script src="/js/directives.js"></script>

    @yield('scripts')

</body>
</html>
