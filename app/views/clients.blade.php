@extends('template')

@section('page-app')ng-app="ncpApp"@stop

@section('title')
    NCP | Clients
@stop

@section('sub_title')
    Clients
@stop

@section('content')


    <div class="container ng-cloak" ng-controller="ClientsCtrl">
        <div id="client-list" ng-hide="showClient()" class="list ng-cloak">
            <div class="filter-buttons">
                <div class="col-sm-6">
                    <select class="form-control" ng-model="clientFilter" ng-change="filterClients()">
                        <option value="recent_clients">Recent Clients</option>
                        <option value="all_clients">All Clients</option>
                        <option value="all_clients_alt">All Clients (Alternate)</option>
                        <option value="intake_list">Clients Pending Intake</option>
                        <option value="wait_list">Clients Awaiting Assignment</option>
                        <option value="on_hold">Clients On Hold</option>
                        <option value="in_progress">Name Changes In Progress</option>
                        <option value="for_firm">Clients By Firm &amp; Attorney</option>
                        <option value="case_list">All Clients With Filed Cases</option>
                        <option value="closed_list">Closed Clients</option>
                        <option value="search">Search</option>
                    </select>
                </div>
                <div class="col-sm-12 ng-cloak">
                    <button class="btn btn-default pull-right ng-cloak" ng-show="clientFilter==='search'"
                            ng-click="toggleFilters()"><span ng-class="filterIcon"></span> {{filterCmd}} Query Editor
                    </button>
                </div>
            </div>
            <div class="col-sm-12">
                <div ng-form name="intakelist_filters" class="ng-cloak col-sm-12 form-inline well"
                     ng-show="clientFilter==='intake_list'">
                    <span class="filter-label"> Region </span>
                    <select class="form-control ng-cloak" ng-show="clientFilter==='intake_list'"
                            ng-model="zoneFilter" ng-change="filterIntakeList()">
                        <option ng-repeat="zone in zones">{{zone.name}}</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div ng-form name="waitlist_filters" class="ng-cloak form-inline well"
                     ng-show="clientFilter==='wait_list'|| clientFilter==='on_hold'">
                    <span class="filter-label"> Region </span>
                    <select class="form-control ng-cloak"
                            ng-show="clientFilter==='wait_list' || clientFilter==='on_hold'"
                            ng-model="zoneFilter" ng-change="setCountyList()">
                        <option ng-repeat="zone in zones">{{zone.name}}</option>
                    </select>
                    <span class="filter-label"> County </span>
                    <select class="form-control ng-cloak"
                            ng-show="clientFilter==='wait_list' || clientFilter==='on_hold'"
                            ng-model="countyFilter" ng-change="filterWaitList()">
                        <option ng-repeat="item in counties_for_zone">{{item}}</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div ng-form name="inprogress_filters" class="ng-cloak form-inline well"
                     ng-show="clientFilter==='in_progress'">
                    <span class="filter-label"> Region </span>
                    <select class="form-control ng-cloak"
                            ng-model="zoneFilter" ng-change="filterInProgress()">
                        <option ng-repeat="zone in zones">{{zone.name}}</option>
                    </select>
                    <span class="filter-label"> Firm </span>
                    <select class="form-control ng-cloak"
                            ng-model="firmFilter" ng-change="filterInProgress()">
                        <option ng-repeat="name in firms.names">{{name}}</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div ng-form name="firm_filters" class="ng-cloak form-horizontal well" ng-show="clientFilter==='for_firm'">
                    <label class="control-label"> Firm </label>
                    <select class="form-control ng-cloak" ng-show="clientFilter==='for_firm'"
                            ng-model="firmFilter" ng-change="filterFirm()">
                        <option ng-repeat="name in firms.names">{{name}}</option>
                    </select>
                    <label class="control-label" ng-show="firmFilter"> Region </label>
                    <select class="form-control ng-cloak" ng-show="firmFilter"
                            ng-model="zoneFilter" ng-change="filterFirm()">
                        <option ng-repeat="zone in zones">{{zone.name}}</option>
                    </select>
                    <label class="control-label" ng-show="firmFilter"> Attorney </label>
                    <select class="form-control ng-cloak" ng-show="firmFilter"
                            ng-model="attorneyFilter" ng-change="filterAttorney()">
                        <option ng-repeat="name in attorneys.names">{{name}}</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div ng-form name="close_filters" class="ng-cloak form-inline well"
                     ng-show="clientFilter==='closed_list'">
                    <span>Show </span>
                    <select class="form-control" ng-model="closedClientFilter.intake" ng-change="filterClosedClients()">
                        <option value="Yes">Had Intake</option>
                        <option value="No">No Intake</option>
                    </select>
                    <span>&amp; </span>
                    <select class="form-control" ng-model="closedClientFilter.assigned"
                            ng-change="filterClosedClients()">
                        <option value="Yes">Assigned</option>
                        <option value="No">Not Assigned</option>
                    </select>
                    <span>&amp; </span>
                    <select class="form-control" ng-model="closedClientFilter.accepted"
                            ng-change="filterClosedClients()">
                        <option value="Yes">Accepted</option>
                        <option value="No">Rejected</option>
                    </select>
                    <span>&amp; </span>
                    <select class="form-control" ng-model="closedClientFilter.complete"
                            ng-change="filterClosedClients()">
                        <option value="Yes">Completed</option>
                        <option value="No">Incomplete</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="search-control ng-cloak" ng-show="clientFilter==='search'">
                    <div ng-form name="search_form" class="ng-cloak form-inline form-search well" ng-show="showFilters">
                        <a ng-href="/clients/emails/{{filterText}}" class="btn btn-default pull-right"
                           ng-disabled="!enableDownload()"><span class="glyphicon glyphicon-circle-arrow-down"></span>
                            Download Emails</a>
                        <button class="btn btn-default pull-right" ng-disabled="!enableSearch()"
                                ng-click="applyFilters()"><span class="glyphicon glyphicon-search"></span> Search
                        </button>
                        <div ng-repeat="filterGroup in filterGroups" ng-include="'filter_group_renderer.html'"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="progress progress-striped active ng-cloak" ng-show="loading">
                    <div class="progress-bar" style="width: 100%;"></div>
                </div>
                <button class="btn btn-default btn-block ng-cloak" ng-hide="loading" ng-click="newClient()">
                    <span class="glyphicon glyphicon-plus-sign"></span> <b>Add Client</b>
                </button>
            </div>
            <div class="col-sm-12">
                <div class="gridStyle" ng-grid="gridOptions"></div>
            </div>
        </div>
        <div ng-show="showClient()" class="ng-cloak detail form">
            <div class="ng-cloak" ng-class="banner">
                <div class="fluid-container">
                    <div class="row">
                        <div class="col-sm-12 banner-id">
                            <p>Client ID: <b>{{client.current.id}}</b> Name:
                                <b>{{client.current.preferred_first_name}} {{client.current.preferred_middle_name}} {{client.current.preferred_last_name}}
                                    <span class="legal_name">(legal name: {{client.current.legal_first_name}} {{client.current.legal_middle_name}} {{client.current.legal_last_name}})</span></b></p>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-sm-12 form-inline well">
                            <div style="padding:5px;">
                            <select class="form-control" ng-model="clientForm"
                                    ng-change="showForm()">
                                <option ng-repeat="item in clientForms">{{item}}</option>
                            </select>
                            <button class="btn btn-default ng-cloak" style="margin:5px;" ng-click="toggleNotes()"><span
                                        ng-class="notesIcon"></span> Notes
                            </button>
                            <button class="btn btn-default ng-cloak" style="margin:5px;" ng-disabled="noValidChanges()"
                                    ng-click="saveClient(false)"><span class="glyphicon glyphicon-file"></span>
                                Save
                            </button>
                            <button class="btn btn-default ng-cloak" style="margin:5px;" ng-click="closeClient()"><span
                                        class="glyphicon gylphicon-remove-circle"></span> Done
                            </button>
                            </div>
                        </div>
                    </div>
                    <div class="row" ng-form name="notes_form">
                        <div class="col-sm-12" ng-show="showNotes">
                            <!-- notes_general -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                <textarea class="form-control" name="notes_general" rows="8"
                                          ng-model="client.current.notes_general"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="client-form" ng-class="formContainer">
                <div class="faces" ng-class="flipClass">
                    <div class="front">
                        <div ng-form name="training_form" class="form-inline well"
                             ng-show="!client.current.date_intake_completed && !(role == 'Administrator')">
                            <label class="checkbox">
                                <input type="checkbox" name="show_all_prompts"
                                       ng-model="showPrompts" ng-disabled="isFlipped"
                                       ng-change="client.setPrompts(showPrompts)"/>
                                Show Prompts
                            </label>
                        </div>
                        <div ng-show="client.current.is_closed === 'Yes'" class="closed">
                            <span class="closed-text">This client's case has been closed</span>
                            <button class="btn btn-default pull-right" ng-click="openClient()">Open Case</button>
                        </div>

                        <div ng-form name="intake_form" class="form-horizontal col-sm-12">
                            <!-- date_intake_scheduled -->
                            <div class="form-group" ng-show="! client.current.date_intake_completed">
                                <label class="col-sm-3 control-label" for="intake_scheduled">Date Intake Scheduled For</label>

                                <div class="col-sm-3">
                                    <input type="text" name="intake_scheduled" class="form-control"
                                           placeholder="mm/dd/yyyy"
                                           ng-pattern="/^(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/(199\d|[2-9]\d{3})$/"
                                           ng-model="client.current.date_intake_scheduled"
                                           ng-disabled="isFlipped"/>

                                    <div class="error" ng-show="intake_form.intake_scheduled.$error.pattern">
                                        {{client.errors.date}}</div>
                                </div>
                            </div>
                            <!-- preferred_first_name -->
                            <p ng-show="client.prompts.preferred_first_name.show"
                               class="prompt ng-cloak">{{client.prompts.preferred_first_name.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="preferred_first_name">Preferred First
                                    Name</label>

                                <div class="col-sm-3">
                                    <input type="text" name="preferred_first_name" class="form-control" required
                                           ng-model="client.current.preferred_first_name" ng-disabled="isFlipped"
                                           ui-event="{ focusin : 'client.prompts.preferred_first_name.show=true',
                                   focusout: 'client.prompts.preferred_first_name.show=showPrompts'}"/>
                                    <div class="error" ng-show="intake_form.preferred_first_name.$error.required">This
                                        is a required entry.
                                    </div>
                                </div>
                            </div>
                            <!-- preferred_middle_name -->
                            <p ng-show="client.prompts.preferred_middle_name.show"
                               class="prompt ng-cloak">{{client.prompts.preferred_middle_name.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="preferred_middle_name">Preferred Middle
                                    Name</label>

                                <div class="col-sm-3">
                                    <input type="text" name="preferred_middle_name" class="form-control"
                                           ng-model="client.current.preferred_middle_name" ng-disabled="isFlipped"
                                           ui-event="{ focusin : 'client.prompts.preferred_middle_name.show=true',
                                   focusout: 'client.prompts.preferred_middle_name.show=showPrompts'}"/>
                                </div>
                            </div>
                            <!-- preferred_last_name -->
                            <p ng-show="client.prompts.preferred_last_name.show"
                               class="prompt ng-cloak">{{client.prompts.preferred_last_name.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="preferred_last_name">Preferred Last
                                    Name</label>

                                <div class="col-sm-3">
                                    <input type="text" name="preferred_last_name" class="form-control" required
                                           ng-model="client.current.preferred_last_name" ng-disabled="isFlipped"
                                           ui-event="{ focusin : 'client.prompts.preferred_last_name.show=true',
                                   focusout: 'client.prompts.preferred_last_name.show=showPrompts'}"/>
                                    <div class="error" ng-show="intake_form.preferred_last_name.$error.required">This is
                                        a required entry.
                                    </div>
                                </div>
                            </div>
                            <!-- legal_first_name -->
                            <p ng-show="client.prompts.legal_first_name.show"
                               class="prompt ng-cloak">{{client.prompts.legal_first_name.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="legal_first_name">Legal First Name</label>

                                <div class="col-sm-3">
                                    <input type="text" name="legal_first_name" class="form-control"
                                           ng-model="client.current.legal_first_name" ng-disabled="isFlipped"
                                           ui-event="{ focusin : 'client.prompts.legal_first_name.show=true',
                                                focusout: 'client.prompts.legal_first_name.show=showPrompts'}"/>
                                </div>
                            </div>
                            <!-- legal_middle_name -->
                            <p ng-show="client.prompts.legal_middle_name.show"
                               class="prompt ng-cloak">{{client.prompts.legal_middle_name.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="legal_middle_name">Legal Middle Name</label>

                                <div class="col-sm-3">
                                    <input type="text" name="legal_middle_name" class="form-control"
                                           ng-model="client.current.legal_middle_name" ng-disabled="isFlipped"
                                           ui-event="{ focusin : 'client.prompts.legal_middle_name.show=true',
                                   focusout: 'client.prompts.legal_middle_name.show=showPrompts'}"/>
                                </div>
                            </div>
                            <!-- legal_last_name -->
                            <p ng-show="client.prompts.legal_last_name.show"
                               class="prompt ng-cloak">{{client.prompts.legal_last_name.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="legal_last_name">Legal Last Name</label>

                                <div class="col-sm-3 controls">
                                    <input type="text" name="legal_last_name" class="form-control"
                                           ng-model="client.current.legal_last_name" ng-disabled="isFlipped"
                                           ui-event="{ focusin : 'client.prompts.legal_last_name.show=true',
                                   focusout: 'client.prompts.legal_last_name.show=showPrompts'}"/>
                                </div>
                            </div>
                            <!-- aliases -->
                            <p ng-show="client.prompts.aliases.show"
                               class="prompt ng-cloak">{{client.prompts.aliases.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="aliases">Aliases</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="aliases" rows="3"
                                      ui-event="{ focusin : 'client.prompts.aliases.show=true', focusout: 'client.prompts.aliases.show=false'}"
                                      ng-model="client.current.aliases" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- has_previous_request -->
                            <p ng-show="client.prompts.has_previous_request.show"
                               class="prompt ng-cloak">{{client.prompts.has_previous_request.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_previous_request">Previous Name
                                    Change</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin : 'client.prompts.has_previous_request.show=true',
                                        focusout: 'client.prompts.has_previous_request.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_previous_request"
                                               ng-model="client.current.has_previous_request" ng-disabled="isFlipped"
                                               id="has_previous_request_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_previous_request"
                                               ng-model="client.current.has_previous_request" ng-disabled="isFlipped"
                                               id="has_previous_request_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_previous_request -->
                            <p ng-show="client.prompts.details_previous_request.show && client.current.has_previous_request"
                               class="prompt ng-cloak">{{client.prompts.details_previous_request.text}}</p>

                            <div class="form-group" ng-show="client.current.has_previous_request">
                                <label class="col-sm-3 control-label" for="details_previous_request">Previous Change
                                    Details</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_previous_request" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_previous_request.show=true',
                                                focusout: 'client.prompts.details_previous_request.show=false'}"
                                      ng-model="client.current.details_previous_request"
                                      ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- sex_assigned_at_birth -->
                            <p ng-show="client.prompts.sex_assigned_at_birth.show"
                               class="prompt ng-cloak">{{client.prompts.sex_assigned_at_birth.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="sex_assigned_at_birth">Sex Assigned At
                                    Birth</label>

                                <div class="col-sm-2">
                                    <select class="form-control" ng-model="client.current.sex_assigned_at_birth"
                                            ng-disabled="isFlipped"
                                            ui-event="{ focusin : 'client.prompts.sex_assigned_at_birth.show=true',
                                                focusout: 'client.prompts.sex_assigned_at_birth.show=showPrompts'}">
                                        <option ng-repeat="item in client.options.sex_assigned_at_birth">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.sex_assigned_at_birth">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- has_birth_certificate -->
                            <p ng-show="client.prompts.has_birth_certificate.show"
                               class="prompt ng-cloak">{{client.prompts.has_birth_certificate.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_birth_certificate">Has Birth
                                    Certificate</label>

                                <div class="col-sm-2" ui-event="{ focusin : 'client.prompts.has_birth_certificate.show=true',
                                        focusout: 'client.prompts.has_birth_certificate.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_birth_certificate"
                                               ng-model="client.current.has_birth_certificate" ng-disabled="isFlipped"
                                               id="has_birth_certificate_no" value="No"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_birth_certificate"
                                               ng-model="client.current.has_birth_certificate" ng-disabled="isFlipped"
                                               id="has_birth_certificate_yes" value="Yes"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- birth_certificate_number -->
                            <p ng-show="client.prompts.birth_certificate_number.show"
                               class="prompt ng-cloak">{{client.prompts.birth_certificate_number.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="birth_certificate_number">Birth Certificate
                                    Number</label>

                                <div class="col-sm-9">
                                    <input type="text" name="birth_certificate_number" class="form-control"
                                           ui-event="{ focusin : 'client.prompts.birth_certificate_number.show=true',
                                   focusout: 'client.prompts.birth_certificate_number.show=showPrompts'}"
                                           ng-model="client.current.birth_certificate_number" ng-disabled="isFlipped"/>
                                </div>
                            </div>
                            <!-- date_of_birth -->
                            <!-- <p ng-show="client.prompts.date_of_birth.show" class="prompt ng-cloak">{{client.prompts.date_of_birth.text}}</p>-->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="date_of_birth">Date of Birth</label>

                                <div class="col-sm-3">
                                    <input type="text" name="date_of_birth" placeholder="mm/dd/yyyy"
                                           class="form-control"
                                           ng-model="client.current.date_of_birth" ng-change="client.computeAge()"
                                           ng-disabled="isFlipped"
                                           ng-pattern="/^(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/(19\d{2}|[2-9]\d{3})$/"
                                           ui-event="{ focusin : 'client.prompts.date_of_birth.show=true',
                                   focusout: 'client.prompts.date_of_birth.show=showPrompts'}"/>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                    <div class="error" ng-show="intake_form.date_of_birth.$error.pattern">
                                        {{client.errors.date}}</div>
                                </div>
                            </div>
                            <!-- age -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="age">Age</label>

                                <div class="col-sm-1">
                                    <input type="text" name="age" class="form-control" ng-model="client.current.age"
                                           ng-disabled="true"/>
                                </div>
                            </div>
                            <!-- place_of_birth -->
                            <p ng-show="client.prompts.place_of_birth.show"
                               class="prompt ng-cloak">{{client.prompts.place_of_birth.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="place_of_birth">Place of Birth</label>

                                <div class="col-sm-9">
                                    <input type="text" name="place_of_birth" class="form-control"
                                           ui-event="{ focusin : 'client.prompts.place_of_birth.show=true',
                                   focusout: 'client.prompts.place_of_birth.show=false'}"
                                           ng-model="client.current.place_of_birth" ng-disabled="isFlipped"/>
                                </div>
                            </div>
                            <!-- state_of_birth -->
                            <p ng-show="client.prompts.state_of_birth.show"
                               class="prompt ng-cloak">{{client.prompts.state_of_birth.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="state_of_birth">State Of Birth</label>

                                <div class="col-sm-2">
                                    <select class="form-control"
                                            ng-model="client.current.state_of_birth" ng-disabled="isFlipped"
                                            ui-event="{ focusin : 'client.prompts.state_of_birth.show=true',
                                    focusout: 'client.prompts.state_of_birth.show=showPrompts'}">
                                        <option ng-repeat="item in state_codes">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in state_codes">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- citizenship_status -->
                            <p ng-show="client.prompts.citizenship_status.show"
                               class="prompt ng-cloak">{{client.prompts.citizenship_status.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="citizenship_status">Citizenship
                                    Status</label>

                                <div class="col-sm-9">
                                    <select class="form-control" ng-model="client.current.citizenship_status"
                                            ng-change="client.setCitizenship()" ng-disabled="isFlipped"
                                            ui-event="{ focusin : 'client.prompts.citizenship_status.show=true',
                                    focusout: 'client.prompts.citizenship_status.show=showPrompts'}">
                                        <option ng-repeat="item in client.options.citizenship_status">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.citizenship_status">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- details_immigration -->
                            <p ng-show="client.prompts.details_immigration.show && !client.current.is_citizen"
                               class="prompt ng-cloak">{{client.prompts.details_immigration.text}}</p>

                            <div class="form-group" ng-hide="client.current.is_citizen">
                                <label class="col-sm-3 control-label" for="details_immigration">Immigration
                                    Details</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_immigration" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_immigration.show=true',
                                                    focusout: 'client.prompts.details_immigration.show=false'}"
                                      ng-model="client.current.details_immigration" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>

                            <!-- phone1 -->
                            <p ng-show="client.prompts.phone1.show"
                               class="prompt ng-cloak">{{client.prompts.phone1.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="phone1">Phone 1</label>

                                <div class="col-sm-3">
                                    <input type="text" name="phone1" placeholder="999-999-9999x9999"
                                           class="form-control"
                                           ng-model="client.current.phone1" ng-disabled="isFlipped"
                                           ng-pattern="/[0-9]{3}-[0-9]{3}-[0-9]{4}(x[0-9]{1,4})?$/"
                                           ui-event="{ focusin : 'client.prompts.phone1.show=true',
                                   focusout: 'client.prompts.phone1.show=showPrompts'}"/>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                    <div class="error"
                                         ng-show="intake_form.phone1.$error.pattern">{{client.errors.phone}}</div>
                                </div>
                            </div>
                            <!-- phone1_type -->
                            <p ng-show="client.prompts.phone1_type.show"
                               class="prompt ng-cloak">{{client.prompts.phone1_type.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="phone1_type">Phone 1 Type</label>

                                <div class="col-sm-2">
                                    <select class="form-control" name="phone1_type"
                                            ng-model="client.current.phone1_type" ng-disabled="isFlipped"
                                            ui-event="{ focusin : 'client.prompts.phone1_type.show=true',
                                    focusout: 'client.prompts.phone1_type.show=showPrompts'}">
                                        <option ng-repeat="item in client.options.phone1_type">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.phone1_type">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- phone1_notes -->
                            <p ng-show="client.prompts.phone1_notes.show"
                               class="prompt ng-cloak">{{client.prompts.phone1_notes.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="phone1_notes">Phone 1 Notes</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="phone1_notes" rows="2"
                                      ui-event="{ focusin : 'client.prompts.phone1_notes.show=true',
                                      focusout: 'client.prompts.phone1_notes.show=showPrompts'}"
                                      ng-model="client.current.phone1_notes" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- phone2 -->
                            <p ng-show="client.prompts.phone2.show"
                               class="prompt ng-cloak">{{client.prompts.phone2.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="phone2">Phone 2</label>

                                <div class="col-sm-3">
                                    <input type="text" name="phone2" placeholder="999-999-999x9999" class="form-control"
                                           ng-model="client.current.phone2" ng-disabled="isFlipped"
                                           ng-pattern="/[0-9]{3}-[0-9]{3}-[0-9]{4}(x[0-9]{1,4})?$/"
                                           ui-event="{ focusin : 'client.prompts.phone2.show=true',
                                   focusout: 'client.prompts.phone2.show=showPrompts'}"/>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                    <div class="error"
                                         ng-show="intake_form.phone2.$error.pattern">{{client.errors.phone}}</div>
                                </div>
                            </div>
                            <!-- phone2_type -->
                            <p ng-show="client.prompts.phone2_type.show"
                               class="prompt ng-cloak">{{client.prompts.phone2_type.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="phone2_type">Phone 2 Type</label>

                                <div class="col-sm-2">
                                    <select class="form-control" name="phone2_type"
                                            ng-model="client.current.phone2_type" ng-disabled="isFlipped"
                                            ui-event="{ focusin : 'client.prompts.phone2_type.show=true',
                                    focusout: 'client.prompts.phone2_type.show=showPrompts'}">
                                        <option ng-repeat="item in client.options.phone2_type">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.phone2_type">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- phone2_notes -->
                            <p ng-show="client.prompts.phone2_notes.show"
                               class="prompt ng-cloak">{{client.prompts.phone2_notes.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="phone2_notes">Phone 2 Notes</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="phone2_notes" rows="2"
                                      ui-event="{ focusin : 'client.prompts.phone2_notes.show=true',
                                      focusout: 'client.prompts.phone2_notes.show=showPrompts'}"
                                      ng-model="client.current.phone2_notes" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- email -->
                            <p ng-show="client.prompts.email.show"
                               class="prompt ng-cloak">{{client.prompts.email.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="email">Email</label>

                                <div class="col-sm-9">
                                    <input type="email" name="email" class="form-control"
                                           ng-model="client.current.email" ng-disabled="isFlipped"
                                           ui-event="{ focusin : 'client.prompts.email.show=true',
                                   focusout: 'client.prompts.email.show=showPrompts'}"/>

                                    <div class="error"
                                         ng-show="intake_form.email.$error.email">{{client.errors.email}}</div>
                                </div>
                            </div>
                            <!-- alternate_contact -->
                            <p ng-show="client.prompts.alternate_contact.show"
                               class="prompt ng-cloak">{{client.prompts.alternate_contact.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="alternate_contact">Alternate Contact</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="alternate_contact" rows="5"
                                      ui-event="{ focusin : 'client.prompts.alternate_contact.show=true',
                                      focusout: 'client.prompts.alternate_contact.show=showPrompts'}"
                                      ng-model="client.current.alternate_contact" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- address_attention -->
                            <p ng-show="client.prompts.address_attention.show"
                               class="prompt ng-cloak">{{client.prompts.address_attention.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="address_attention">Address Mail To</label>

                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="address_attention"
                                           ui-event="{ focusin : 'client.prompts.address_attention.show=true',
                                      focusout: 'client.prompts.address_attention.show=showPrompts'}"
                                           ng-model="client.current.address_attention" ng-disabled="isFlipped"/>
                                </div>
                            </div>
                            <!-- address_street -->
                            <p ng-show="client.prompts.address_street.show"
                               class="prompt ng-cloak">{{client.prompts.address_street.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="address_street">Street</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="address_street" rows="2"
                                      ui-event="{ focusin : 'client.prompts.address_street.show=true',
                                      focusout: 'client.prompts.address_street.show=showPrompts'}"
                                      ng-model="client.current.address_street" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- address_city -->
                            <p ng-show="client.prompts.address_city.show"
                               class="prompt ng-cloak">{{client.prompts.address_city.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="address_city">City/Town</label>

                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="address_city"
                                           ui-event="{ focusin : 'client.prompts.address_city.show=true',
                                      focusout: 'client.prompts.address_city.show=showPrompts'}"
                                           ng-model="client.current.address_city" ng-disabled="isFlipped"/>
                                </div>
                            </div>
                            <!-- state -->
                            <p ng-show="client.prompts.state.show"
                               class="prompt ng-cloak">{{client.prompts.state.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="state">State</label>

                                <div class="col-sm-2">
                                    <select class="form-control"
                                            ng-model="client.current.state" ng-disabled="isFlipped"
                                            ng-change="getCounties(client.current.state)"
                                            ui-event="{ focusin : 'client.prompts.state.show=true',
                                    focusout: 'client.prompts.state.show=showPrompts'}">
                                        <option ng-repeat="item in state_codes">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in state_codes">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- address_zip_code -->
                            <p ng-show="client.prompts.address_zip_code.show"
                               class="prompt ng-cloak">{{client.prompts.address_zip_code.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="address_zip_code">Zip Code</label>

                                <div class="col-sm-3">
                                    <input type="text" class="form-control" name="address_zip_code"
                                           ui-event="{ focusin : 'client.prompts.address_zip_code.show=true',
                                      focusout: 'client.prompts.address_zip_code.show=showPrompts'}"
                                           ng-model="client.current.address_zip_code" ng-disabled="isFlipped"/>
                                </div>
                            </div>
                            <!-- county -->
                            <p ng-show="client.prompts.county.show"
                               class="prompt ng-cloak">{{client.prompts.county.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="county">County</label>

                                <div class="col-sm-3">
                                    <select class="form-control"
                                            ng-model="client.current.county" ng-disabled="isFlipped"
                                            ui-event="{ focusin : 'client.prompts.county.show=true',
                                    focusout: 'client.prompts.county.show=showPrompts'}">
                                        <option ng-repeat="item in counties">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.county">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- address_notes -->
                            <p ng-show="client.prompts.address_notes.show"
                               class="prompt ng-cloak">{{client.prompts.address_notes.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="address_notes">Address Notes</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="address_notes" rows="3"
                                      ui-event="{ focusin : 'client.prompts.address_notes.show=true',
                                      focusout: 'client.prompts.address_notes.show=showPrompts'}"
                                      ng-model="client.current.address_notes" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- previous_addresses -->
                            <p ng-show="client.prompts.previous_addresses.show && client.current.state === 'PA'"
                               class="prompt ng-cloak">{{client.prompts.previous_addresses.text}}</p>

                            <div class="form-group" ng-show="client.current.state === 'PA'">
                                <label class="col-sm-3 control-label" for="previous_addresses">Previous
                                    Addresses</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="previous_addresses" rows="15"
                                      ui-event="{ focusin : 'client.prompts.previous_addresses.show=true',
                                      focusout: 'client.prompts.previous_addresses.show=showPrompts'}"
                                      ng-model="client.current.previous_addresses" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- type_of_housing -->
                            <p ng-show="client.prompts.type_of_housing.show"
                               class="prompt ng-cloak">{{client.prompts.type_of_housing.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="type_of_housing">Resides In</label>

                                <div class="col-sm-3">
                                    <select class="form-control"
                                            ng-model="client.current.type_of_housing" ng-disabled="isFlipped"
                                            ui-event="{ focusin : 'client.prompts.type_of_housing.show=true',
                                    focusout: 'client.prompts.type_of_housing.show=showPrompts'}">
                                        <option ng-repeat="item in client.options.type_of_housing">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.type_of_housing">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- resides_with -->
                            <p ng-show="client.prompts.resides_with.show"
                               class="prompt ng-cloak">{{client.prompts.resides_with.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="resides_with">Resides With</label>

                                <div class="col-sm-3">
                                    <select class="form-control"
                                            ng-model="client.current.resides_with" ng-disabled="isFlipped"
                                            ui-event="{ focusin : 'client.prompts.resides_with.show=true',
                                    focusout: 'client.prompts.resides_with.show=showPrompts'}">
                                        <option ng-repeat="item in client.options.resides_with">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.resides_with">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- has_marriage -->
                            <p ng-show="client.prompts.has_marriage.show"
                               class="prompt ng-cloak">{{client.prompts.has_marriage.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_marriage">Marriage/Civil Union</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin: 'client.prompts.has_marriage.show=true',
                                         focusout: 'client.prompts.has_marriage.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_marriage"
                                               ng-model="client.current.has_marriage" ng-disabled="isFlipped"
                                               id="has_marriage_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_marriage"
                                               ng-model="client.current.has_marriage" ng-disabled="isFlipped"
                                               id="has_marriage_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_marriage -->
                            <p ng-show="client.prompts.details_marriage.show && client.current.has_marriage"
                               class="prompt ng-cloak">{{client.prompts.details_marriage.text}}</p>

                            <div class="form-group" ng-show="client.current.has_marriage">
                                <label class="col-sm-3 control-label" for="details_marriage">Marriage Details</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_marriage" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_marriage.show=true',
                                      focusout: 'client.prompts.details_marriage.show=false'}"
                                      ng-model="client.current.details_marriage" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- has_children -->
                            <p ng-show="client.prompts.has_children.show"
                               class="prompt ng-cloak">{{client.prompts.has_children.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_children">Has Children</label>

                                <div class="col-sm-3" ui-event="{ focusin : 'client.prompts.has_children.show=true',
                        focusout: 'client.prompts.has_children.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_children"
                                               ng-model="client.current.has_children" ng-disabled="isFlipped"
                                               id="has_children_no" value="No"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_children"
                                               ng-model="client.current.has_children" ng-disabled="isFlipped"
                                               id="has_children_yes" value="Yes"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_children -->
                            <p ng-show="client.prompts.details_children.show && client.current.has_children == 'Yes'"
                               class="prompt ng-cloak">{{client.prompts.details_children.text}}</p>

                            <div class="form-group" ng-hide="client.current.has_children != 'Yes'">
                                <label class="col-sm-3 control-label" for="details_children">Children Details</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_children" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_children.show=true',
                                      focusout: 'client.prompts.details_children.show=false'}"
                                      ng-model="client.current.details_children" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>

                            <!-- has_convictions -->
                            <p ng-show="client.prompts.has_convictions.show"
                               class="prompt ng-cloak">{{client.prompts.has_convictions.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_convictions">Arrests/Convictions</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin : 'client.prompts.has_convictions.show=true',
                                        focusout: 'client.prompts.has_convictions.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_convictions"
                                               ng-model="client.current.has_convictions" ng-disabled="isFlipped"
                                               id="has_convictions_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_convictions"
                                               ng-model="client.current.has_convictions" ng-disabled="isFlipped"
                                               id="has_convictions_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_convictions -->
                            <p ng-show="client.prompts.details_convictions.show && client.current.has_convictions"
                               class="prompt ng-cloak">{{client.prompts.details_convictions.text}}</p>

                            <div class="form-group" ng-show="client.current.has_convictions">
                                <label class="col-sm-3 control-label" for="details_convictions">Conviction
                                    Details</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_convictions" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_convictions.show=true',
                                                focusout: 'client.prompts.details_convictions.show=false'}"
                                      ng-model="client.current.details_convictions" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- has_warrants -->
                            <p ng-show="client.prompts.has_warrants.show"
                               class="prompt ng-cloak">{{client.prompts.has_warrants.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_warrants">Warrants/Unpaid Tickets</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin : 'client.prompts.has_warrants.show=true',
                                        focusout: 'client.prompts.has_warrants.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_warrants"
                                               ng-model="client.current.has_warrants" ng-disabled="isFlipped"
                                               id="has_warrants_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_warrants"
                                               ng-model="client.current.has_warrants" ng-disabled="isFlipped"
                                               id="has_property_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_warrants -->
                            <p ng-show="client.prompts.details_warrants.show && client.current.has_warrants"
                               class="prompt ng-cloak">{{client.prompts.details_warrants.text}}</p>

                            <div class="form-group" ng-show="client.current.has_warrants">
                                <label class="col-sm-3 control-label" for="details_warrants">Legal Liability
                                    Details</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_warrants" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_warrants.show=true',
                                                focusout: 'client.prompts.details_warrants.show=showPrompts'}"
                                      ng-model="client.current.details_warrants" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- is_registered_sex_offender -->
                            <p ng-show="client.prompts.is_registered_sex_offender.show && client.current.state === 'WI'"
                               class="prompt ng-cloak">{{client.prompts.is_registered_sex_offender.text}}</p>

                            <div class="form-group" ng-show="client.current.state === 'WI'">
                                <label class="col-sm-3 control-label" for="is_registered_sex_offender">Sex
                                    Offender</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin : 'client.prompts.is_registered_sex_offender.show=true',
                                        focusout: 'client.prompts.is_registered_sex_offender.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="is_registered_sex_offender"
                                               ng-model="client.current.is_registered_sex_offender"
                                               ng-disabled="isFlipped"
                                               id="is_registered_sex_offender_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="is_registered_sex_offender"
                                               ng-model="client.current.is_registered_sex_offender"
                                               ng-disabled="isFlipped"
                                               id="is_registered_sex_offender_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_sex_offender -->
                            <p ng-show="client.prompts.details_sex_offender.show && client.current.is_registered_sex_offender && client.current.state === 'WI'"
                               class="prompt ng-cloak">{{client.prompts.details_sex_offender.text}}</p>

                            <div class="form-group"
                                 ng-show="(client.current.state === 'WI') && client.current.is_registered_sex_offender">
                                <label class="col-sm-3 control-label" for="details_sex_offender">Sex Offender
                                    Details</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_sex_offender" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_sex_offender.show=true',
                                                focusout: 'client.prompts.details_sex_offender.show=false'}"
                                      ng-model="client.current.details_sex_offender" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- is_on_parole -->
                            <p ng-show="client.prompts.is_on_parole.show && client.current.state === 'WI'"
                               class="prompt ng-cloak">{{client.prompts.is_on_parole.text}}</p>

                            <div class="form-group" ng-show="client.current.state === 'WI'">
                                <label class="col-sm-3 control-label" for="is_on_parole">On Parole</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin : 'client.prompts.is_on_parole.show=true',
                                        focusout: 'client.prompts.is_on_parole.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="is_on_parole"
                                               ng-model="client.current.is_on_parole" ng-disabled="isFlipped"
                                               id="is_on_parole_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="is_on_parole"
                                               ng-model="client.current.is_on_parole" ng-disabled="isFlipped"
                                               id="is_on_parole_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_parole -->
                            <p ng-show="client.prompts.details_parole.show && client.current.is_on_parole && client.current.state === 'WI'"
                               class="prompt ng-cloak">{{client.prompts.details_parole.text}}</p>

                            <div class="form-group"
                                 ng-show="(client.current.state === 'WI') && client.current.is_on_parole">
                                <label class="col-sm-3 control-label" for="details_parole">Parole Details</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_parole" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_parole.show=true',
                                                focusout: 'client.prompts.details_parole.show=false'}"
                                      ng-model="client.current.details_parole" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- has_judgments -->
                            <p ng-show="client.prompts.has_judgments.show"
                               class="prompt ng-cloak">{{client.prompts.has_judgments.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_judgments">Has Judgments</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin : 'client.prompts.has_judgments.show=true',
                                        focusout: 'client.prompts.has_judgments.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_judgments"
                                               ng-model="client.current.has_judgments" ng-disabled="isFlipped"
                                               id="has_judgments_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_judgments"
                                               ng-model="client.current.has_judgments" ng-disabled="isFlipped"
                                               id="has_judgments_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_judgments -->
                            <p ng-show="client.prompts.details_judgments.show && client.current.has_judgments"
                               class="prompt ng-cloak">{{client.prompts.details_judgments.text}}</p>

                            <div class="form-group" ng-show="client.current.has_judgments">
                                <label class="col-sm-3 control-label" for="details_judgments">Judgment Details</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_judgments" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_judgments.show=true',
                                                focusout: 'client.prompts.details_judgments.show=false'}"
                                      ng-model="client.current.details_judgments" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- has_bankruptcies -->
                            <p ng-show="client.prompts.has_bankruptcies.show"
                               class="prompt ng-cloak">{{client.prompts.has_bankruptcies.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_bankruptcies">Has Bankruptcies</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin : 'client.prompts.has_bankruptcies.show=true',
                                        focusout: 'client.prompts.has_bankruptcies.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_bankruptcies"
                                               ng-model="client.current.has_bankruptcies" ng-disabled="isFlipped"
                                               id="has_bankruptcies_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_bankruptcies"
                                               ng-model="client.current.has_bankruptcies" ng-disabled="isFlipped"
                                               id="has_bankruptcies_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_bankruptcies -->
                            <p ng-show="client.prompts.details_bankruptcies.show && client.current.has_bankruptcies"
                               class="prompt ng-cloak">{{client.prompts.details_bankruptcies.text}}</p>

                            <div class="form-group" ng-show="client.current.has_bankruptcies">
                                <label class="col-sm-3 control-label" for="details_bankruptcies">Bankruptcy
                                    Details</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_bankruptcies" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_bankruptcies.show=true',
                                                    focusout: 'client.prompts.details_bankruptcies.show=false'}"
                                      ng-model="client.current.details_bankruptcies" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- has_legal_proceedings -->
                            <p ng-show="client.prompts.has_legal_proceedings.show"
                               class="prompt ng-cloak">{{client.prompts.has_legal_proceedings.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_legal_proceedings">Has Legal
                                    Proceedings</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin : 'client.prompts.has_legal_proceedings.show=true',
                                        focusout: 'client.prompts.has_legal_proceedings.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_legal_proceedings"
                                               ng-model="client.current.has_legal_proceedings" ng-disabled="isFlipped"
                                               id="has_legal_proceedings_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_legal_proceedings"
                                               ng-model="client.current.has_legal_proceedings" ng-disabled="isFlipped"
                                               id="has_legal_proceedings_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_legal_proceedings -->
                            <p ng-show="client.prompts.details_legal_proceedings.show && client.current.has_legal_proceedings"
                               class="prompt ng-cloak">{{client.prompts.details_legal_proceedings.text}}</p>

                            <div class="form-group" ng-show="client.current.has_legal_proceedings">
                                <label class="col-sm-3 control-label" for="details_legal_proceedings">Legal Proceeding
                                    Details</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_legal_proceedings" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_legal_proceedings.show=true',
                                                focusout: 'client.prompts.details_legal_proceedings.show=false'}"
                                      ng-model="client.current.details_legal_proceedings"
                                      ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- reasons_for_name_change -->
                            <p ng-show="client.prompts.reasons_for_name_change.show"
                               class="prompt ng-cloak">{{client.prompts.reasons_for_name_change.text}}</p>

                            <div class="form-group"
                                 ui-event="{ focusin : 'client.prompts.reasons_for_name_change.show=true',
                                     focusout: 'client.prompts.reasons_for_name_change.show=showPrompts'}">
                                <label class="col-sm-3 control-label" for="default_reason_for_name_change">Name Change
                                    Reason</label>

                                <div class="col-sm-9">
                                    <select class="form-control"
                                            ng-model="client.current.reasons_for_name_change" ng-disabled="isFlipped">
                                        <option></option>
                                        <option>I am a man and I would like my name to be consistent with my identity and appearance.</option>
                                        <option>I am a woman and I would like my name to be consistent with my identity and appearance.</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.default_reason_for_name_change">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                            <textarea class="form-control" name="reasons_for_name_change" rows="5"
                                      ng-model="client.current.reasons_for_name_change"
                                      ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- is_urgent -->
                            <p ng-show="client.prompts.is_urgent.show"
                               class="prompt ng-cloak">{{client.prompts.is_urgent.text}}</p>

                            <div class="form-group"
                                 ui-event="{ focusin : 'client.prompts.is_urgent.show=true',
                                        focusout: 'client.prompts.is_urgent.show=showPrompts'}">
                                <label class="col-sm-3 control-label" for="is_urgent">Is Urgent</label>

                                <div class="col-sm-3">

                                    <label class="radio radio-inline">
                                        <input type="radio" name="is_urgent"
                                               ng-model="client.current.is_urgent" ng-disabled="isFlipped"
                                               id="is_urgent_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="is_urgent"
                                               ng-model="client.current.is_urgent" ng-disabled="isFlipped"
                                               id="is_urgent_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- reasons_for_urgency -->
                            <p ng-show="client.prompts.reasons_for_urgency.show && client.current.is_urgent"
                               class="prompt ng-cloak">{{client.prompts.reasons_for_urgency.text}}</p>

                            <div class="form-group" ng-show="client.current.is_urgent">
                                <label class="col-sm-3 control-label" for="reasons_for_urgency">Reason for
                                    Urgency</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="reasons_for_urgency" rows="5"
                                      ui-event="{ focusin : 'client.prompts.reasons_for_urgency.show=true',
                                                    focusout: 'client.prompts.reasons_for_urgency.show=false'}"
                                      ng-model="client.current.reasons_for_urgency" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- no_publication -->
                            <p ng-show="client.prompts.no_publication.show && client.current.state != 'TX'"
                               class="prompt ng-cloak">{{client.prompts.no_publication.text}}</p>

                            <div class="form-group" ng-show="client.current.state != 'TX'"
                                 ui-event="{ focusin : 'client.prompts.no_publication.show=true',
                                        focusout: 'client.prompts.no_publication.show=showPrompts'}">
                                <label class="col-sm-3 control-label" for="no_publication">Waive Publication</label>

                                <div class="col-sm-3">

                                    <label class="radio radio-inline">
                                        <input type="radio" name="no_publication"
                                               ng-model="client.current.no_publication" ng-disabled="isFlipped"
                                               id="no_publication_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="no_publication"
                                               ng-model="client.current.no_publication" ng-disabled="isFlipped"
                                               id="no_publication_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_publication_waiver -->
                            <p ng-show="client.prompts.details_publication_waiver.show && client.current.no_publication && client.current.state != 'TX'"
                               class="prompt ng-cloak">{{client.prompts.details_publication_waiver.text}}</p>

                            <div class="form-group" ng-show="client.current.no_publication">
                                <label class="col-sm-3 control-label" for="details_publication_waiver">Reasons for
                                    Waiver</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_publication_waiver" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_publication_waiver.show=true',
                                                focusout: 'client.prompts.details_publication_waiver.show=false'}"
                                      ng-model="client.current.details_publication_waiver"
                                      ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- gender_transition -->
                            <p ng-show="client.prompts.gender_transition.show"
                               class="prompt ng-cloak">{{client.prompts.gender_transition.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="gender_transition">Gender Transition</label>

                                <div class="col-sm-3">
                                    <select class="form-control"
                                            ui-event="{ focusin : 'client.prompts.gender_transition.show=true',
                                    focusout: 'client.prompts.gender_transition.show=showPrompts'}"
                                            ng-model="client.current.gender_transition" ng-disabled="isFlipped">
                                        <option ng-repeat="item in client.options.gender_transition">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.gender_transition">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- gender_identity -->
                            <p ng-show="client.prompts.gender_identity.show"
                               class="prompt ng-cloak">{{client.prompts.gender_identity.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="gender_identity">Gender Identity</label>

                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="gender_identity"
                                           ui-event="{ focusin : 'client.prompts.gender_identity.show=true',
                                   focusout: 'client.prompts.gender_identity.show=showPrompts'}"
                                           ng-model="client.current.gender_identity" ng-disabled="isFlipped"/>
                                </div>
                            </div>
                            <!-- preferred_pronoun -->
                            <p ng-show="client.prompts.preferred_pronoun.show"
                               class="prompt ng-cloak">{{client.prompts.preferred_pronoun.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="preferred_pronoun">Preferred Pronouns</label>

                                <div class="col-sm-3">
                                    <select class="form-control"
                                            ng-model="client.current.preferred_pronoun" ng-disabled="isFlipped"
                                            ui-event="{ focusin : 'client.prompts.preferred_pronoun.show=true',
                                    focusout: 'client.prompts.preferred_pronoun.show=showPrompts'}">
                                        <option ng-repeat="item in client.options.preferred_pronoun">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.preferred_pronoun">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- preferred_pronoun_notes -->
                            <div class="form-group" ng-hide="client.current.preferred_pronoun != 'other'">
                                <label class="col-sm-3 control-label" for="preferred_pronoun_notes">Description</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="preferred_pronoun_notes" rows="2"
                                      ng-model="client.current.preferred_pronoun_notes"
                                      ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- ethnicity -->
                            <p ng-show="client.prompts.ethnicity.show"
                               class="prompt ng-cloak">{{client.prompts.ethnicity.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="ethnicity">Race/Ethnicity</label>

                                <div class="col-sm-9 checkbox">
                                    <label class="checkbox ng-cloak" ng-repeat="item in client.checkLists['ethnicity']">
                                        <input type="checkbox"
                                               ng-model="item.value" ng-disabled="isFlipped"/> {{item.name}}
                                    </label>
                                </div>
                            </div>
                            <!-- education -->
                            <p ng-show="client.prompts.education.show"
                               class="prompt ng-cloak">{{client.prompts.education.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="education">Education</label>

                                <div class="col-sm-4">
                                    <select class="form-control"
                                            ng-model="client.current.education" ng-disabled="isFlipped"
                                            ui-event="{ focusin : 'client.prompts.education.show=true',
                                    focusout: 'client.prompts.education.show=showPrompts'}">
                                        <option ng-repeat="item in client.options.education">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.education">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- is_student -->
                            <p ng-show="client.prompts.is_student.show"
                               class="prompt ng-cloak">{{client.prompts.is_student.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="is_student">Is Student</label>

                                <div class="col-sm-3" ui-event="{ focusin : 'client.prompts.is_student.show=true',
                        focusout: 'client.prompts.is_student.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="is_student"
                                               ng-model="client.current.is_student" ng-disabled="isFlipped"
                                               id="is_student_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="is_student"
                                               ng-model="client.current.is_student" ng-disabled="isFlipped"
                                               id="is_student_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_student -->
                            <p ng-show="client.prompts.details_student.show && client.current.is_student"
                               class="prompt ng-cloak">{{client.prompts.details_student.text}}</p>

                            <div class="form-group" ng-show="client.current.is_student">
                                <label class="col-sm-3 control-label" for="details_student">College/University</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_student" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_student.show=true',
                                      focusout: 'client.prompts.details_student.show=false'}"
                                      ng-model="client.current.details_student" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- employment -->
                            <p ng-show="client.prompts.employment.show"
                               class="prompt ng-cloak">{{client.prompts.employment.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="employment">Employment</label>

                                <div class="col-sm-9 checkbox">
                                    <label class="checkbox ng-cloak" ng-repeat="item in client.checkLists.employment">
                                        <input type="checkbox" name="employment"
                                               ng-model="item.value" ng-disabled="isFlipped"/> {{item.name}}
                                    </label>
                                </div>
                            </div>
                            <!-- employer -->
                            <p ng-show="client.prompts.employer.show"
                               class="prompt ng-cloak">{{client.prompts.employer.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="employer">Employer</label>

                                <div class="col-sm-9">
                                    <input type="text" name="employer" class="form-control"
                                           ng-model="client.current.employer" ng-disabled="isFlipped"
                                           ui-event="{ focusin : 'client.prompts.employer.show=true',
                                   focusout: 'client.prompts.employer.show=showPrompts'}"/>
                                </div>
                            </div>
                            <!-- job title -->
                            <p ng-show="client.prompts.job_title.show"
                               class="prompt ng-cloak">{{client.prompts.job_title.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="job_title">Job Title</label>

                                <div class="col-sm-9">
                                    <input type="text" name="job_title" class="form-control"
                                           ng-model="client.current.job_title" ng-disabled="isFlipped"
                                           ui-event="{ focusin : 'client.prompts.job_title.show=true',
                                   focusout: 'client.prompts.job_title.show=showPrompts'}"/>
                                </div>
                            </div>
                            <!-- is_licensed_professional -->
                            <p ng-show="client.prompts.is_licensed_professional.show && client.current.state === 'WI'"
                               class="prompt ng-cloak">{{client.prompts.is_licensed_professional.text}}</p>

                            <div class="form-group" ng-show="client.current.state === 'WI'">
                                <label class="col-sm-3 control-label" for="is_licensed_professional">Licensed
                                    Professional</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin : 'client.prompts.is_licensed_professional.show=true',
                                        focusout: 'client.prompts.is_licensed_professional.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="is_licensed_professional"
                                               ng-model="client.current.is_licensed_professional"
                                               ng-disabled="isFlipped"
                                               id="is_licensed_professional_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="is_licensed_professional"
                                               ng-model="client.current.is_licensed_professional"
                                               ng-disabled="isFlipped"
                                               id="is_licensed_professional_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- has_notified_licensing_board -->
                            <p ng-show="client.prompts.has_notified_licensing_board.show && client.current.is_licensed_professional == '1' && client.current.state === 'WI'"
                               class="prompt ng-cloak">{{client.prompts.has_notified_licensing_board.text}}</p>

                            <div class="form-group"
                                 ng-show="client.current.is_licensed_professional == '1' && client.current.state === 'WI'">
                                <label class="col-sm-3 control-label" for="has_notified_licensing_board">Has Notified
                                    Board</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin : 'client.prompts.has_notified_licensing_board.show=true',
                                        focusout: 'client.prompts.has_notified_licensing_board.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_notified_licensing_board"
                                               ng-model="client.current.has_notified_licensing_board"
                                               ng-disabled="isFlipped"
                                               id="has_notified_licensing_board_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_notified_licensing_board"
                                               ng-model="client.current.has_notified_licensing_board"
                                               ng-disabled="isFlipped"
                                               id="has_notified_licensing_board_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- annual income -->
                            <p ng-show="client.prompts.annual_income.show"
                               class="prompt ng-cloak">{{client.prompts.annual_income.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="annual_income">Annual Income</label>

                                <div class="col-sm-2">
                                    <input type="text" name="annual_income" class="form-control"
                                           ng-pattern="/(^[0-9]?[0-9]?[0-9],[0-9]{3}$)|(^0$)/"
                                           ui-event="{ focusin : 'client.prompts.annual_income.show=true',
                                      focusout: 'client.prompts.annual_income.show=showPrompts'}"
                                           ng-model="client.current.annual_income" ng-disabled="isFlipped"/>
                                </div>
                                <div class="col-sm-offset-3 col-sm-9">
                                    <div class="error" ng-show="intake_form.annual_income.$error.pattern">
                                        {{client.errors.annual_income}}</div>
                                </div>
                            </div>
                            <!-- income -->
                            <p ng-show="client.prompts.income.show"
                               class="prompt ng-cloak">{{client.prompts.income.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="income">Income</label>

                                <div class="col-sm-9">
                            <textarea name="income" rows="3" class="form-control"
                                      ui-event="{ focusin : 'client.prompts.income.show=true',
                                      focusout: 'client.prompts.income.show=showPrompts'}"
                                      ng-model="client.current.income" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- expenses -->
                            <p ng-show="client.prompts.expenses.show"
                               class="prompt ng-cloak">{{client.prompts.expenses.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="expenses">Expenses</label>

                                <div class="col-sm-9">
                            <textarea name="expenses" rows="3" class="form-control"
                                      ui-event="{ focusin : 'client.prompts.expenses.show=true',
                                      focusout: 'client.prompts.expenses.show=showPrompts'}"
                                      ng-model="client.current.expenses" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- has_property -->
                            <p ng-show="client.prompts.has_property.show"
                               class="prompt ng-cloak">{{client.prompts.has_property.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_property">Has Property</label>

                                <div class="col-sm-3" ui-event="{ focusin : 'client.prompts.has_property.show=true',
                        focusout: 'client.prompts.has_property.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_property"
                                               ng-model="client.current.has_property" ng-disabled="isFlipped"
                                               id="has_property_no" value="0"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_property"
                                               ng-model="client.current.has_property" ng-disabled="isFlipped"
                                               id="has_property_yes" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- details_property -->
                            <p ng-show="client.prompts.details_property.show && client.current.has_property"
                               class="prompt ng-cloak">{{client.prompts.details_property.text}}</p>

                            <div class="form-group" ng-show="client.current.has_property">
                                <label class="col-sm-3 control-label" for="details_property">Property Details</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="details_property" rows="5"
                                      ui-event="{ focusin : 'client.prompts.details_property.show=true',
                                      focusout: 'client.prompts.details_property.show=false'}"
                                      ng-model="client.current.details_property" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- has_public_assistance -->
                            <p ng-show="client.prompts.has_public_assistance.show"
                               class="prompt ng-cloak">{{client.prompts.has_public_assistance.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_public_assistance">Has Public
                                    Assistance</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin: 'client.prompts.has_public_assistance.show=true',
                                        focusout: 'client.prompts.has_public_assistance.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_public_assistance"
                                               ng-model="client.current.has_public_assistance" ng-disabled="isFlipped"
                                               id="has_public_assistance_no" value="No"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_public_assistance"
                                               ng-model="client.current.has_public_assistance" ng-disabled="isFlipped"
                                               id="has_public_assistance_yes" value="Yes"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- has_medicaid -->
                            <p ng-show="client.prompts.has_medicaid.show"
                               class="prompt ng-cloak">{{client.prompts.has_medicaid.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_medicaid">Has Medicaid</label>

                                <div class="col-sm-3"
                                     ui-event="{ focusin: 'client.prompts.has_medicaid.show=true',
                                         focusout: 'client.prompts.has_medicaid.show=showPrompts'}">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_medicaid"
                                               ng-model="client.current.has_medicaid" ng-disabled="isFlipped"
                                               id="has_medicaid_no" value="No"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_medicaid"
                                               ng-model="client.current.has_medicaid" ng-disabled="isFlipped"
                                               id="has_medicaid_yes" value="Yes"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- payment_ability -->
                            <p ng-show="client.prompts.payment_ability.show"
                               class="prompt ng-cloak">{{client.prompts.payment_ability.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="payment_ability">Can Pay Fees</label>

                                <div class="col-sm-6">
                                    <select class="form-control"
                                            ng-model="client.current.payment_ability" ng-disabled="isFlipped"
                                            ui-event="{ focusin: 'client.prompts.payment_ability.show=true',
                                    focusout: 'client.prompts.payment_ability.show=showPrompts'}">
                                        <option ng-repeat="item in client.options.payment_ability">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.payment_ability">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- has_state_id -->
                            <p ng-show="client.prompts.has_state_id.show"
                               class="prompt ng-cloak">{{client.prompts.has_state_id.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_state_id">Has {{client.current.state}}
                                    State
                                    ID</label>

                                <div class="col-sm-3">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_state_id"
                                               ng-model="client.current.has_state_id" ng-disabled="isFlipped"
                                               id="has_state_id_no" value="No"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_state_id"
                                               ng-model="client.current.has_state_id" ng-disabled="isFlipped"
                                               id="has_state_id_yes" value="Yes"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- has_passport -->
                            <p ng-show="client.prompts.has_passport.show"
                               class="prompt ng-cloak">{{client.prompts.has_passport.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="has_passport">Has Passport</label>

                                <div class="col-sm-3">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_passport"
                                               ng-model="client.current.has_passport" ng-disabled="isFlipped"
                                               id="has_passport_no" value="No"> No
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="has_passport"
                                               ng-model="client.current.has_passport" ng-disabled="isFlipped"
                                               id="has_passport_yes" value="Yes"> Yes
                                    </label>
                                </div>
                            </div>
                            <!-- referrer -->
                            <p ng-show="client.prompts.referrer_id.show"
                               class="prompt ng-cloak">{{client.prompts.referrer_id.text}}</p>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="referrer_id">Referrer</label>

                                <div class="col-sm-9">
                                    <select id="referrer_id" class="form-control"
                                            ng-model="referrer_name" ng-disabled="isFlipped"
                                            ui-event="{ focusin : 'client.prompts.referrer.show=true',
                                                focusout: 'client.prompts.referrer.show=showPrompts'}"
                                            ng-change="changeReferrer()">
                                        <option ng-repeat="name in referrers.names">{{name}}</option>
                                    </select>
                                </div>
                            </div>
                            <!-- date_intake_completed -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="date_intake_completed">Date Intake
                                    Completed</label>

                                <div class="col-sm-3">
                                    <input type="text" name="date_intake_completed" placeholder="mm/dd/yyyy"
                                           class="form-control"
                                           ng-pattern="/^(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/(199\d|[2-9]\d{3})$/"
                                           ng-model="client.current.date_intake_completed" ng-disabled="isFlipped"/>
                                    <div class="error" ng-show="intake_form.date_intake_completed.$error.pattern">
                                        {{client.errors.date}}</div>
                                </div>


                            </div>

                            <!-- notes_intake -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="notes_intake">Intake Notes</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="notes_intake" rows="3"
                                      ng-model="client.current.notes_intake" ng-disabled="isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- interviewer -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="interviewer">Interviewer</label>

                                <div class="col-sm-4">
                                    <input type="text" name="interviewer" class="form-control"
                                           ng-model="client.current.interviewer" ng-disabled="isFlipped"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-3">
                                    <button class="btn btn-default control-btn ng-cloak" ng-show="client.current.id"
                                            ng-disabled="emailInProgress || !noValidChanges() || isFlipped"
                                            ng-click="sendAgreement()"><span class="glyphicon glyphicon-envelope"></span>
                                        Email Agreement
                                    </button>
                                </div>
                                <div class="col-sm-offset-3 col-sm-3">

                                    <button class="btn btn-default control-btn ng-cloak" ng-click="confirmDelete()"
                                            ng-hide="role==='User'"
                                            ng-disabled="client.isNew()"><span class="glyphicon glyphicon-trash"></span>
                                        Delete
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end intake_form -->
                    <div class="back" ng-class="bannerReset">
                        <div id="back-form" ng-form name="case_form" class="form-horizontal">
                            <!-- is_accepted -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="optionsAccepted">Case Accepted</label>

                                <div class="col-sm-3">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="optionsAccepted"
                                               ng-model="client.current.is_accepted"
                                               ng-disabled="!isFlipped" id="optionsAcceptedYes"
                                               value="Yes"> Yes
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="optionsAccepted"
                                               ng-model="client.current.is_accepted"
                                               ng-disabled="!isFlipped" id="optionsAcceptedNo"
                                               value="No"> No
                                    </label>
                                </div>
                            </div>
                            <!-- reasons_for_rejection -->
                            <div class="form-group" ng-hide="client.current.is_accepted == 'Yes'">
                                <label class="col-sm-3 control-label" for="reasons_for_rejection">Reasons for
                                    Rejection</label>

                                <div class="col-sm-3">
                                    <select class="form-control"
                                            ng-model="client.current.reasons_for_rejection"
                                            ng-disabled="!isFlipped">
                                        <option ng-repeat="item in client.options.reasons_for_rejection">{{item}}</option>
                                    </select>
                                    <table ng-show="showOverlayOptions" class="options-overlay ng-cloak">
                                        <tr ng-repeat="item in client.options.reasons_for_rejection">
                                            <td>{{item}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- on_hold -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="optionsOnHold">On Hold</label>

                                <div class="col-sm-3">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="optionsOnHold" ng-model="client.current.on_hold"
                                               ng-disabled="!isFlipped" id="optionsOnHoldYes"
                                               value="1"> Yes
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="optionsOnHold" ng-model="client.current.on_hold"
                                               ng-disabled="!isFlipped" id="optionsOnHoldNo"
                                               value="0"> No
                                    </label>
                                </div>
                            </div>
                            <!-- date_of_assignment -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="date_of_assignment">Date of
                                    Assignment</label>

                                <div class="col-sm-3">
                                    <input ui-date="dateOptions" type="text" name="date_of_assignment"
                                           placeholder="mm/dd/yyyy" class="form-control"
                                           ng-model="client.current.date_of_assignment" ng-disabled="!isFlipped"/>

                                    <div class="inline-controls">
                                        <a ng-href="/clients/report/{{client.current.id}}" target="_new" class="btn"
                                           ng-show="!newAttorney">
                                            <span class="glyphicon glyphicon-download"></span> Download Report</a>
                                    </div>
                                </div>

                            </div>

                            <!-- firms -->
                            <div id="firm-list" class="form-group">

                                <label class="col-sm-3 control-label" for="firms">Firms</label>

                                <div class="col-sm-9">
                                    <table class="table table-condensed table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="item in client.firms.elemList">
                                            <td>{{item.name}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="inline-controls">
                                        <button class="btn btn-default" ng-click="toggleFirms()">
                                            <span ng-class="firmsIcon"></span> Assign Firms
                                        </button>
                                        <div ng-form="firm-form" ng-show="assignFirms">
                                            <div class="gridStyle" ng-grid="firmGridOptions"
                                                 ng-disabled="!isFlipped"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- lawyers -->
                            <div class="form-group">

                                <label class="col-sm-3 control-label" for="lawyers">Attorneys</label>

                                <div class="col-sm-9">
                                    <table class="table table-condensed table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Firm</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="item in client.attorneys.dataList">
                                            <td>{{item.name}}</td>
                                            <td>{{item.phone}}</td>
                                            <td>
                                                <a href="mailto:{{item.email}}?&subject={{client.current.preferred_first_name}} {{client.current.preferred_last_name}} (legal name: {{client.current.legal_first_name}} {{client.current.legal_last_name}}) Name Change"
                                                   target="_blank">{{item.email}}</a></td>
                                            <td>{{item.firm_name}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div id="attorney-list" class="inline-controls">
                                        <button class="btn btn-default" ng-click="toggleAttorneys()">
                                            <span ng-class="attorneysIcon"></span> Assign Attorneys
                                        </button>
                                        <div ng-form="attorney-form" ng-show="assignAttorneys && !newAttorney">
                                            <div class="sub-form-control-bar form-inline">
                                                <span>From Firm </span>
                                                <select class="form-control" ng-model="firm_name"
                                                        ng-change="changeFirm()">
                                                    <option ng-repeat="firm in client.firms.elemList">{{firm.name}}</option>
                                                </select>
                                                <button class="btn btn-default" ng-click="toggleNewAttorney()">
                                                    <span class="glyphicon glyphicon-plus-sign"></span> New Attorney
                                                </button>
                                            </div>
                                            <div class="gridStyle" ng-grid="firmAttorneyGridOptions"
                                                 ng-disabled="!isFlipped"></div>
                                        </div>
                                    </div>
                                    <!--- Begin add new attorney form --->
                                    <div ng-form name="new_attorney_form" class="form-horizontal well"
                                         ng-show="assignAttorneys && newAttorney" ng-disabled="!isFlipped">
                                        <div ng-form class="form-inline sub-form">
                                            <label class="sub-form-title">Add New {{firm_name}} Attorney</label>

                                            <div class="sub-form-buttons pull-right">
                                                <button class="btn btn-default" ng-click="cancelNewAttorney()">
                                                    <span class="glyphicon glyphicon-remove-sign"></span> Cancel
                                                </button>
                                                <button class="btn btn-default" ng-click="saveNewAttorney()"
                                                        ng-disabled="new_attorney_form.$pristine || new_attorney_form.$invalid">
                                                    <span class="glyphicon glyphicon-file"></span> Save
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="new_attorney_firm_name">Firm</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="new_attorney_firm_name"
                                                       class="form-control"
                                                       ng-model="firm_name" ng-disabled="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="new_attorney_last_name">Last
                                                Name</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="new_attorney_last_name"
                                                       class="form-control"
                                                       required
                                                       ng-model="new_attorney.attorney.last_name"/>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-8">
                                                <p class="error"
                                                   ng-show="new_attorney_form.new_attorney_last_name.$error.required">
                                                    This is a required entry.</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="new_attorney_first_name">First
                                                Name</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="new_attorney_first_name"
                                                       class="form-control"
                                                       required
                                                       ng-model="new_attorney.attorney.first_name"/>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-8">
                                                <p class="error"
                                                   ng-show="new_attorney_form.new_attorney_first_name.$error.required">
                                                    This is a required entry.</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="new_attorney_middle_name">Middle
                                                Name</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="new_attorney_middle_name"
                                                       class="form-control"
                                                       ng-model="new_attorney.attorney.middle_name"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="new_attorney_nick_name">Nick
                                                Name</label>

                                            <div class="col-sm-8 controls">
                                                <input type="text" name="new_attorney_nick_name"
                                                       class="form-control"
                                                       ng-model="new_attorney.attorney.nick_name"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="new_attorney_title">Title</label>

                                            <div class="col-sm-8 controls">
                                                <input type="text" name="new_attorney_title" class="form-control"
                                                       ng-model="new_attorney.attorney.title"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="is_firm_contact">Is Firm Wide
                                                Contact</label>

                                            <div class="col-sm-3">
                                                <label class="radio radio-inline">
                                                    <input type="radio" name="is_firm_contact"
                                                           ng-model="attorney.current.is_firm_contact"
                                                           id="is_firm_contact_no" value="0"> No
                                                </label>
                                                <label class="radio radio-inline">
                                                    <input type="radio" name="is_firm_contact"
                                                           ng-model="attorney.current.is_firm_contact"
                                                           id="is_firm_contact_yes" value="1"> Yes
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="new_attorney_phone">Phone</label>

                                            <div class="col-sm-3">
                                                <input type="text" name="new_attorney_phone" class="form-control"
                                                       placeholder="999-999-9999x9999"
                                                       ng-pattern="/[0-9]{3}-[0-9]{3}-[0-9]{4}(x[0-9]{1,4})?$/"
                                                       ng-model="new_attorney.attorney.phone"/>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-8">
                                                <div class="error"
                                                     ng-show="new_attorney_form.new_attorney_phone.$error.pattern">
                                                    {{new_attorney.errors.phone}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="new_attorney_cell">Cell</label>

                                            <div class="col-sm-3">
                                                <input type="text" name="new_attorney_cell" class="form-control"
                                                       placeholder="999-999-9999"
                                                       ng-pattern="/[0-9]{3}-[0-9]{3}-[0-9]{4}$/"
                                                       ng-model="new_attorney.attorney.cell"/>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-8">
                                                <div class="error"
                                                     ng-show="new_attorney_form.new_attorney_cell.$error.pattern">
                                                    {{new_attorney.errors.cell}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="new_attorney_fax">Fax</label>

                                            <div class="col-sm-3">
                                                <input type="text" name="new_attorney_fax" class="form-control"
                                                       placeholder="999-999-9999"
                                                       ng-pattern="/[0-9]{3}-[0-9]{3}-[0-9]{4}$/"
                                                       ng-model="new_attorney.attorney.fax"/>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-8">
                                                <div class="error"
                                                     ng-show="new_attorney_form.new_attorney_fax.$error.pattern">
                                                    {{new_attorney.errors.fax}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="new_attorney_email">Email</label>

                                            <div class="col-sm-8">
                                                <input type="email" name="new_attorney_email" class="form-control"
                                                       ng-model="new_attorney.attorney.email"/>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-8">
                                                <div class="error"
                                                     ng-show="new_attorney_form.new_attorney_email.$error.email">
                                                    {{new_attorney.errors.email}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="new_attorney_url">Url</label>

                                            <div class="col-sm-8">
                                                <input type="url" name="new_attorney_url" class="form-control"
                                                       ng-model="new_attorney.attorney.url"/>
                                            </div>
                                            <div class="col-sm-offset-3 col-sm-8">
                                                <div class="error"
                                                     ng-show="new_attorney_form.new_attorney_url.$error.url">
                                                    {{new_attorney.errors.url}}</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="new_attorney_zone">Location</label>

                                            <div class="col-sm-8">
                                                <select name="new_attorney_zone" class="form-control"
                                                        ng-model="new_attorney.zone_name"
                                                        ng-disabled="false" ng-change="changeZone()">
                                                    <option ng-repeat="zone in zones">{{zone.name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="new_attorney_notes_general">Notes</label>

                                            <div class="col-sm-8">
                                        <textarea name="new_attorney_notes_general" rows="5" class="form-control"
                                                  ng-model="new_attorney.attorney.notes_general"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!--- End add new attorney form --->
                                </div>
                            </div>

                            <!-- BEGIN CASE -->
                            <!-- case.date_of_filing -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="date_of_filing">Date of Filing</label>

                                <div class="col-sm-3">
                                    <input ui-date="dateOptions" type="text" name="date_of_filing"
                                           placeholder="mm/dd/yyyy"
                                           class="form-control"
                                           ng-model="client.current.date_of_filing" ng-disabled="!isFlipped"/>
                                </div>
                            </div>
                            <!-- case.court_index_number -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="court_index_number">Court Index
                                    Number</label>

                                <div class="col-sm-3">
                                    <input type="text" name="court_index_number" class="form-control"
                                           ng-model="client.current.court_index_number" ng-disabled="!isFlipped"/>
                                </div>
                            </div>
                            <!-- case.date_of_hearing -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="date_of_hearing">Date of Hearing</label>

                                <div class="col-sm-3">
                                    <input ui-date="dateOptions" type="text" name="date_of_hearing"
                                           placeholder="mm/dd/yyyy"
                                           class="form-control"
                                           ng-model="client.current.date_of_hearing" ng-disabled="!isFlipped"/>
                                </div>
                            </div>
                            <!-- case.judge -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="judge">Judge</label>

                                <div class="col-sm-3">
                                    <select id="judge" class="form-control"
                                            ng-model="client.current.judge" ng-disabled="!isFlipped"
                                            ng-change="changeJudge()">
                                        <option ng-repeat="name in judges.names">{{name}}</option>
                                    </select>
                                </div>
                            </div>
                            <!-- case.notes_hearing -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="notes_hearing">Hearing Notes</label>

                                <div class="col-sm-9">
                            <textarea class="form-control" name="notes_hearing" rows="5"
                                      ng-model="client.current.notes_hearing" ng-disabled="!isFlipped"></textarea>
                                </div>
                            </div>
                            <!-- END CASE -->

                            <!-- name_change_completed -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="optionsCompleted">Name Change</label>

                                <div class="col-sm-3">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="optionsCompleted"
                                               ng-model="client.current.name_change_completed"
                                               ng-disabled="!isFlipped"
                                               id="optionsIncomplete"
                                               value="No"> Incomplete
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="optionsCompleted"
                                               ng-model="client.current.name_change_completed"
                                               ng-disabled="!isFlipped"
                                               id="optionsComplete"
                                               value="Yes"> Completed
                                    </label>
                                </div>
                            </div>
                            <!-- is_closed -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="optionsStatus">Case Status</label>

                                <div class="col-sm-3">
                                    <label class="radio radio-inline">
                                        <input type="radio" name="optionsStatus"
                                               ng-model="client.current.is_closed" ng-disabled="!isFlipped"
                                               id="optionsStatusOpen" value="No"> Open
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" name="optionsStatus"
                                               ng-model="client.current.is_closed" ng-disabled="!isFlipped"
                                               id="optionsStatusClosed" value="Yes"> Closed
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="created">Created:</label>

                                <div class="col-sm-3">
                                    <input type="text" name="created" class="form-control"
                                           ng-model="client.current.created_at" ng-disabled="true"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="updated">Updated:</label>

                                <div class="col-sm-3">
                                    <input type="text" name="updated" class="form-control"
                                           ng-model="client.current.updated_at" ng-disabled="true"/>
                                </div>
                            </div>
                        </div>
                        <!-- End case_form -->
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script type="text/ng-template" id="filter_group_renderer.html">
        <select class="form-control" ng-model="filterGroup.operator">
            <option value="and" ng-selected="true">And</option>
            <option value="or">Or</option>
        </select>
        <button class="btn btn-default" ng-click="addFilterGroup(filterGroup)">Add Sub Group</button>
        <button class="btn btn-default" ng-click="deleteFilterGroup(filterGroup)"
                ng-show="filterGroup.groups.length > 0">Remove Sub Group
        </button>

        <div class="filter-condition" ng-repeat="condition in filterGroup.conditions">
            <select class="form-control" ng-model="condition.field" ng-change="changeConditionField(condition)"
                    ng-options="field.label for field in condition.fields">
            </select>

            <select class="form-control" ng-model="condition.option" ng-change="changeConditionOption(condition)"
                    ng-options="opt.label for opt in condition.options">
            </select>

        <span ng-show="condition.inputs > 0">
            <input class="form-control" ng-show="condition.field.filterInput ==='text'"
                   ng-change="changeConditionValue(condition)"
                   type="text" ng-model="condition.value1"/>
            <input class="form-control" ng-show="condition.field.filterInput ==='date'"
                   ng-change="changeConditionValue(condition)"
                   ui-date="dateOptions" placeholder="mm/dd/yyyy"
                   type="text" ng-model="condition.value1"/>
            <select class="form-control" ng-show="condition.field.filterInput ==='select'"
                    ng-change="changeConditionValue(condition)"
                    ng-model="condition.value1"
                    ng-options="opt for opt in condition.field.options">
            </select>
        </span>

        <span ng-show="condition.inputs === 2">
            to
            <input class="form-control" ng-show="condition.field.filterInput ==='text'"
                   ng-change="changeConditionValue(condition)"
                   type="text" ng-model="condition.value2"/>
            <input class="form-control" ng-show="condition.field.filterInput ==='date'"
                   ng-change="changeConditionValue(condition)"
                   ui-date="dateOptions" placeholder="mm/dd/yyyy"
                   type="text" ng-model="condition.value2"/>
        </span>

            <button class="btn btn-default" ng-click="removeCondition(filterGroup, condition)">
                <span class="glyphicon glyphicon-minus-sign"></span>
            </button>

            <button class="btn btn-default" ng-click="addCondition(filterGroup, condition)">
                <span class="glyphicon glyphicon-plus-sign"></span>
            </button>
        </div>
        <div class="filter-group well" ng-repeat="filterGroup in filterGroup.groups"
             ng-include="'filter_group_renderer.html'"></div>
    </script>

    <script type="text/ng-template" id="confirmClose.html">
        <div class="modal-header">
            <h3 class="modal-title">Unsaved Changes Exist</h3>
        </div>
        <div class="modal-body">
            <p>Unsaved changes exist for this client!</p>
        </div>
        <div class="modal-footer">
            <button class="btn btn-default" ng-click="close()">Discard Changes</button>
            <button class="btn btn-default" ng-click="cancel()">Continue Editing</button>
            <button class="btn btn-primary" ng-click="save()">Save Changes</button>
        </div>
    </script>

    <script type="text/ng-template" id="confirmDelete.html">
        <div class="modal-header">
            <h3 class="modal-title">Confirm Deletion</h3>
        </div>
        <div class="modal-body">
            <p>Are you sure you want to delete all of the data for this client?</p>
        </div>
        <div class="modal-footer">
            <button class="btn btn-default" ng-click="cancel()">Cancel</button>
            <button class="btn btn-primary" ng-click="delete()">Delete</button>
        </div>
    </script>

@stop
