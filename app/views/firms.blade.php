@extends('template')

@section('page-app')ng-app="ncpApp"@stop

@section('title')
    NCP | Firms
@stop

@section('sub_title')
    Firms
@stop

@section('content')

    <div ng-controller="FirmsCtrl">
        <div ng-hide="showFirm()" class="list">
            <div>
                <button class="btn btn-default btn-block" ng-click="newFirm()">
                    <i class="glyphicon glyphicon-plus-sign"></i> <b>Add Firm</b>
                </button>
            </div>
            <div class="gridStyle" ng-grid="gridOptions"></div>
        </div>
        <div ng-show="showFirm()" class="ng-cloak detail">
            <div class="ng-cloak" ng-class="banner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 banner-id">
                            <p>Firm ID: <b>{{firm.current.id}}</b> Name: <b>{{firm.current.full_name}}</b></p>
                        </div>
                        <div class="col-sm-6 banner-buttons">
                            <button class="btn btn-default ng-cloak" ng-hide="role==='User'" ng-click="deleteFirm()"><i
                                        class="glyphicon glyphicon-trash"></i> Delete
                            </button>
                            <button class="btn btn-default ng-cloak" ng-click="closeFirm()"><i
                                        class="glyphicon glyphicon-remove-circle"></i> Done
                            </button>
                            <button class="btn btn-default ng-cloak"
                                    ng-disabled="firm_form.$invalid || firm_form.$pristine" ng-click="saveFirm(false)"><i
                                        class="glyphicon glyphicon-file"></i> Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <form name="firm_form" class="form-horizontal col-sm-12">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="full_name">Full Name</label>

                    <div class="col-sm-6">
                        <input type="text" name="full_name" class="form-control"
                               ng-model="firm.current.full_name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="short_name">Short Name</label>

                    <div class="col-sm-3">
                        <input type="text" name="short_name" class="form-control" ng-model="firm.current.short_name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="donor_levels">Donor Level</label>

                    <div class="col-sm-3">
                        <select id="donor_levels" class="form-control"
                                ng-model="donorLevelName"
                                ng-change="changeDonorLevel()">
                            <option ng-repeat="name in donorLevels.names">{{name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="phone">Phone</label>

                    <div class="col-sm-2">
                        <input type="text" name="phone" class="form-control" placeholder="999-999-9999x9999"
                               ng-model="firm.current.phone" ng-pattern="/[0-9]{3}-[0-9]{3}-[0-9]{4}(x[0-9]{1,4})?$/"/>
                    </div>
                    <div class="col-sm-offset-3 col-sm-6">
                        <div class="error" ng-show="firm_form.phone.$error.pattern">
                            {{firm.errors.phone}}</div>
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="address_1">Address (Line 1)</label>

                    <div class="col-sm-6">
                        <input type="text" name="address_1" class="form-control"
                               ng-model="firm.current.address_1"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="address_2">Address (Line 2)</label>

                    <div class="col-sm-6">
                        <input type="text" name="address_2" class="form-control input-block-level"
                               ng-model="firm.current.address_2"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="zip">Zip Code</label>

                    <div class="col-sm-2">
                        <input type="text" name="zip" class="form-control"
                               ng-model="firm.current.zip" ng-pattern="/[0-9]{5}(-[0-9]{4})?$/"/>
                    </div>
                    <div class="col-sm-offset-3 col-sm-6">
                        <div class="error" ng-show="firm_form.zip.$error.pattern">
                            {{firm.errors.zip}}</div>
                    </div>
                </div>
                <div class="form-group">

                    <label class="col-sm-3 control-label" for="is_active">Is Active</label>

                    <div class="col-sm-2">
                        <input type="checkbox" name="is_active" class="input-sm" ng-model="firm.current.is_active"
                               ng-true-value="Yes" ng-false-value="No"/>
                    </div>


                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="url">Url</label>

                    <div class="col-sm-6">
                        <input type="url" name="url" class="form-control input-block-level"
                               ng-model="firm.current.url"/>
                    </div>
                    <div class="col-sm-offset-3 col-sm-6">
                        <div class="error" ng-show="firm_form.url.$error.url">
                            {{firm.errors.url}}</div>
                    </div>
                </div>
                <div class="form-group">

                    <label class="col-sm-3 control-label" for="contacts">Contacts</label>

                    <div class="col-sm-9">
                        <table class="table table-condensed table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Title</th>
                                <th>Region</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="item in firm.current.contacts">
                                <td>{{item.first_name}} {{item.last_name}}</td>
                                <td>{{item.phone}}</td>
                                <td><a href="mailto:{{item.email}}" target="_blank">{{item.email}}</a></td>
                                <td>{{item.title}}</td>
                                <td>{{item.zone_name}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="notes_general">Notes</label>

                    <div class="col-sm-6">
                    <textarea class="form-control input-block-level" name="notes_general" rows="5"
                              ng-model="firm.current.notes_general"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="created">Created:</label>

                    <div class="col-sm-2">
                        <input type="text" name="created" class="form-control" ng-model="firm.current.created_at"
                               ng-disabled="true"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="updated">Updated:</label>

                    <div class="col-sm-2">
                        <input type="text" name="updated" class="form-control" ng-model="firm.current.updated_at"
                               ng-disabled="true"/>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/ng-template" id="confirmClose.html">
        <div class="modal-header">
            <h3 class="modal-title">Unsaved Changes Exist</h3>
        </div>
        <div class="modal-body">
            <p>Unsaved changes exist for this firm!</p>
        </div>
        <div class="modal-footer">
            <button class="btn btn-default" ng-click="close()">Discard Changes</button>
            <button class="btn btn-default" ng-click="cancel()">Continue Editing</button>
            <button class="btn btn-primary" ng-click="save()">Save Changes</button>
        </div>
    </script>

    <script type="text/ng-template" id="confirmDelete.html">
        <div class="modal-header">
            <h3 class="modal-title">Confirm Deletion</h3>
        </div>
        <div class="modal-body">
            <p>Are you sure you want to delete all of the data for this firm?</p>
        </div>
        <div class="modal-footer">
            <button class="btn btn-default" ng-click="cancel()">Cancel</button>
            <button class="btn btn-primary" ng-click="delete()">Delete</button>
        </div>
    </script>

@stop