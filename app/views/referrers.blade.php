@extends('template')

@section('page-app')ng-app="ncpApp"@stop

@section('title')
NCP | Referrers
@stop

@section('sub_title')
Referrers
@stop

@section('content')

<div ng-controller="ReferrersCtrl">
    <div ng-hide="showReferrer()" class="list">
        <div>
            <button class="btn btn-default btn-block"  ng-click="newReferrer()">
                <i class="glyphicon glyphicon-plus-sign"></i> <b>Add Referrer</b>
            </button>
        </div>
        <div class="gridStyle" ng-grid="gridOptions"></div>
    </div>
    <div ng-show="showReferrer()" class="ng-cloak detail">
        <div class="ng-cloak" ng-class="banner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 banner-id">
                        <p>Referrer ID: <b>{{referrer.current.id}}</b> Name: <b>{{referrer.current.name}}</b></p>
                    </div>
                    <div class="col-sm-6 banner-buttons">
                        <button class="btn btn-default ng-cloak" ng-hide="role==='User'" ng-click="deleteReferrer()"><i class="glyphicon glyphicon-trash"></i> Delete
                        </button>
                        <button class="btn btn-default ng-cloak" ng-click="clearReferrer()"><i class="glyphicon glyphicon-remove-circle"></i>
                            Done
                        </button>
                        <button class="btn btn-default ng-cloak" ng-disabled="referrer_form.$invalid || referrer_form.$pristine" ng-click="saveReferrer()"><i class="glyphicon glyphicon-file"></i> Save</button>
                    </div>
                </div>
            </div>
        </div>
        <form name="referrer_form" class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="name">Name</label>

                <div class="col-sm-6">
                    <input type="text" name="name" class="form-control"
                           ng-model="referrer.current.name"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="notes_general">Notes</label>

                <div class="col-sm-6">
                    <textarea class="form-control" name="notes_general" rows="5"
                              ng-model="referrer.current.notes_general"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="created">Created:</label>
                <div class="col-sm-2">
                    <input type="text" name="created" class="form-control" ng-model="referrer.current.created_at" ng-disabled="true"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="updated">Updated:</label>
                <div class="col-sm-2">
                    <input type="text" name="updated" class="form-control" ng-model="referrer.current.updated_at" ng-disabled="true"/>
                </div>
            </div>
        </form>
    </div>
</div>



@stop