@extends('template')

@section('page-app')ng-app="statesApp"@stop

@section('styles')
<link rel="stylesheet" href="/css/jqvmap.css" media="screen"/>
<link rel="stylesheet" href="/css/states.css" />
@stop

@section('title')
NCP | States
@stop


@section('header')
<div class="container">
    <div class="row">
        <div class="col-sm-12 small-header">
            <h1>TLDEF Name Change Project <span class="smaller">50 State Database Preview</span></h1>
        </div>
    </div>
</div>
@stop

@section('content')
<div ng-controller="StateCtrl">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <form>
                    <legend>Browse Our Database By State</legend>
                    <label>Select a state</label>
                    <select id="fullstatename" ng-model="fullStateName" ng-change="changeFullState()">
                        <option ng-repeat="name in statenames">{{name}}</option>
                    </select>
                </form>
                <div vectormap id="vmap" style="width: 360px; height: 270px;">
                </div>
            </div>
            <div class="col-sm-6 state state-preview" ng-bind-html-unsafe="fullstate.html">
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
<script src="/js/jquery.vmap.js"></script>
<script src="/js/maps/jquery.vmap.usa.js"></script>

<script src="/js/statesApp.js"></script>

@stop