<!doctype html>
<html lang="en" class="no-js" ng-app="allStatesApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TLDEF: The Name Change Project</title>
    <link href="http://www.transgenderlegal.org/css/reset.css" rel="stylesheet" type="text/css" />
    <link href="http://www.transgenderlegal.org/css/style.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="http://www.transgenderlegal.org/favicon.ico" />
    <link rel="stylesheet" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/css/states.css" />
    <link rel="stylesheet" href="/css/overrides.css" />
    <link rel="stylesheet" href="/css/angular-ui.css" />
    <script src="/js/lib/modernizr-2.6.2.min.js"></script>
</head>
<body>
<div id="header" class="internal" style="background-image: url('http://www.transgenderlegal.org/images/global/header_image_8.jpg')">
    <a href="http://www.transgenderlegal.org/"><img id="header-logo" src="http://www.transgenderlegal.org/images/global/header_logo.gif" width="182" height="68" alt="Transgender Legal Defense and Education Fund" /></a> <img id="header-title" src="http://www.transgenderlegal.org/images/global/header_title.gif" width="448" height="39" alt="Working for Transgender Equal Rights" /> <br clear="all" />
</div>
<div id="frame">
    <div id="sidebar-left">
        <ul id="navigation">
            <li> <a href="http://www.transgenderlegal.org/page.php?id=2">About TLDEF</a> </li>
            <li> <a class="active" href="http://www.transgenderlegal.org/work_index.php">Our Work</a>
                <ul>
                    <li><a href="http://www.transgenderlegal.org/work_show.php?id=2">Access to Health Care</a></li>
                    <li><a href="http://www.transgenderlegal.org/work_show.php?id=3">Fair Employment</a></li>
                    <li><a href="http://www.transgenderlegal.org/work_show.php?id=4">Educational Equity</a></li>
                    <li><a href="http://www.transgenderlegal.org/work_show.php?id=5">Housing & Shelter</a></li>
                    <li><a href="http://www.transgenderlegal.org/work_show.php?id=6">Sex-Segregated Facilities</a></li>
                    <li><a href="http://www.transgenderlegal.org/work_show.php?id=7">The Name Change Project</a></li>
                    <!-- <li><a class="active" href="test.html">The Name Change Project 50 State Database</a></li> -->
                    <li><a href="http://www.transgenderlegal.org/work_show.php?id=8">Transgender Health Initiative of New York</a></li>
                    <li><a href="http://www.transgenderlegal.org/work_show.php?id=9">Other Issues</a></li>
                </ul>
            </li>
            <li><a href="http://www.transgenderlegal.org/news_index.php">TLDEF in the News</a></li>
            <li><a href="http://www.transgenderlegal.org/press_index.php">Press Releases</a></li>
            <li><a href="http://www.transgenderlegal.org/publications_index.php">Publications</a></li>
            <li><a href="http://www.transgenderlegal.org/page.php?id=4">Jobs/Internships</a></li>
            <li><a href="http://www.transgenderlegal.org/page.php?id=7">Support TLDEF</a></li>
            <li><a href="http://www.transgenderlegal.org/page.php?id=10">Contact Us</a></li>
        </ul>
        <div id="sidebar-logos">
            <a href="http://www.transgenderlegal.org/work_show.php?id=8"><img id="thiny-logo" src="http://www.transgenderlegal.org/images/global/thiny_logo.gif" width="108" height="52" alt="Transgender Health Initiative of NY" /></a> <a href="http://www.transgenderlegal.org/work_show.php?id=7"><img id="name-change-logo" src="http://www.transgenderlegal.org/images/global/name_change_logo.gif" width="103" height="43" alt="The Name Change Project" /></a>
        </div>
    </div>
    <div id="center">
        <div id="breadcrumbs">
            <a href="http://www.transgenderlegal.org/">Home</a>&nbsp;&gt;&nbsp; <a href="http://www.transgenderlegal.org/work_index.php">Our Work</a>&nbsp;&gt;&nbsp;
        </div>
        <div id="content-states">
            <h2>Legal Name Change 50 State Database</h2>
            <div ng-controller="AllStateCtrl">
                <div class="state well" ng-bind-html-unsafe="allstates.html">
                </div>
            </div>
        </div>
    </div>
    <div id="sidebar-right">
        <div class="sidebar-contents">
            <div id="sidebar-donate">
                <a href="http://www.nycharities.org/donate/c_donate.asp?CharityCode=1991" target="popup" onclick="openWin(); void(0);"><img src="http://www.transgenderlegal.org/images/global/header_donate.jpg" width="150" height="48" alt="Donate to TLDEF through NYCharities.org" /></a>
            </div>
            <h2>
                Join Our Mailing List
            </h2>
            <form name="ccoptin" action="http://ui.constantcontact.com/d.jsp" target="popup" onsubmit="openWin()" method="post" id="ccoptin">
                <input class="text" type="text" name="ea" value="Enter your e-mail address" onfocus="if(this.value=='Enter your e-mail address') { this.value=''; }" />
                <input class="submit" type="submit" name="go" value="Sign Up!" />
                <input type="hidden" name="m" value="1101546628533">
                <input type="hidden" name="p" value="oi">
                <a class="nocolor" style="font-size:10px" href="http://www.transgenderlegal.org/page.php?id=9">Privacy&nbsp;Policy</a>
            </form>
            <br />
            <br />
            <h1>
                Related Topics
            </h1>
            <h3>
                <a href="http://www.transgenderlegal.org/page.php?id=40">Getting to Our Office</a>
            </h3>
            <p>
                We're located at 151 West 19th Street in Manhattan, between 6th and 7th Avenues. The office is on the 11th floor, in Suite... <a class="more" href="http://www.transgenderlegal.org/page.php?id=40">more</a>
            </p>
            <h3>
                <a href="http://www.transgenderlegal.org/headline_show.php?id=43">Say My Name!</a>
            </h3>
            <p>
                It's been a year since we launched the Name Change Project, with the simple but revolutionary idea that... <a class="more" href="http://www.transgenderlegal.org/headline_show.php?id=43">more</a>
            </p>
            <h3>
                <a href="http://www.transgenderlegal.org/press_show.php?id=312">TLDEF Applauds New Policy on Passport Gender Changes</a>
            </h3>
            <p>
                The Transgender Legal Defense &amp; Education Fund (TLDEF) applauds the State Department's adoption today of new... <a class="more" href="http://www.transgenderlegal.org/press_show.php?id=312">more</a>
            </p>
            <h3>
                <a class="external" href="http://query.nytimes.com/gst/fullpage.html?res=9E0CEFD8113FF934A35752C1A9609C8B63">NYC Birth Certificates</a>
            </h3>
            <p>
                Separating anatomy from what it means to be a man or a woman, New York City is moving forward with a plan to let people... <a class="more" href="http://query.nytimes.com/gst/fullpage.html?res=9E0CEFD8113FF934A35752C1A9609C8B63">more</a>
            </p>
        </div>
    </div>
    <br clear="all" />
</div>
<div id="footer">
    Copyright &copy; 2012 Transgender Legal Defense &amp; Education Fund, Inc.&nbsp;&nbsp;|&nbsp;&nbsp;151 West 19th Street, Suite 1103, New York, NY 10011&nbsp;&nbsp;&nbsp;Tel: 646.862.9396&nbsp;&nbsp;&nbsp;Fax: 914.920.4057
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/lib/jquery-1.9.1.min.js"><\/script>')</script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.0.5/angular.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/lib/angular-1.0.5/angular.min.js"><\/script>')</script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.5/angular-resource.min.js">
<script>window.jQuery || document.write('<script src="/js/lib/angular-1.0.5/angular-resource.min.js"><\/script>')</script>
<script src="/js/lib/angular-ui/angular-ui.min.js"></script>
<script src="/js/lib/bootstrap.min.js"></script>
<script src="/js/app.js"></script>
<script src="/js/services.js"></script>
<script src="/js/controllers.js"></script>
<script src="/js/filters.js"></script>
<script src="/js/directives.js"></script>
</body>
</html>
