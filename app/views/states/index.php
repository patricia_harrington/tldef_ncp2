<!doctype html>
<html lang="en" class="no-js" ng-app="statesApp">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>TLDEF: The Name Change Project</title>
	<link href="http://www.transgenderlegal.org/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="http://www.transgenderlegal.org/css/style.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="http://www.transgenderlegal.org/favicon.ico" />
	<link rel="stylesheet" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/css/states.css" />
    <link rel="stylesheet" href="/css/overrides.css" />
	<link rel="stylesheet" href="/css/jqvmap.css" media="screen" />
	<link rel="stylesheet" href="/css/angular-ui.css" />
    <script src="/js/lib/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript">
    function openWin()
    {
        window.open("","popup",'menubar=0,resizable=0,scrollbars=1,statusbar=0,location=0,width=770,height=600');
    }
    </script>
</head>
<body>
<div id="header" class="internal" style="background-image: url('http://www.transgenderlegal.org/images/global/header_image_8.jpg')">
	<a href="http://www.transgenderlegal.org/"><img id="header-logo" src="http://www.transgenderlegal.org/images/global/header_logo.gif" width="182" height="68" alt="Transgender Legal Defense and Education Fund" /></a> <img id="header-title" src="http://www.transgenderlegal.org/images/global/header_title.gif" width="448" height="39" alt="Working for Transgender Equal Rights" /> <br clear="all" />
</div>
<div id="frame">
	<div id="sidebar-left">
		<ul id="navigation">
			<li> <a href="http://www.transgenderlegal.org/page.php?id=2">About TLDEF</a> </li>
			<li> <a class="active" href="http://www.transgenderlegal.org/work_index.php">Our Work</a> 
			<ul>
				<li><a href="http://www.transgenderlegal.org/work_show.php?id=2">Access to Health Care</a></li>
				<li><a href="http://www.transgenderlegal.org/work_show.php?id=3">Fair Employment</a></li>
				<li><a href="http://www.transgenderlegal.org/work_show.php?id=4">Educational Equity</a></li>
				<li><a href="http://www.transgenderlegal.org/work_show.php?id=5">Housing & Shelter</a></li>
				<li><a href="http://www.transgenderlegal.org/work_show.php?id=6">Sex-Segregated Facilities</a></li>
				<li><a href="http://www.transgenderlegal.org/work_show.php?id=7">The Name Change Project</a></li>
				<!-- <li><a class="active" href="test.html">The Name Change Project 50 State Database</a></li> -->
				<li><a href="http://www.transgenderlegal.org/work_show.php?id=8">Transgender Health Initiative of New York</a></li>
				<li><a href="http://www.transgenderlegal.org/work_show.php?id=9">Other Issues</a></li>
			</ul>
			</li>
			<li><a href="http://www.transgenderlegal.org/news_index.php">TLDEF in the News</a></li>
			<li><a href="http://www.transgenderlegal.org/press_index.php">Press Releases</a></li>
			<li><a href="http://www.transgenderlegal.org/publications_index.php">Publications</a></li>
			<li><a href="http://www.transgenderlegal.org/page.php?id=4">Jobs/Internships</a></li>
			<li><a href="http://www.transgenderlegal.org/page.php?id=7">Support TLDEF</a></li>
			<li><a href="http://www.transgenderlegal.org/page.php?id=10">Contact Us</a></li>
		</ul>
		<div id="sidebar-logos">
            <a href="http://www.transgenderlegal.org/work_show.php?id=7"><img id="name-change-logo" src="http://www.transgenderlegal.org/images/global/name_change_logo.gif" width="103" height="43" alt="The Name Change Project" /></a>
		</div>
	</div>
	<div id="center">
		<div id="breadcrumbs">
			<a href="http://www.transgenderlegal.org/">Home</a>&nbsp;&gt;&nbsp; <a href="http://www.transgenderlegal.org/work_index.php">Our Work</a>&nbsp;&gt;&nbsp; 
		</div>
		<div id="content-states">
			<h2>Name Change Project 50 State Database</h2>
			<div ng-controller="StateCtrl">
				<div class="row">
					<div id=tldef-content class="tabbable">
						<ul class="nav nav-pills tldef">
							<li class="active"><a href="#browse-pane" data-toggle="tab">Browse By State</a></li>
							<li><a href="#report-pane" data-toggle="tab">Custom Report</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane" id="report-pane">
								<div class="well">
									<form name="reportForm">
										<legend>Generate A Customized Report</legend> <span class="help-block">If you join our mailing list by entering your email address, you can generate a customized report based on your state of birth and the state in which you currently reside.</span> <label>Enter your email address</label> 
										<input size="16" type="email" name="emailaddress" required ng-model="emailAddress" placeholder="Email">
										</input>
										<span class="error" ng-show="reportForm.emailaddress.$error.required">Email Address Is Required.</span>
                                        <span class="error" ng-show="reportForm.emailaddress.$error.email">A Valid Email Address Is Required.</span>
                                        <label>Select the state where you were born</label>
										<select id="birth_states" name="birth_states" ng-disabled="reportForm.$invalid" ng-options="name for name in statenames" ng-model="birthStateName" ng-change="changeBirthState()">
											<option value="">-- select a state --</option>
										</select>
										<label>Select the state where you reside now</label> 
										<select id="resident_states" name="resident_states" ng-disabled="reportForm.$invalid" ng-options="name for name in statenames" ng-model="residentStateName" ng-change="changeResidentState()">
											<option value="">-- select a state --</option>
										</select>
									</form>
								</div>
								<div class="well">
									<ul class="nav nav-tabs nav-stacked">
										<li class="active"><a href="#birth-pane" data-toggle="tab">{{birthStateName}} Birth Certificate Information</a></li>
										<li><a href="#resident-pane" data-toggle="tab" ">{{residentStateName}} Resident Information</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="birth-pane">
											<div class="state" ng-bind-html-unsafe="birthstate.html">
											</div>
										</div>
										<div class="tab-pane" id="resident-pane">
											<div class="state" ng-bind-html-unsafe="residentstate.html">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane active" id="browse-pane">
								<div class="well">
									<form>
										<legend>Browse Our Database By State</legend> <label>Select a state</label> 
										<select id="fullstatename" ui-select2 ng-model="fullStateName" ng-change="changeFullState()">
											<option data-placeholder value="">-- select a state --</option>
											<option ng-repeat="name in statenames">{{name}}</option>
										</select>
									</form>
									<div vectormap id="vmap" style="width: 420px; height: 300px;">
									</div>
								</div>
								<div class="state well" ng-bind-html-unsafe="fullstate.html">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="sidebar-right">
		<div class="sidebar-contents">
			<div id="sidebar-donate">
				<a href="http://www.nycharities.org/donate/c_donate.asp?CharityCode=1991" target="popup" onclick="openWin(); void(0);"><img src="http://www.transgenderlegal.org/images/global/header_donate.jpg" width="150" height="48" alt="Donate to TLDEF through NYCharities.org" /></a> 
			</div>
			<h2>
				Join Our Mailing List 
			</h2>
			<form name="ccoptin" action="http://ui.constantcontact.com/d.jsp" target="popup" onsubmit="openWin()" method="post" id="ccoptin">
				<input class="text" type="text" name="ea" value="Enter your e-mail address" onfocus="if(this.value=='Enter your e-mail address') { this.value=''; }" />
				<input class="submit" type="submit" name="go" value="Sign Up!" />
				<input type="hidden" name="m" value="1101546628533">
				<input type="hidden" name="p" value="oi">
				<a class="nocolor" style="font-size:10px" href="http://www.transgenderlegal.org/page.php?id=9">Privacy&nbsp;Policy</a> 
			</form>
			<br />
			<br />
			<h1>
				Related Topics 
			</h1>
			<h3>
				<a href="http://www.transgenderlegal.org/page.php?id=40">Getting to Our Office</a> 
			</h3>
			<p>
				We're located at 151 West 19th Street in Manhattan, between 6th and 7th Avenues. The office is on the 11th floor, in Suite... <a class="more" href="http://www.transgenderlegal.org/page.php?id=40">more</a> 
			</p>
			<h3>
				<a href="http://www.transgenderlegal.org/headline_show.php?id=43">Say My Name!</a> 
			</h3>
			<p>
				It's been a year since we launched the Name Change Project, with the simple but revolutionary idea that... <a class="more" href="http://www.transgenderlegal.org/headline_show.php?id=43">more</a> 
			</p>
			<h3>
				<a href="http://www.transgenderlegal.org/press_show.php?id=312">TLDEF Applauds New Policy on Passport Gender Changes</a> 
			</h3>
			<p>
				The Transgender Legal Defense &amp; Education Fund (TLDEF) applauds the State Department's adoption today of new... <a class="more" href="http://www.transgenderlegal.org/press_show.php?id=312">more</a> 
			</p>
			<h3>
				<a class="external" href="http://query.nytimes.com/gst/fullpage.html?res=9E0CEFD8113FF934A35752C1A9609C8B63">NYC Birth Certificates</a> 
			</h3>
			<p>
				Separating anatomy from what it means to be a man or a woman, New York City is moving forward with a plan to let people... <a class="more" href="http://query.nytimes.com/gst/fullpage.html?res=9E0CEFD8113FF934A35752C1A9609C8B63">more</a> 
			</p>
		</div>
	</div>
	<br clear="all" />
</div>
<div id="footer">
	Copyright &copy; 2014 Transgender Legal Defense &amp; Education Fund, Inc.&nbsp;&nbsp;|&nbsp;&nbsp;151 West 19th Street, Suite 1103, New York, NY 10011&nbsp;&nbsp;&nbsp;Tel: 646.862.9396&nbsp;&nbsp;&nbsp;Fax: 914.920.4057
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/lib/jquery-2.0.3.min.js"><\/script>')</script>
<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
<script>window.jQuery || document.write('<script src="/js/lib/jquery-ui-1.10.1.custom.min.js"><\/script>')</script>
<script src="/js/lib/underscore.min.js"></script>

<!--
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.1.4/angular.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/lib/angular-1.1.4/angular.min.js"><\/script>')</script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.1.4/angular-resource.min.js">
<script>window.jQuery || document.write('<script src="/js/lib/angular-1.1.4/angular-resource.min.js"><\/script>')</script>
-->
<script src="/js/lib/angular-1.2.0/angular.min.js"></script>
<script src="/js/lib/angular-1.2.0/angular-resource.min.js"></script>
<script src="/js/lib/angular-ui/ui-event.min.js"></script>
<script src="/js/lib/bootstrap.min.js"></script>

<script src="/js/jquery.vmap.js"></script>
<script src="/js/maps/jquery.vmap.usa.js"></script>

<script src="/js/statesApp.js"></script>
<script src="/js/services.js"></script>
<script src="/js/controllers.js"></script>
<script src="/js/filters.js"></script>
<script src="/js/directives.js"></script>

</body>
</html>
