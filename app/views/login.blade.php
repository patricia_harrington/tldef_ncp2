@extends('template')

@section('page-app')ng-app="ncpApp"@stop

@section('body-attributes')class="login-page"@stop

@section('title')
NCP Login
@stop

@section('content')

<div id="login-form" class="container">

<%Form::open(array('url' => 'login','class' => 'form-signin'))%>

     <!-- heading -->
     <h2 class="form-signin-heading">Please sign in</h2>

     <!-- username field -->
     <% Form::label('username', 'Username') %>
     <% Form::text('username', null, array('class' => 'form-control')) %>
    
     <!-- password field -->
     <% Form::label('password', 'Password') %>
     <% Form::password('password', array('class' => 'form-control')) %>

    <% Form::hidden('role', $role) %>
     <!-- submit button -->
     <p><% Form::submit('Login', array('class' => 'btn btn-default btn-large btn-primary')) %></p>
    <!-- check for login errors flash var -->
    @if (Session::has('login_errors'))
    <p class="error">Username or password incorrect.</p>
    @endif

<%Form::close()%>

</div>

@stop