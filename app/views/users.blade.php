@extends('template')

@section('page-app')ng-app="ncpApp"@stop

@section('title')
NCP | Users
@stop

@section('sub_title')
Users
@stop

@section('content')

<div ng-controller="UsersCtrl">

    <div ng-hide="showUser()" class="list">
        <div>
            <button class="btn btn-default btn-block"  ng-click="newUser()">
                <i class="glyphicon glyphicon-plus-sign"></i> <b>Add User</b>
            </button>
        </div>
        <div class="gridStyle" ng-grid="gridOptions"></div>
    </div>

    <div ng-show="showUser()" class="ng-cloak detail">
        <div class="ng-cloak" ng-class="banner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 banner-id">
                        <p>User ID: <b>{{user.current.id}}</b> Name: <b>{{user.current.username}}</b></p>
                    </div>
                    <div class="col-sm-6 banner-buttons">
                        <button class="btn btn-default ng-cloak" ng-hide="role==='User'" ng-click="deleteUser()"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                        <button class="btn btn-default ng-cloak" ng-click="clearUser()"><i class="glyphicon glyphicon-remove-circle"></i> Done
                        </button>
                        <button class="btn btn-default ng-cloak" ng-disabled="user_form.$invalid || user_form.$pristine" ng-click="saveUser()"><i class="glyphicon glyphicon-file"></i> Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div ng-form name="user_form" class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="username">User Name</label>

                <div class="col-sm-6 controls">
                    <input type="text" name="username" class="form-control"
                           ng-model="user.current.username"/>
                </div>
            </div>
            <div class="form-group">

                <label class="col-sm-3 control-label" for="password">Password</label>

                <div class="col-sm-6">
                    <div ng-form name="password_form" class="form-inline">

                        <input type="password" ng-disabled="!editPassword" name="password1" class="form-control" ng-model="password1"/>
                        <label class="text" ng-show="editPassword"> Verify Password <input type="password" name="password2" class="form-control" ng-model="password2"/></label>
                        <button class="btn btn-default" ng-click="togglePassword()"><i ng-class="passwordIcon"></i>{{passwordAction}}</button>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="email">Email</label>
                <div class="col-sm-6">
                    <input type="email" name="email" class="form-control" ng-model="user.current.email"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="role">Role</label>
                <div class="col-sm-3">
                    <select id="role" class="form-control" ng-model="user.current.role">
                        <option ng-repeat="role in user.roles">{{role}}</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="active">Active</label>

                <div class="col-sm-1">
                    <input type="checkbox" name="active" class="input-small" ng-model="user.current.active"
                           ng-true-value="Yes" ng-false-value="No"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="created">Created:</label>
                <div class="col-sm-2">
                    <input type="text" name="created" class="form-control" ng-model="user.current.created_at" ng-disabled="true"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="updated">Updated:</label>
                <div class="col-sm-2">
                    <input type="text" name="updated" class="form-control" ng-model="user.current.updated_at" ng-disabled="true"/>
                </div>
            </div>
        </div>
    </div>
</div>



@stop
