@extends('template')

@section('page-app')ng-app="ncpApp"@stop
@section('styles')
    <link rel="stylesheet" href="/css/codemirror.min.css">
@stop

@section('title')
    NCP | States
@stop


@section('header')
    <div class="container">
        <div class="row">
            <div class="hidden-xs col-sm-12 small-header">
                <h1>TLDEF Name Change Project <span class="smaller">States Editor v<span app-version></span></span></h1>

                <p class="pull-right"><a href="http://daringfireball.net/projects/markdown/basics"
                                         class="btn btn-default" target="_blank"> <i
                                class="glyphicon glyphicon-book"></i> Markdown
                        Syntax Guide </a></p>
                <!--            <button-->
                <!--                popover="Double clicking on the left pane toggles between edit and preview mode. The right pane displays the published version."-->
                <!--                popover-placement="left" popover-title="Release Notes" class="btn pull-right"> Version <span app-version></span> --->
                <!--                Release Notes-->
                <!--            </button>-->
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="container" ng-controller="StateEditorCtrl">
        <div class="row col-sm-12">
            <div class="col-sm-6 form-horizontal">
                <div class="form-group">
                    <label class="control-label" for="state_name">State </label>
                    <select class="select form-control ng-cloak" name="state_name" ng-model="fullStateName"
                            ng-change="getState()">
                        <option ng-repeat="name in statenames">{{name}}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label" for="section_name">Section </label>
                    <select class="select form-control ng-cloak" name="section_name" ng-model="editSection"
                            ng-disabled="noState"
                            ng-change="changeEditSection()">
                        <option ng-repeat="section in sections" value="{{section.id}}">{{section.text}}</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <button class="btn btn-default control-btn pull-right" ng-disabled="noInput" ng-click="putState()">
                    <i class="glyphicon glyphicon-file"></i> Save
                </button>
                <button class="btn btn-default control-btn pull-right" ng-disabled="noInput" ng-click="getState()">
                    <i class="glyphicon glyphicon-remove-circle"></i> Cancel
                </button>
            </div>
        </div>
        <div class="row col-12">
            <div class="col-sm-6">
                <div class="edit-pane well">
                        <textarea ui-codemirror="editorOptions" class="ng-cloak" ng-model="editSectionText"
                                  ng-change="setInput()"></textarea>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="relative-face-container">
                    <div class="faces" ng-class="flipClass">
                        <div class="back">
                            <div class="preview-header">Editing: Unsaved changes exist.</div>
                            <ncp-markdown-preview markdown="editSectionText"></ncp-markdown-preview>
                        </div>
                        <div class="front" ng-hide="noState">
                            <div class="preview-header ng-cloak">Published: {{timestamp}}</div>
                            <ncp-state-preview html="fullstate.html"></ncp-state-preview>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@stop

@section('scripts')

    <script src="/js/lib/codemirror/codemirror.min.js"></script>
    <script src="/js/lib/codemirror/markdown.min.js"></script>
    <script src="/js/lib/showdown.min.js"></script>

@stop
