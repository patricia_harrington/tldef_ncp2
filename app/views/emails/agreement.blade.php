<!DOCTYPE html>
<html>
<head>
    <title>TLDEF Client Agreement</title>
</head>
<body>
<p>Dear <% $first_name %>,

<p>Welcome to the TLDEF Name Change Project.</p>

<p>Please click this <a href="http://<% $reg_server %>/agreement/<% $guid %>">link</a> to read and complete the TLDEF Client Agreement.</p>
<p>Once you have completed the agreement, we will begin the process of matching you with an attorney.</p>

<p>Sincerely,</p>

<div id=":yr" class="Ar Au" style="display: block;">
    <div id=":1ub" class="Am Al editable Xp0HJf-LW-avf" hidefocus="true" aria-label="Signature" g_editable="true"
         role="textbox" contenteditable="true" style="direction: ltr;"><span
            style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:13px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);display:inline!important;float:none">Patricia Harrington<br>Intake Coordinator<br></span><span
            style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:13px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px;background-color:rgb(255,255,255);display:inline!important;float:none"></span>

        <div
            style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:13px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px">
            <br></div>
        <div
            style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:13px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px">
            <img src="http://www.transgenderlegal.org/media/uploads/doc_310.jpg"><br></div>
        <div
            style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:13px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px">
            <br></div>
        <div
            style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:13px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px">
            Transgender Legal Defense
        </div>
        <div
            style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:13px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px">
            &amp; Education Fund, Inc.<br><br></div>
        <div
            style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:13px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px">
            151 West 19th Street<br>Suite 1103<br>New York, New York&nbsp; 10011<br><br>t: 646.862.9396, x706<br>f:
            914.920.4057<br>e:<span>&nbsp;</span><a href="mailto:pharrington@transgenderlegal.org" target="_blank">pharrington@transgenderlegal.org</a><br>w:<span>&nbsp;</span><a
                href="http://transgenderlegal.org/" target="_blank">transgenderlegal.org</a></div>
        <div
            style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:13px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px">
            <br></div>
        <div
            style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:13px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px">
            <i>Follow us on<span>&nbsp;</span><a href="http://www.twitter.com/tldef" target="_blank">Twitter</a><span>&nbsp;</span>and<span>&nbsp;</span><a
                    href="http://www.facebook.com/TransLegalDefense" target="_blank">Facebook</a>.</i></div>
        <div
            style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:13px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px">
            <i><br></i></div>
        <div
            style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:13px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px">
            <a href="http://transgenderlegal.org/page.php?id=7" target="_blank"><b>Support our
                    work</b></a><span>&nbsp;</span>for equal rights.<br><br><span style="font-size:x-small">This email may contain privileged, confidential and/or proprietary information intended only for the person(s) named. Any use, distribution, copying or disclosure to another person is strictly prohibited. If you are not the addressee indicated in this message (or responsible for delivery of the message to such person), you may not copy or deliver this message to anyone. In such case, you should destroy this message and kindly notify the sender by reply email.</span>
        </div>
    </div>
</div>
</body>
</html>