@extends('template')

@section('page-app')ng-app="ncpApp"@stop

@section('styles')
    <link rel="stylesheet" href="/css/dangle.css">
@stop

@section('title')
    NCP | Reports
@stop

@section('sub_title')
    Reports
@stop

@section('content')

    <div ng-controller="SummaryCtrl">
        <div class="container">
            <div class="row">

                <div id="sidebar" class="col-sm-3">

                    <div class="list-group">
                        <a class="list-group-item" ng-class="active.summary" href="#/summary">Summary Statistics</a>
                        <a class="list-group-item" ng-class="active.demographics" href="#/demographics">Demographic Statistics</a>
                        <a class="list-group-item" ng-class="active.regional" href="#/regional">Regional Statistics</a>
                        <a class="list-group-item" ng-class="active.by_county" href="#/by_county">By State &amp; County</a>
                        <a class="list-group-item" ng-class="active.by_firm" href="#/by_firm">By Firm</a>
                        <a class="list-group-item" ng-class="active.by_referrer" href="#/by_referrer">By Referrer</a></li>
                        <a class="list-group-item" ng-class="active.by_interviewer" href="#/by_interviewer">By Interviewer</a>
                    </div>

                </div>
                <div id="content" style="padding:10px;" class="ng-cloak col-sm-6" ng-show="ready">

                    <div class="row" ng-view>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script src="/js/lib/d3.v3.min.js"></script>
    <!--<script src="/js/lib/dangle.module.min.js"></script>-->
    <script src="/js/lib/dangle.bar.min.js"></script>
    <script src="/js/lib/dangle.pie.min.js"></script>
@stop