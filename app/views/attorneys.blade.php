@extends('template')

@section('page-app')ng-app="ncpApp"@stop

@section('title')
    NCP | Attorneys
@stop

@section('sub_title')
    Attorneys
@stop

@section('content')

    <div ng-controller="AttorneysCtrl">
        <div ng-hide="showAttorney()" class="ng-cloak list">
            <div ng-form name="attorney_filters" class="ng-cloak form-inline well">
                <span class="filter-label">Firm </span>
                <select id="firm_name" class="form-control" ng-model="selectedFirm" ng-change="selectFirm()">
                    <option ng-repeat="name in firm.options">{{name}}</option>
                </select>

                <span class="filter-label"> Region </span>
                <select class="form-control ng-cloak"
                        ng-model="zoneFilter" ng-change="filterFirmByZone()">
                    <option ng-repeat="zone in zones">{{zone.name}}</option>
                </select>
            </div>
            <div>
                <div class="progress progress-striped active ng-cloak" ng-show="loading">
                    <div class="bar" style="width: 100%;"></div>
                </div>
                <button class="btn btn-default btn-block" ng-click="newAttorney()" ng-show="firm.valid()">
                    <i class="glyphicon glyphicon-plus-sign"></i> <b>Add Attorney</b> <br>
                    <span class="firm-name">Firm: {{selectedFirm}}</span>
                </button>
            </div>
            <div class="gridStyle" ng-grid="attorneyGridOptions"></div>
        </div>
        <div ng-show="showAttorney()" class="ng-cloak detail">
            <div class="ng-cloak" ng-class="banner">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 banner-id">
                            <p>Attorney ID: <b>{{attorney.current.id}}</b> Name:
                                <b>{{attorney.current.first_name}} {{attorney.current.last_name}}</b></p>
                        </div>
                        <div class="col-sm-6 banner-buttons">
                            <button class="btn btn-default ng-cloak" ng-hide="role==='User'" ng-click="deleteAttorney()"><i
                                        class="glyphicon glyphicon-trash"></i> Delete
                            </button>
                            <button class="btn btn-default ng-cloak" ng-click="closeAttorney()"><i class="glyphicon glyphicon-remove-circle"></i>
                                Done
                            </button>
                            <button class="btn btn-default ng-cloak" ng-disabled="attorney_form.$invalid || attorney_form.$pristine"
                                    ng-click="saveAttorney(false)"><i class="glyphicon glyphicon-file"></i> Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <form name="attorney_form" class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="last_name">Last Name</label>

                    <div class="col-sm-6">
                        <input type="text" name="last_name" class="form-control" ng-model="attorney.current.last_name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="first_name">First Name</label>

                    <div class="col-sm-6">
                        <input type="text" name="first_name" class="form-control"
                               ng-model="attorney.current.first_name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="middle_name">Middle Name</label>

                    <div class="col-sm-6">
                        <input type="text" name="middle_name" class="form-control"
                               ng-model="attorney.current.middle_name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="nick_name">Nick Name</label>

                    <div class="col-sm-6">
                        <input type="text" name="nick_name" class="form-control" ng-model="attorney.current.nick_name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="title">Title</label>

                    <div class="col-sm-6">
                        <input type="text" name="title" class="form-control"
                               ng-model="attorney.current.title"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="is_firm_contact">Is Firm Wide Contact</label>

                    <div class="col-sm-6">
                        <label class="radio radio-inline">
                            <input type="radio" name="is_firm_contact"
                                   ng-model="attorney.current.is_firm_contact"
                                   id="is_firm_contact_no" value="0"> No
                        </label>
                        <label class="radio radio-inline">
                            <input type="radio" name="is_firm_contact"
                                   ng-model="attorney.current.is_firm_contact"
                                   id="is_firm_contact_yes" value="1"> Yes
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="is_active">Is Active</label>

                    <div class="col-sm-6">
                        <label class="radio radio-inline">
                            <input type="radio" name="is_active"
                                   ng-model="attorney.current.is_active"
                                   id="is_active_yes" value="0"> No
                        </label>
                        <label class="radio radio-inline">
                            <input type="radio" name="is_active"
                                   ng-model="attorney.current.is_active"
                                   id="is_active_yes" value="1"> Yes
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="phone">Phone</label>

                    <div class="col-sm-3">
                        <input type="text" name="phone" class="form-control" placeholder="999-999-9999x9999"
                               ng-pattern="/[0-9]{3}-[0-9]{3}-[0-9]{4}(x[0-9]{1,4})?$/"
                               ng-model="attorney.current.phone"/>

                        <div class="error" ng-show="attorney_form.phone.$error.pattern">
                            {{attorney.errors.phone}}</div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="cell">Cell</label>

                    <div class="col-sm-3">
                        <input type="text" name="cell" class="form-control" placeholder="999-999-9999"
                               ng-pattern="/[0-9]{3}-[0-9]{3}-[0-9]{4}$/" ng-model="attorney.current.cell"/>

                        <div class="error" ng-show="attorney_form.cell.$error.pattern">
                            {{attorney.errors.cell}}</div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="fax">Fax</label>

                    <div class="col-sm-3">
                        <input type="text" name="fax" class="form-control" placeholder="999-999-9999"
                               ng-pattern="/[0-9]{3}-[0-9]{3}-[0-9]{4}$/" ng-model="attorney.current.fax"/>

                        <div class="error" ng-show="attorney_form.fax.$error.pattern">
                            {{attorney.errors.fax}}</div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="email">Email</label>

                    <div class="col-sm-6">
                        <input type="email" name="email" class="form-control"
                               ng-model="attorney.current.email"/>

                        <div class="error" ng-show="attorney_form.email.$error.email">
                            {{attorney.errors.email}}</div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="url">Url</label>

                    <div class="col-sm-6">
                        <input type="url" name="url" class="form-control"
                               ng-model="attorney.current.url"/>

                        <div class="error" ng-show="attorney_form.url.$error.url">
                            {{attorney.errors.url}}</div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="attorney_firm_name">Firm</label>

                    <div class="col-sm-6">
                        <select id="attorney_firm_name" class="form-control"
                                ng-model="attorney.firm_name"
                                ng-disabled="false"
                                ng-change="changeFirm()">
                            <option ng-repeat="name in firm.names">{{name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="zone">Location</label>

                    <div class="col-sm-6">
                        <select id="zone" class="form-control" ng-model="zone_name"
                                ng-disabled="false" ng-change="changeZone()">
                            <option ng-repeat="zone in zones">{{zone.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="notes_general">Notes</label>

                    <div class="col-sm-6">
                    <textarea class="form-control" name="notes_general" rows="5"
                              ng-model="attorney.current.notes_general"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="created">Created:</label>

                    <div class="col-sm-3">
                        <input type="text" name="created" class="form-control" ng-model="attorney.current.created_at"
                               ng-disabled="true"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="updated">Updated:</label>

                    <div class="col-sm-2">
                        <input type="text" name="updated" class="form-control" ng-model="attorney.current.updated_at"
                               ng-disabled="true"/>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/ng-template" id="confirmClose.html">
        <div class="modal-header">
            <h3 class="modal-title">Unsaved Changes Exist</h3>
        </div>
        <div class="modal-body">
            <p>Unsaved changes exist for this attorney!</p>
        </div>
        <div class="modal-footer">
            <button class="btn btn-default" ng-click="close()">Discard Changes</button>
            <button class="btn btn-default" ng-click="cancel()">Continue Editing</button>
            <button class="btn btn-primary" ng-click="save()">Save Changes</button>
        </div>
    </script>

    <script type="text/ng-template" id="confirmDelete.html">
        <div class="modal-header">
            <h3 class="modal-title">Confirm Deletion</h3>
        </div>
        <div class="modal-body">
            <p>Are you sure you want to delete all of the data for this attorney?</p>
        </div>
        <div class="modal-footer">
            <button class="btn btn-default" ng-click="cancel()">Cancel</button>
            <button class="btn btn-primary" ng-click="delete()">Delete</button>
        </div>
    </script>


@stop