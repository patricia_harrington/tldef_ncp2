<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('before' => 'auth', function () {
    return View::make('login')->with('active', 'login')->with('role', '');
}));

Route::get('client-editor', array('before' => 'auth', function () {
    return View::make('clients')->with('active', 'Clients')->with('role', Auth::user()->role);
}));

Route::get('firm-editor', array('before' => 'auth', function () {
    return View::make('firms')->with('active', 'Firms')->with('role', Auth::user()->role);
}));

Route::get('attorney-editor', array('before' => 'auth', function () {
    return View::make('attorneys')->with('active', 'Attorneys')->with('role', Auth::user()->role);
}));

Route::get('referrer-editor', array('before' => 'auth', function () {
    return View::make('referrers')->with('active', 'Referrers')->with('role', Auth::user()->role);
}));

Route::get('judge-editor', array('before' => 'auth', function () {
    return View::make('judges')->with('active', 'Judges')->with('role', Auth::user()->role);
}));

Route::get('reports-page', array('before' => 'auth', function () {
    return View::make('reports')->with('active', 'Reports')->with('role', Auth::user()->role);
}));

Route::get('state-editor', array('before' => 'auth', function () {
    return View::make('states')->with('active', 'States')->with('role', Auth::user()->role);
}));


Route::get('user-editor', array('before' => 'auth', function () {
    return View::make('users')->with('active', 'Users')->with('role', Auth::user()->role);
}));


Route::get('login', function () {
    return View::make('login')->with('active', 'login')->with('role', '');
});

Route::post('login', function () {

    $username = Input::get('username');
    $password = Input::get('password');

    if (Auth::attempt(array('username' => $username, 'password' => $password), true)) {
        // we are now logged in, go back to the original page.
        if (Auth::user()->role == 'Editor') {
            return Redirect::intended('state-editor')->with('active', 'States')->with('role', Auth::user()->role);
        }
        elseif (Auth::user()->role == 'Guest') {
            return Redirect::intended('states-preview')->with('active', 'Preview')->with('role', Auth::user()->role);
        } else {
            return Redirect::intended('client-editor')->with('active', 'Clients')->with('role', Auth::user()->role);
        }
    }
    else {
        return Redirect::to('login')->with('login_errors', true)->with('active', 'login')->with('role', '');
    }
});

Route::get('logout', function () {
    Auth::logout();
    return Redirect::to('login')->with('active', 'login')->with('role', '');
});

Route::get('admin', array('before' => 'auth', function () {
        $user = User::all();
        return View::make('admin')->with('users', $user)->with('role', Auth::user()->role);
    })
);

Route::group(array('before' => 'auth'), function()
{
    Route::controller('attorneys','AttorneysController');
    Route::controller('clients', 'ClientsController');
    Route::controller('client-fields', 'ClientFieldsController');
    Route::controller('donor-levels','DonorLevelsController');
    Route::controller('firms', 'FirmsController');
    Route::controller('judges', 'JudgesController');
    Route::controller('referrers', 'ReferrersController');
    Route::controller('reports', 'ReportsController');
    Route::controller('state-info', 'StateInfoController');
    Route::controller('assignments', 'AssignmentsController');
    Route::controller('users','UsersController');
    Route::controller('zones', 'ZonesController');
});


Route::get('agreement-email/{id}', 'AgreementController@getEmail');

Route::controller('statedata','StateDatabaseController');


// State Preview operations
Route::get('states', array('before' => 'auth', function () {
    return View::make('states.index')->with('active', 'Preview')->with('role', Auth::user()->role);
}));

Route::get('states-preview', array('before' => 'auth', function () {
    return View::make('states.preview')->with('active', 'Preview')->with('role', Auth::user()->role);
}));

Route::get('states/nclegal', array('before' => 'auth', function () {
    return View::make('states.nclegal');
}));

