'use strict';

//// Declare app level module which depends on filters, and services
//var ncpApp = angular.module('ncpApp', ['ngRoute', 'ui.event', 'ui.date', 'ui.codemirror', 'ui.bootstrap', 'ngGrid', 'dangle', 'ncpApp.filters', 'ncpApp.services', 'ncpApp.directives']).
//    config(['$routeProvider', function($routeProvider) {
//        $routeProvider.when('/summary', {templateUrl:'/partials/summary.html'})
//            .when('/by_county', {templateUrl:'/partials/by-county.html'})
//            .when('/by_firm', {templateUrl:'/partials/by-firm.html'})
//            .when('/by_referrer', { templateUrl:'/partials/by-referrer.html' })
//            .when('/by_interviewer', { templateUrl:'/partials/by-interviewer.html' })
//            .when('/by_gender', { templateUrl:'/partials/by-gender.html' })
//            .when('/regional', { templateUrl:'/partials/regional.html' })
//            .when('/demographics', {templateUrl:'/partials/demographics.html'})
//            .when('/by_age', { templateUrl:'/partials/by-age.html' })
//            .when('/by_income', { templateUrl:'/partials/by-income.html' })
//            .when('/by_ethnicity', { templateUrl:'/partials/by-ethnicity.html' })
//            .when('/by_education', { templateUrl:'/partials/by-education.html' })
//            .when('/by_public_assistance', { templateUrl:'/partials/by-public-assistance.html' })
//            .when('/by_medicaid', { templateUrl:'/partials/by-medicaid.html' })
//            .when('/by_sex_assigned_at_birth', { templateUrl:'/partials/by-sex-assigned-at-birth.html' })
//            .otherwise({redirectTo: '/summary'});
//    }]);
//
//// Declare app level module which depends on filters, and services
//var allStatesApp = angular.module('allStatesApp', ['allStatesApp.services']).
//    config(['$routeProvider', function($routeProvider) {
//        $routeProvider.otherwise({redirectTo: '/states'});
//    }]);
