'use strict';

/* Directives */


angular.module('ncpApp.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }]).
  directive('bsPopover', function(){
        return function(scope, elm, attrs) {
            elm.popover();
        };
  }).
  directive('ncpStatePreview', function() {
        var previewTemplate = '<div class="state-preview well"></div> ';
        return {
            restrict: 'E',
            compile:function (tElement, tAttrs, transclude) {
                var previewElement = angular.element(previewTemplate);
                tElement.append(previewElement);
                return function (scope, element, attrs ) {
                    scope.$watch(attrs.html, function(value){
                        if (value) {
                            previewElement.html(value);
                        }
                    });
                };
            }
        };
  }).
  directive('ncpFocus', function($timeout) {
        return {
            scope: { trigger: '=ncpFocus' },
            link: function(scope, element) {
                scope.$watch('trigger', function(value) {
                    if(value === true) {
                        //$timeout(function() {
                        element[0].focus();
                        scope.trigger = false;
                        //});
                    }
                });
            }
        };
  }).
  directive('ncpMarkdownPreview', function() {
    var converter = new Showdown.converter();
    var previewTemplate = '<div class="markdown-preview well"></div> ';
    return {
        restrict: 'E',
        compile:function (tElement, tAttrs, transclude) {
            var previewElement = angular.element(previewTemplate);
            tElement.append(previewElement);
            return function (scope, element, attrs ) {
                scope.$watch(attrs.markdown, function(value){
                    if (!value) {
                        previewElement.html('');
                    } else {
                        var makeHtml = converter.makeHtml(value);
                        previewElement.html(makeHtml);
                    }
                });
            };
        }
    };
  });

//angular.module('ng').directive('ngFocus', function($timeout) {
//    return {
//        link: function ( scope, element, attrs ) {
//            scope.$watch( attrs.ngFocus, function ( val ) {
//                if ( angular.isDefined( val ) && val ) {
//                    $timeout( function () { element[0].focus(); } );
//                }
//            }, true);
//
//            element.bind('blur', function () {
//                if ( angular.isDefined( attrs.ngFocusLost ) ) {
//                    scope.$apply( attrs.ngFocusLost );
//
//                }
//            });
//        }
//    };
//});


