'use strict';

/* Services */

angular.module('ncpApp.services', ['ngResource']).
    factory('AttorneyDataSource', function($resource){
        return $resource('/attorneys/:verb/:object', {}, {
            add: {method:'POST', params:{verb:'add',object:''}, isArray:false},
            delete: {method:'POST', params:{verb:'delete',object:''}, isArray:false},
            list: {method:'GET', params:{verb:'list',object:''}, isArray:false},
            query: {method:'GET', params:{verb:'id',object:'0'}, isArray:false},
            update: {method:'POST', params:{verb:'update',object:''}, isArray:false}
        });
    }).
    factory('FirmAttorneyDataSource', function($resource){
        return $resource('/attorneys/:verb/:firm_id/:zone_id', {}, {
            forFirm: {method:'GET', params:{verb:'for-firm',firm_id:'0',zone_id:'0'}, isArray:false}
        });
    }).
    factory('ClientDataSource', function($resource){
        return $resource('/clients/:verb/:object', {}, {
            add: {method:'POST', params:{verb:'add',object:''}, isArray:false},
            delete: {method:'POST', params:{verb:'delete',object:''}, isArray:false},
            clearFirm: {method:'POST', params:{verb:'clear-firm',object:''}, isArray:false},
            list: {method:'GET', params:{verb:'list',object:''}, isArray:false},
            since: {method:'GET', params:{verb:'since',object:'0'}, isArray:false},
            query: {method:'GET', params:{verb:'id',object:'0'}, isArray:false},
            update: {method:'POST', params:{verb:'update',object:''}, isArray:false},
            intakeList:  {method:'GET', params:{verb:'intake-list',object:'0'}, isArray:false},
            forAttorney: {method:'GET', params:{verb:'for-attorney',object:'0'}, isArray:false},
            closedList:  {method:'GET', params:{verb:'closed-list',object:''}, isArray:false},
            caseList: {method:'GET', params:{verb:'case-list',object:''}, isArray:false},
            list2: {method:'GET', params:{verb:'list2',object:''}, isArray:false},
            search: {method: 'POST', params: {verb: 'search', object: ''}, isArray: false}
        });
    }).
    factory('FirmClientDataSource', function($resource){
        return $resource('/clients/:verb/:firm_id/:zone_id', {}, {
            forFirm: {method:'GET', params:{verb:'for-firm',firm_id:'0',zone_id:'0'}, isArray:false}
        });
    }).
    factory('ClientFieldsDataSource', function ($resource) {
        return $resource('/client-fields/:verb/:object', {}, {
            add: {method: 'POST', params: {verb: 'add', object: ''}, isArray: false},
            delete: {method: 'POST', params: {verb: 'delete', object: ''}, isArray: false},
            list: {method: 'GET', params: {verb: 'list', object: ''}, isArray: false},
            filters: {method: 'GET', params: {verb: 'filters', object: ''}, isArray: false},
            query: {method: 'GET', params: {verb: 'id', object: '0'}, isArray: false},
            update: {method: 'POST', params: {verb: 'update', object: ''}, isArray: false}
        });
    }).
    factory('EmailAgreement', function ($resource){
        return $resource('/agreement-email/:id', {}, {
            query: {method:'GET', params:{id:0}, isArray:false}
        });
    }).
    factory('ClientsInProgress', function ($resource){
        return $resource('/clients/in-progress/:zone/:firm', {}, {
            query: {method:'GET', params:{zone:0,firm:0}, isArray:false}
        });
    }).
    factory('ClientsWaiting', function($resource){
        return $resource('/clients/waiting/:zone/:county/:hold', {}, {
            query:  {method:'GET', params:{zone:0, county:'All', hold:0},
                isArray:false}
        });
    }).
    factory('ClosedClients', function($resource){
        return $resource('/clients/closed/:complete/:accepted/:intake/:assigned', {}, {
            query:  {method:'GET', params:{complete:'incomplete', accepted:'Yes', intake:'Yes', assigned:'Yes'},
                isArray:false}
        });
    }).
    factory('FirmDataSource', function($resource){
        return $resource('/firms/:verb/:object', {}, {
            add: {method:'POST', params:{verb:'add',object:''}, isArray:false},
            delete: {method:'POST', params:{verb:'delete',object:''}, isArray:false},
            list: {method:'GET', params:{verb:'list',object:''}, isArray:false},
            sorted_list: {method:'GET', params:{verb:'sorted-list',object:''}, isArray:false},
            query: {method:'GET', params:{verb:'id',object:'0'}, isArray:false},
            update: {method:'POST', params:{verb:'update',object:''}, isArray:false}
        });
    }).
    factory('ReferrerDataSource', function($resource){
        return $resource('/referrers/:verb/:object', {}, {
            add: {method:'POST', params:{verb:'add',object:''}, isArray:false},
            delete: {method:'POST', params:{verb:'delete',object:''}, isArray:false},
            list: {method:'GET', params:{verb:'list',object:''}, isArray:false},
            query: {method:'GET', params:{verb:'id',object:'0'}, isArray:false},
            update: {method:'POST', params:{verb:'update',object:''}, isArray:false}
        });
    }).
    factory('JudgeDataSource', function($resource){
        return $resource('/judges/:verb/:object', {}, {
            add: {method:'POST', params:{verb:'add',object:''}, isArray:false},
            delete: {method:'POST', params:{verb:'delete',object:''}, isArray:false},
            list: {method:'GET', params:{verb:'list',object:''}, isArray:false},
            query: {method:'GET', params:{verb:'id',object:'0'}, isArray:false},
            update: {method:'POST', params:{verb:'update',object:''}, isArray:false}
        });
    }).
    factory('ReportDataSourceOld', function($resource){
        return $resource('/reports/:verb/:object1/:object2', {}, {
            summary: {method:'GET', params:{verb:'summary',object1:'',object2:''}, isArray:false},
            status: {method:'GET', params:{verb:'status',object1:'',object2:''}, isArray:false}
        });
    }).
    factory('ReportDataSource', function($resource){
        return $resource('/reports/:verb/:object', {}, {
            demographics: {method:'POST', params:{verb:'demographics',object:''}, isArray:false},
            demographics_pdf: {method:'POST', params:{verb:'demographics-pdf',object:''}, isArray:false},
            ages: {method:'POST', params:{verb:'age-histogram',object:''}, isArray:false},
            incomes: {method:'POST', params:{verb:'income-histogram',object:''}, isArray:false},
            ethnicity: {method:'POST', params:{verb:'ethnicity',object:''}, isArray:false},
            education: {method:'POST', params:{verb:'education',object:''}, isArray:false},
            public_assistance: {method:'POST', params:{verb:'public-assistance',object:''}, isArray:false},
            medicaid: {method:'POST', params:{verb:'medicaid',object:''}, isArray:false},
            sex_assigned_at_birth: {method:'POST', params:{verb:'sex-assigned-at-birth',object:''}, isArray:false}
        });
    }).
    factory('ReportEmail', function($resource){
        return $resource('/reports/:verb1/:object1/:object2/:verb2/:object3/:object4', {}, {
            status: {method:'GET', params:{verb1:'status',object1:'',object2:'',verb2:'email',object3:'',object4:''}, isArray:false}
        });
    }).
    factory('StateInfo', function($resource){
        return $resource('/state-info/:verb/:object1/:object2', {}, {
            statelist: {method:'GET', params:{verb:'list',object1:'',object2:''}, isArray:false},
            countylist: {method:'GET', params:{verb:'counties',object1:'NY',object2:''}, isArray:false},
            zones: {method:'GET', params:{verb:'zones',object1:'NY',object2:''}, isArray:false},
            countiesforzone:{method:'GET', params:{verb:'counties-for-zone',object1:'53',object2:''}, isArray:false}
        });
    }).
    factory('ZoneInfo', function($resource){
        return $resource('/zones/:verb/:object', {}, {
            zoneList: {method:'GET', params:{verb:'list',object:''}, isArray:false}
        });
    }).
    factory('UserDataSource', function($resource){
        return $resource('/users/:verb/:object', {}, {
            add: {method:'POST', params:{verb:'add',object:''}, isArray:false},
            delete: {method:'POST', params:{verb:'delete',object:''}, isArray:false},
            list: {method:'GET', params:{verb:'list',object:''}, isArray:false},
            query: {method:'GET', params:{verb:'id',object:'0'}, isArray:false},
            update: {method:'POST', params:{verb:'update',object:''}, isArray:false}
        });
    }).
    factory('StateDataSource', function($resource){
        return $resource('/statedata/:verb/:object', {}, {
            query: {method:'GET', params:{verb:'id', object:'1'}, isArray:false},
            preview: {method:'GET', params:{verb:'html', object:'1'}, isArray:false},
            update: {method:'POST', params:{verb:'update',object:''}, isArray:false}
        });
    }).
    factory('DonorLevelDataSource', function($resource){
        return $resource('/donor-levels/:verb/:object', {}, {
            add: {method:'POST', params:{verb:'add',object:''}, isArray:false},
            delete: {method:'POST', params:{verb:'delete',object:''}, isArray:false},
            list: {method:'GET', params:{verb:'list',object:''}, isArray:false},
            query: {method:'GET', params:{verb:'id',object:'0'}, isArray:false},
            update: {method:'POST', params:{verb:'update',object:''}, isArray:false}
        });
    }).
    value('ui.config', {
        // The ui-jq directive namespace
        jq: {
            // The Tooltip namespace
            tooltip: {
                // Tooltip options. This object will be used as the defaults
                placement: 'right'
            }
        }
    }).
    value('ui.config', {
        codemirror: {
            theme: 'default',
            lineNumbers: true,
            lineWrapping: true
        }
    }).
    value('version', '2.0');



angular.module('allStatesApp.services', ['ngResource']).
    factory('AllState', function($resource){
        return $resource('statedata/sections/:selector', {}, {
            query: {method:'GET', params:{selector:'nc_legal'}, isArray:false}
        });
    });