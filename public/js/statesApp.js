'use strict';


// Declare app level module which depends on filters, and services
angular.module('statesApp', ['ui.event', 'statesApp.filters', 'statesApp.services', 'statesApp.directives']).
    config(['$routeProvider', function($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/states'});
    }]);

angular.module('statesApp.filters', []).
    filter('interpolate', ['version', function (version) {
        return function (text) {
            return String(text).replace(/\%VERSION\%/mg, version);
        };
    }]);

angular.module('statesApp.services', ['ngResource']).
    factory('ForState', function($resource){
        return $resource('/statedata/section/:id/:selector', {}, {
            query: {method:'GET', params:{id:'1',selector:'nc_legal'}, isArray:false}
        });
    });

angular.module('statesApp.directives', []).
    directive('vectormap',function() {
        return function(scope, elm, attrs) {
            var state_codes =  [
                "al", "ak", "az", "ar", "ca", "co", "ct", "de", "dc", "fl",
                "ga", "hi", "id", "il", "in", "ia", "ks", "ky", "la", "me",
                "md", "ma", "mi", "mn", "ms", "mo", "mt", "ne", "nv", "nh",
                "nj", "nm", "ny", "nc", "nd", "oh", "ok", "or", "pa", "pr",
                "ri", "sc", "sd", "tn", "tx", "ut", "vt", "va", "wa", "wv",
                "wi", "wy"
            ];
            scope.$watch('fullStateName',function(){
                var code = state_codes[scope.statenames.indexOf(scope.fullStateName)];
                elm.trigger('updateRegion', code);
            });
            elm.vectorMap({
                map: 'usa_en',
                selectedColor: '#a1d2d1',
                hoverColor: '#e1fbfa',
                enableZoom: false,
                showTooltip: true,
                selectedRegions: null,
                onRegionClick: function(event, code, region)
                {
                    var id = state_codes.indexOf(code);
                    scope.fullStateName = scope.statenames[id];
                    scope.$apply(function() {
                        scope.fullStateName.$setViewValue();
                    });
                    scope.changeFullState();
                }
            });
        };
    });




function StateCtrl($scope,ForState) {
    $scope.fullStateId = -1;
    $scope.birthStateName = '';
    $scope.residentStateName = '';
    $scope.changeFullState = function () {
        var state_id = $scope.statenames.indexOf($scope.fullStateName) + 1;
        var section_selector = 'all';
        $scope.fullstate = ForState.query({id:state_id, selector:section_selector});
    };
    $scope.changeBirthState = function () {
        $scope.birthstate = ForState.query({id:($scope.statenames.indexOf($scope.birthStateName) + 1), selector:'birth-certificate-only'});
    };
    $scope.changeResidentState = function () {
        $scope.residentstate = ForState.query({id:($scope.statenames.indexOf($scope.residentStateName) + 1), selector:'no-birth-certificate'});
    };
    $scope.statenames = [
        'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
        'Colorado', 'Connecticut', 'Delaware', 'District of Columbia', 'Florida',
        'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana',
        'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine',
        'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
        'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
        'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
        'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Puerto Rico',
        'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas',
        'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia',
        'Wisconsin', 'Wyoming'];
}
StateCtrl.$inject = ['$scope', ForState];
