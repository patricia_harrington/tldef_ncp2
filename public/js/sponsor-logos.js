/**
 * Filename: sponsor-logos.js
 * Author: Patricia Harrington
 * Created: 6/13/13 10:45 PM
 * Copyright 2013 Transgender Legal Defense & Education Fund, Inc.
 */
'use strict';

angular.module('sponsorsLogosApp', ['sponsorsLogosApp.services', 'sponsorsLogosApp.directives']);

angular.module('sponsorsLogosApp.services', ['ngResource']).
    factory('Sponsors', function($resource){
        return $resource('http://admin.transgenderlegal.dev/sponsors/sorted', {}, {
            query: {method:'JSONP', params:{callback: 'JSON_CALLBACK'}, isArray:false}
        });
    }).factory('Settings', function($resource){
        return $resource('http://admin.transgenderlegal.dev/sponsors/settings', {}, {
            update: {method:'POST', params:{}, isArray:false}
        });
    });

angular.module('sponsorsLogosApp.directives', []).
    directive('sponsorsLogos', function() {
        return {
            restrict: 'E',
            scope: true,
            template: '<div class="sponsor-title">{{settings.title}}</div><div class="sponsor-logos"><img ng-class="transition" ng-src="{{sponsors[imgIndex].logo_image}}"></div>',
        };
    }).
    directive('sponsorsOptions', function() {
        return {
            restrict:'E',
            replace: true,
            scope: true,
            template: [
                '<div id="sidebar-options">',
                '<h2>Settings</h2>',
                '<div ng-form name="settingsForm">',
                '<label class="checkbox"><input type="checkbox" name="showLogos" ng-true-value="yes" ng-false-value="no" ng-model="settings.showLogo"/>Show Logos</label>',
                '<label>Title: <input type="text" name="title" ng-model="settings.title"/></label>',
                '<label>Transition: <select ng-model="settings.transition" ng-change="setTransition"><option value="fade">Fade</option><option value="slide">Slide</option></select></label>',
                '<button ng-disabled="settingsForm.$pristine" ng-click="saveSettings()">Update Settings</button>',
                '</div>',
                '<h2>Preview</h2>',
                '<div id="sponsors"><sponsors-logos ng-show="true"></sponsors-logos></div>',
                '<button ng-click="loadSponsors()">Refresh Preview</button>',
                '</div>'
        ].join('')
        };
    });

/* Controller */

function sponsorsLogosCtrl($scope, $timeout, Sponsors, Settings ) {
    $scope.setTransition = function(transition) {
        if (transition === 'slide') {
            $scope.transitionEnter = 'slidein';
            $scope.transitionExit = 'slideout';
        } else {
            // fade is the default
            $scope.transitionEnter = 'fadein';
            $scope.transitionExit = 'fadeout';
        }
    };
    $scope.cancelTimeouts = function() {
        if ($scope.timeoutAdvance) {
            $timeout.cancel($scope.timeoutAdvance);
            if ( $scope.timeoutExit ) {
                $timeout.cancel($scope.timeoutExit);
                if ( $scope.timeoutEnter ) {
                    $timeout.cancel($scope.timeoutEnter);
                }
            }
        }
    };
    $scope.startSlides = function(index) {
        $scope.imgIndex = index;
        $scope.timeoutAdvance = $timeout(function advanceSlide() {
            $scope.transition = $scope.transitionExit;
            $scope.timeoutExit = $timeout(function fadeSlide() {
                $scope.imgIndex = ($scope.imgIndex + 1) % $scope.sponsors.length;
                var delay = $scope.sponsors[$scope.imgIndex].display_duration * 1000;
                $scope.timeoutEnter = $timeout(function fadeSlide() {
                    $scope.transition = $scope.transitionEnter;
                    $scope.timeoutAdvance =  $timeout(advanceSlide, delay);
                }, 400);
            }, 400 );
        }, $scope.sponsors[$scope.imgIndex].display_duration * 1000 );
    };
    $scope.loadSponsors = function() {
        $scope.data = Sponsors.query({}, function () {
            $scope.cancelTimeouts();
            $scope.settings = $scope.data.settings;
            $scope.setTransition($scope.settings.transition);
            $scope.sponsors = $scope.data.sponsors;
            $scope.startSlides(0);
        });
    };
    $scope.saveSettings = function() {
        Settings.update($scope.settings);
    };
    $scope.timeout = null;
    $scope.loadSponsors();
}
sponsorsLogosCtrl.$inject = ['$scope', '$timeout', Sponsors, Settings];

