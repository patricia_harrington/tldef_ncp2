'use strict';

// Declare app level module which depends on filters, and services
var ncpApp = angular.module('ncpApp', ['ngRoute', 'ui.event', 'ui.date', 'ui.codemirror', 'ui.bootstrap', 'ngGrid', 'dangle', 'ncpApp.filters', 'ncpApp.services', 'ncpApp.directives']).
    config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/summary', {templateUrl:'/partials/summary.html'})
            .when('/by_county', {templateUrl:'/partials/by-county.html'})
            .when('/by_firm', {templateUrl:'/partials/by-firm.html'})
            .when('/by_referrer', { templateUrl:'/partials/by-referrer.html' })
            .when('/by_interviewer', { templateUrl:'/partials/by-interviewer.html' })
            .when('/by_gender', { templateUrl:'/partials/by-gender.html' })
            .when('/regional', { templateUrl:'/partials/regional.html' })
            .when('/demographics', {templateUrl:'/partials/demographics.html'})
            .when('/by_age', { templateUrl:'/partials/by-age.html' })
            .when('/by_income', { templateUrl:'/partials/by-income.html' })
            .when('/by_ethnicity', { templateUrl:'/partials/by-ethnicity.html' })
            .when('/by_education', { templateUrl:'/partials/by-education.html' })
            .when('/by_public_assistance', { templateUrl:'/partials/by-public-assistance.html' })
            .when('/by_medicaid', { templateUrl:'/partials/by-medicaid.html' })
            .when('/by_sex_assigned_at_birth', { templateUrl:'/partials/by-sex-assigned-at-birth.html' })
            .otherwise({redirectTo: '/summary'});
    }]);

// Declare app level module which depends on filters, and services
var allStatesApp = angular.module('allStatesApp', ['allStatesApp.services']).
    config(['$routeProvider', function($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/states'});
    }]);


/* Controllers */

ncpApp.controller('PageCtrl', ['$scope', function($scope)  {
    $scope.showHeader = true;
    $scope.$on('ShowHeader', function() {
        $scope.showHeader = true;
    });
    $scope.$on('HideHeader', function() {
        $scope.showHeader = false;
    });
}]);

ncpApp.controller('StateEditorCtrl', ['$scope', 'StateDataSource', function($scope, StateDataSource) {
    $scope.statenames = [
        'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
        'Colorado', 'Connecticut', 'Delaware', 'District of Columbia', 'Florida',
        'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana',
        'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine',
        'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
        'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
        'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
        'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Puerto Rico',
        'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas',
        'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia',
        'Wisconsin', 'Wyoming'
    ];
    $scope.sections = [
        {'id':'nc_prelude', 'text':'Name Change Prelude'},
        {'id':'nc_legal', 'text':'Legal Name Change'},
        {'id':'nc_birth_certificate', 'text':'Name Change On Birth Certificate'},
        {'id':'nc_drivers_license', 'text':'Name Change On Drivers License'},
        {'id':'nc_postscript', 'text':'Name Change Postscript'},
        {'id':'gc_prelude', 'text':'Sex Designation Prelude'},
        {'id':'gc_legal', 'text':'Legal Sex Designation Change'},
        {'id':'gc_birth_certificate', 'text':'Sex Designation On Birth Certificate'},
        {'id':'gc_drivers_license', 'text':'Sex Designation On Drivers License'},
        {'id':'gc_postscript', 'text':'Sex Designation Postscript'}
    ];
    $scope.editorOptions = {
        lineWrapping : true,
        lineNumbers: true,
        mode: 'markdown'
    };
    $scope.editSectionText = "";
    $scope.editSection = "";
    $scope.lastEditSection = "";
    $scope.noState = true;
    $scope.noInput = true;
    $scope.flipClass = '';
    $scope.showPublished = function() {
        if ($scope.flipClass === 'flipped' ) {
            $scope.flipClass = '';
        }
    };
    $scope.showUnpublished = function() {
        if ($scope.flipClass === '') {
            $scope.flipClass = 'flipped';
        }
    };
    $scope.setInput = function () {
        $scope.noInput = false;
        $scope.showUnpublished();
    };
    $scope.clearInput = function () {
        $scope.noInput = true;
    };
    $scope.getState = function () {
        $scope.get_data = StateDataSource.query({object:$scope.statenames.indexOf($scope.fullStateName) + 1}, $scope.readData);
    };
    $scope.putState = function () {
        $scope.put_data = StateDataSource.update(angular.toJson($scope.writeData()), function () {
            $scope.timestamp = $scope.put_data.timestamp.date;
            $scope.previewState();
        });
    };
    $scope.previewState = function () {
        $scope.fullstate = StateDataSource.preview({object:($scope.statenames.indexOf($scope.fullStateName) + 1)}, function () {
            $scope.showPublished();
            $scope.noState = false;
        });
    };
    $scope.writeData = function () {
        $scope.saveEditSection();
        $scope.noInput = true;
        return $scope.get_data;
    };
    $scope.readData = function () {
        $scope.timestamp = $scope.get_data.updated_at;
        $scope.noInput = true;
        $scope.setEditSection();
        $scope.previewState();
    };
    $scope.changeEditSection = function () {
        $scope.saveEditSection();
        $scope.lastEditSection = $scope.editSection;
        $scope.setEditSection();
    };
    $scope.saveEditSection = function () {
        $scope.get_data[$scope.lastEditSection] = $scope.editSectionText;
    };
    $scope.setEditSection = function () {
        $scope.editSectionText = $scope.get_data[$scope.editSection];
    };
}]);


ncpApp.controller('AllStateCtrl',['$scope','AllState', function($scope, AllState) {
    $scope.allstates = AllState.query({selector:'nc_legal'});
}]);


ncpApp.controller('ClientsCtrl', ['$scope', '$modal',
                     'ClientDataSource', 'ClientFieldsDataSource', 'ClientsWaiting',
                     'ClientsInProgress', 'ClosedClients', 'EmailAgreement',
                     'FirmDataSource', 'AttorneyDataSource',
                     'FirmAttorneyDataSource', 'FirmClientDataSource',
                     'JudgeDataSource', 'ReferrerDataSource', 'StateInfo', 'ZoneInfo', function($scope, $modal,
                                                                                        ClientDataSource, ClientFieldsDataSource, ClientsWaiting,
                                                                                        ClientsInProgress, ClosedClients, EmailAgreement,
                                                                                        FirmDataSource, AttorneyDataSource,
                                                                                        FirmAttorneyDataSource, FirmClientDataSource,
                                                                                        JudgeDataSource, ReferrerDataSource, StateInfo, ZoneInfo) {
    var isDirty;
    $scope.loading = false;
    $scope.showOverlayOptions = false;
    $scope.dateOptions = {
        changeYear: true,
        changeMonth: true,
        yearRange: '2000:+1'
    };
    $scope.setOffsets = function(showNotes) {
        if (showNotes) {
            $scope.banner = $scope.bannerVisible + ' large-banner';
            $scope.formContainer = 'offset-face-container';
        } else {
            $scope.banner = $scope.bannerVisible + ' small-banner';
            $scope.formContainer = 'face-container';
        }
    };
    $scope.toggleNotes = function() {
        if ($scope.showNotes) {
            $scope.notesIcon = 'glyphicon glyphicon-circle-arrow-right';
            $scope.showNotes = false;
        } else {
            $scope.notesIcon = 'glyphicon glyphicon-circle-arrow-down';
            $scope.showNotes = true;
        }
        $scope.setOffsets($scope.showNotes);
    };
    $scope.clientForms = ['Intake Information','Case Details'];
    $scope.showForm = function() {
        $scope.isFlipped = ($scope.clientForm === 'Case Details');
        $scope.flipClass = ($scope.isFlipped) ? 'flipped' : '';
        angular.element('body,html').animate({
            scrollTop: 0
        }, 800);
    };
    $scope.flipForms = function() {
        $scope.isFlipped = !$scope.isFlipped;
        $scope.flipClass = ($scope.isFlipped) ? 'flipped' : '';
        angular.element('body,html').animate({
            scrollTop: 0
        }, 800);
    };
    $scope.confirmDelete = function(){
        var modalInstance = $modal.open({
            templateUrl: 'confirmDelete.html',
            controller: 'ConfirmDeleteCtrl'
        });

        modalInstance.result.then(function (result) {
            if (result === 'delete') {
                $scope.deleteClient();
            }
        });
    };
    $scope.confirmClose = function(){
        var modalInstance = $modal.open({
            templateUrl: 'confirmClose.html',
            controller: 'ConfirmCloseCtrl'
        });

        modalInstance.result.then(function (result) {
            if (result === 'close') {
                $scope.clearClient();
            } else if (result === 'save') {
                $scope.saveClient(true);
            }
        });
    };
    $scope.showPrompts = false;
    $scope.bannerVisible = "banner-hide";
    $scope.flipClass = '';
    $scope.isFlipped = false;
    $scope.formContainer = 'face-container';
    $scope.showNotes = false;
    $scope.notesIcon = 'glyphicon glyphicon-circle-arrow-right';
    $scope.setOffsets(false);
    $scope.state_codes =  [
        "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL",
        "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME",
        "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH",
        "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "PR",
        "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV",
        "WI", "WY"
    ];
    $scope.getZones = function() {
        $scope.zoneList = ZoneInfo.zoneList({}, function () {
            $scope.zones = $scope.zoneList.zones;
        });
    };
//    $scope.zones = [
//        {name:"All",id:"0"},
//        {name:"Albany",id:"57"},
//        {name:"Buffalo",id:"55"},
//        {name:"Long Island",id:"56"},
//        {name:"New York City",id:"53"},
//        {name:"Rochester",id:"54"},
//        {name:"New York",id:"33"},
//        {name:"New Jersey",id:"31"},
//        {name:"Pennsylvania",id:"39"},
//        {name:"Wisconsin",id:"51"}
//    ];
    $scope.getCounties = function(state_code) {
        $scope.countylist = StateInfo.countylist({object1:state_code}, function () {
            $scope.counties = $scope.countylist.counties;
        });
    };
    $scope.getCounties('NY');
    $scope.getZones();
    $scope.firms = {
        data:null,
        list:null,
        getName:function (id) {
            return ((this.id_to_name) ? this.id_to_name[id] : null);
        },
        getId:function (name) {
            return ((this.name_to_id) ? this.name_to_id[name] : null);
        },
        loadRecord:function (data) {
            this.data = data;
            if (this.data) {
                this.list = _.sortBy(this.data.firms, function(item){
                    return item.name.toLowerCase();
                });
            } else {
                this.list = null;
            }
            if (this.list) {
                this.id_to_name = {};
                this.name_to_id = {};
                var n = this.list.length;
                for (var i = 0; i < n; i++) {
                    var id = this.list[i].id;
                    var name = this.list[i].name;
                    this.id_to_name[id] = name;
                    this.name_to_id[name] = id;
                }
                this.names = this.list.map(function (item) {
                    return item.name;
                });
            }
        }
    };
    $scope.firm_name = null;
    $scope.attorneys = {
        data:null,
        list:null,
        getName:function (id) {
            return ((this.id_to_name) ? this.id_to_name[id] : null);
        },
        getId:function (name) {
            return ((this.name_to_id) ? this.name_to_id[name] : null);
        },
        loadRecord:function (data) {
            this.data = data;
            if (this.data) {
                this.list = _.sortBy(this.data.attorneys, function(item){
                    return item.name.toLowerCase();
                });
            } else {
                this.list = null;
            }
            if (this.list) {
                this.id_to_name = {};
                this.name_to_id = {};
                var n = this.list.length;
                for (var i = 0; i < n; i++) {
                    var id = this.list[i].id;
                    var name = this.list[i].name;
                    this.id_to_name[id] = name;
                    this.name_to_id[name] = id;
                }
                this.names = this.list.map(function (item) {
                    return item.name;
                });
            }
        }
    };
    $scope.getZoneName = function(zone_id) {
        var result = null;
        var n = $scope.zones.length;
        for ( var i = 0; i < n; i++ ) {
            if ($scope.zones[i].id === zone_id) {
                result = $scope.zones[i].name;
            }
        }
        return result;
    };
    $scope.getZoneId = function(zone_name) {
        var result = null;
        var n = $scope.zones.length;
        for ( var i = 0; i < n; i++ ) {
            if (zone_name.localeCompare($scope.zones[i].name) === 0) {
                result = $scope.zones[i].id;
            }
        }
        return result;
    };
    $scope.attorneyFilter = '';
    $scope.filterAttorney = function() {
        var attorney_id = $scope.attorneys.getId($scope.attorneyFilter);
        $scope.loading = true;
        $scope.directory = ClientDataSource.forAttorney({object:attorney_id}, function () {
            $scope.client.loadDirectory($scope.directory);
            $scope.loading = false;
        });
    };
    $scope.new_attorney = {
        attorney: null,
        zone_name: '',
        errors: {
            phone:"Enter phone numbers as digits separated by dashes (e.g. 999-999-9999x9999). Extensions are optional and are preceded by an x followed by 1 to 4 digits.",
            cell: "Enter cell numbers as digits separated by dashes (e.g. 999-999-9999).",
            fax: "Enter fax numbers as digits separated by dashes (e.g. 999-999-9999).",
            email:"Email address must be of the form: someone@someplace.xxx where xxx is com, org, or some other two or three character domain name.",
            url: "URL must begin with http://"
        },
        openRecord: function(firm_id) {
            this.attorney = {};
            this.attorney.first_name = '';
            this.attorney.middle_name = '';
            this.attorney.last_name = '';
            this.attorney.nick_name = '';
            this.attorney.title = '';
            this.attorney.is_firm_contact = 'No';
            this.attorney.phone = '';
            this.attorney.cell = '';
            this.attorney.fax = '';
            this.attorney.email = '';
            this.attorney.url = '';
            this.attorney.firm_id = firm_id;
            this.attorney.zone_id = null;
            this.attorney.notes_general = '';
        },
        clearRecord: function() {
            this.attorney = null;
            this.zone_name = '';
        },
        putRecord: function() {
            return this.attorney ? angular.toJson(this.attorney) : null;
        }
    };
    $scope.changeZone = function() {
        if ($scope.new_attorney) {
            $scope.new_attorney.attorney.zone_id = $scope.getZoneId($scope.new_attorney.zone_name);
        }
    };
    $scope.judges = {
        data:null,
        list:null,
        getName:function (id) {
            return ((this.id_to_name) ? this.id_to_name[id] : null);
        },
        getId:function (name) {
            return ((this.name_to_id) ? this.name_to_id[name] : null);
        },
        loadRecord:function (data) {
            this.data = data;
            if (this.data) {
                this.list = _.sortBy(this.data.judges, function(item){
                    return item.name.toLowerCase();
                });
            } else {
                this.list = null;
            }
            if (this.list) {
                this.id_to_name = {};
                this.name_to_id = {};
                var n = this.list.length;
                for (var i = 0; i < n; i++) {
                    var id = this.list[i].id;
                    var name = this.list[i].name;
                    this.id_to_name[id] = name;
                    this.name_to_id[name] = id;
                }
                this.names = this.list.map(function (item) {
                    return item.name;
                });
            }
        }
    };
    $scope.referrers = {
        data:null,
        list:null,
        getName:function (id) {
            return ((this.id_to_name) ? this.id_to_name[id] : null);
        },
        getId:function (name) {
            return ((this.name_to_id) ? this.name_to_id[name] : null);
        },
        loadRecord:function (data) {
            this.data = data;
            if (this.data) {
                this.list = _.sortBy(this.data.referrers, function(item){
                    return item.name.toLowerCase();
                });
            } else {
                this.list = null;
            }
            if (this.list) {
                this.id_to_name = {};
                this.name_to_id = {};
                var n = this.list.length;
                for (var i = 0; i < n; i++) {
                    var id = this.list[i].id;
                    var name = this.list[i].name;
                    this.id_to_name[id] = name;
                    this.name_to_id[name] = id;
                }
                this.names = this.list.map(function (item) {
                    return item.name;
                });
            }
        }
    };
    var IdManager = {
        elemList: [],
        currentIds: [],
        initialIds: [],
        add: function(elem) {
            var self = this;
            if (! _.contains(self.currentIds, elem.id)) {
                self.currentIds.push(elem.id);
                self.elemList.push(elem);
            }
        },
        get: function(elems) {
            this.reset();
            if (elems) {
                for (var i = 0; i < elems.length; i++) {
                    this.elemList.push(elems[i]);
                    this.currentIds.push(elems[i].id);
                    this.initialIds.push(elems[i].id);
                }
            }
        },
        hasId: function(id) {
            return _.contains(this.currentIds, id);
        },
        put: function(client_id) {
            var unchanged = _.intersection(this.currentIds,this.initialIds);
            var addIds = _.difference(this.currentIds,unchanged);
            var removeIds = _.difference(this.initialIds,unchanged);
            // {"client_id":"1096","add":["664"],"remove":["663","666"]}
            var record = {};
            record.client_id = client_id;
            record.add = addIds;
            record.remove = removeIds;
            return record;
        },
        remove: function(elem) {
            this.elemList = _.reject(this.elemList, function(item){ return item.id === elem.id; });
            this.currentIds = _.reject(this.currentIds, function(item){ return item === elem.id; });
        },
        reset: function() {
            this.elemList = [];
            this.currentIds = [];
            this.initialIds = [];
        }
    };
    $scope.referrer_name = null;
    $scope.client = {
        firms: {
            elemList: [],
            currentIds: [],
            initialIds: [],
            add: function(elem) {
                var self = this;
                if (! _.contains(self.currentIds, elem.id)) {
                    self.currentIds.push(elem.id);
                    self.elemList.push(elem);
                }
            },
            get: function(elems) {
                this.reset();
                if (elems) {
                    for (var i = 0; i < elems.length; i++) {
                        this.elemList.push(elems[i]);
                        this.currentIds.push(elems[i].id);
                        this.initialIds.push(elems[i].id);
                    }
                }
            },
            hasId: function(id) {
                return _.contains(this.currentIds, id);
            },
            put: function(client_id) {
                var unchanged = _.intersection(this.currentIds,this.initialIds);
                var addIds = _.difference(this.currentIds,unchanged);
                var removeIds = _.difference(this.initialIds,unchanged);
                // {"client_id":"1096","add":["664"],"remove":["663","666"]}
                var record = {};
                record.client_id = client_id;
                record.add = addIds;
                record.remove = removeIds;
                return record;
            },
            remove: function(elem) {
                this.elemList = _.reject(this.elemList, function(item){ return item.id === elem.id; });
                this.currentIds = _.reject(this.currentIds, function(item){ return item === elem.id; });
            },
            reset: function() {
                this.elemList = [];
                this.currentIds = [];
                this.initialIds = [];
            }
        },
        attorneys: {
            dataList: [],
            ids: [],
            initialIds: [],
            add: function(attorney) {
                var self = this;
                if (! _.contains(self.ids, attorney.id)) {
                    self.ids.push(attorney.id);
                    self.dataList.push(attorney);
                }
            },
            get: function(data) {
                this.reset();
                if (data) {
                    for (var i = 0; i < data.length; i++) {
                        this.dataList.push(data[i]);
                        this.ids.push(data[i].id);
                        this.initialIds.push(data[i].id);
                    }
                }
            },
            hasId: function(id) {
                return _.contains(this.ids, id);
            },
            put: function(client_id) {
                var unchanged = _.intersection(this.ids,this.initialIds);
                var addIds = _.difference(this.ids,unchanged);
                var removeIds = _.difference(this.initialIds,unchanged);
                // {"client_id":"1096","add":["664"],"remove":["663","666"]}
                var record = {};
                record.client_id = client_id;
                record.add = addIds;
                record.remove = removeIds;
                return record;
            },
            remove: function(attorney) {
                this.dataList = _.reject(this.dataList, function(item){ return item.id === attorney.id; });
                this.ids = _.reject(this.ids, function(item){ return item === attorney.id; });
            },
            removeFirm: function(firm_id) {
                this.dataList = _.reject(this.dataList, function(item){ return item.firm_id === firm_id; });
                this.ids = _.pluck(this.dataList, 'id');
            },
            reset: function() {
                this.dataList = [];
                this.ids = [];
                this.initialIds = [];
            }
        },
        current:null,
        columnDefs:{
            'all_clients':[
                {field:'id', displayName:'ID', width:'50px'},
                {field:'preferred_name', displayName:'Preferred Name'},
                {field:'legal_name', displayName:'Legal Name'},
                {field:'phone1', displayName:'Phone 1',width:'150px'},
                {field:'phone2', displayName:'Phone 2',width:'150px'},
                {field:'is_closed', displayName:'Closed',width:'60px'},
                {field:'updated_at.date', displayName:'Last Update',width:'160px'}
            ],
            'all_clients_alt':[
                {field:'id', displayName:'ID', width:'50px'},
                {field:'preferred_name', displayName:'Preferred Name'},
                {field:'has_medicaid', displayName:'Medicaid',width:'80px'},
                {field:'county', displayName:'County'},
                {field:'state', displayName:'State',width:'60px'},
                {field:'place_of_birth', displayName:'Place of Birth'},
                {field:'referrer', displayName:'Referrer'},
                {field:'updated_at.date', displayName:'Last Update',width:'160px'}
            ],
            'case_list':[
                {field:'id', displayName:'ID', width:'50px'},
                {field:'preferred_name', displayName:'Preferred Name'},
                {field:'date_of_filing', displayName:'Filing Date', width:'100px'},
                {field:'court_index_number', displayName:'Index Number'},
                {field:'judge', displayName:'Judge'},
                {field:'date_of_hearing', displayName:'Hearing Date', width:'100px'},
                {field:'notes_hearing', displayName:'Hearing Notes'},
                {field:'updated_at.date', displayName:'Last Update',width:'160px'}
            ],
            'intake_list':[
                {field:'id', displayName:'ID', width:'50px'},
                {field:'preferred_name', displayName:'Preferred Name'},
                {field:'legal_name', displayName:'Legal Name'},
                {field:'phone1', displayName:'Phone 1',width:'150px'},
                {field:'phone2', displayName:'Phone 2',width:'150px'},
                {field:'date_intake_scheduled', displayName:'Intake Scheduled'},
                {field:'updated_at.date', displayName:'Last Update',width:'160px'}
            ],
            'wait_list':[
                {field:'index', displayName: "#", width:'40px'},
                {field:'id', displayName:'ID', width:'50px'},
                {field:'preferred_name', displayName:'Preferred Name'},
                {field:'legal_name', displayName:'Legal Name'},
                {field:'phone1', displayName:'Phone 1',width:'130px'},
                {field:'phone2', displayName:'Phone 2',width:'130px'},
                {field:'date_intake_completed', displayName:'Intake Date',width:'100px'},
                //{field:'county', displayName:'County',
                //    cellTemplate: '<div ng-class="{hilite: notNYC(row.getProperty(col.field)) }"><div class="ngCellText">{{row.getProperty(col.field)}}</div></div>'},
                //{field:'state', displayName:'State',width:'60px'},
                {field:'has_birth_certificate', displayName:'BC',width:'60px'},
                {field:'payment_ability', displayName:'Fee',width:'60px'},
                {field:'updated_at.date', displayName:'Last Update',width:'160px'}
            ],
            'in_progress':[
                {field:'id', displayName:'ID', width:'50px'},
                {field:'preferred_name', displayName:'Preferred Name'},
                {field:'legal_name', displayName:'Legal Name'},
                {field:'phone1', displayName:'Phone 1',width:'100px'},
                {field:'phone2', displayName:'Phone 2',width:'100px'},
                {field:'date_of_assignment', displayName:'Assigned',width:'100px'},
                {field:'date_of_filing', displayName:'Filed',width:'100px'},
                {field:'date_of_hearing', displayName:'Hearing',width:'100px'},
                {field:'updated_at', displayName:'Last Update',width:'160px'}
            ],
            'for_firm':[
                {field:'id', displayName:'ID', width:'50px'},
                {field:'preferred_name', displayName:'Preferred Name'},
                {field:'legal_name', displayName:'Legal Name'},
                {field:'date_intake_completed', displayName:'Intake Date',width:'100px'},
                {field:'date_of_assignment', displayName:'Assignment Date',width:'130px'},
                {field:'name_change_completed', displayName:'Completed',width:'80px'},
                {field:'is_closed', displayName:'Closed',width:'60px'},
                {field:'updated_at.date', displayName:'Last Update',width:'160px'}
            ],
            'closed_list':[
                {field:'id', displayName:'ID', width:'50px'},
                {field:'preferred_name', displayName:'Preferred Name'},
                {field:'date_intake_completed', displayName:'Intake Date',width:'100px'},
                {field:'date_of_assignment', displayName:'Assigned On',width:'100px'},
                {field:'is_accepted', displayName:'Accepted',width:'80px'},
                {field:'name_change_completed', displayName:'Completed',width:'80px'},
                {field:'reasons_for_rejection', displayName:'Reason For Rejection'},
                {field:'updated_at.date', displayName:'Last Update',width:'160px'}

            ],
            'search':[
                {field:'id', displayName:'ID', width:'50px'},
                {field:'preferred_name', displayName:'Preferred Name'},
                {field:'legal_name', displayName:'Legal Name'},
                {field:'phone1', displayName:'Phone 1',width:'150px'},
                {field:'phone2', displayName:'Phone 2',width:'150px'},
                {field:'is_closed', displayName:'Closed',width:'60px'},
                {field:'updated_at', displayName:'Last Update',width:'160px'}
            ]
        },
        directory:null,
        errors: {
            annual_income:"Enter the annual income in the format 999,999 (digits and commas only) or 0 for no income.",
            date:"Enter dates in the format mm/dd/yyyy.",
            email:"The email address is not complete.",
            phone:"Enter phone numbers as digits separated by dashes (e.g. 999-999-9999x9999). Extensions are optional and are preceded by an x followed by 1 to 4 digits."
        },
        fields: null,
        prompts: null,
        options: null,
        checkLists: null,
        list:null,
        record:null,
        clearDirectory:function () {
            this.directory = null;
            this.list = null;
        },
        loadDirectory:function (directory) {
            this.directory = directory;
            if (this.directory) {
                this.list = this.directory.clients;
            } else {
                this.list = null;
            }
        },
        sizeOfDirectory:function() {
            return (this.list ? this.list.length : 0);
        },
        loadFields: function(fields) {
            var self = this;
            self.fields = fields;
            var prompts = {};
            var options = {};
            var checkLists = {};
            _.each(self.fields, function(field, index, list) {
                var field_name = field.name;
                var prompt = field.prompt;
                prompts[field_name] = {text:prompt,show:false};
                switch (field.type) {
                    case 'checkbox':
                        var checkList = [];
                        _.each(field.options, function(option, index, list){
                            checkList.push({name:option,value:false});
                        });
                        checkLists[field_name] = checkList;
                        break;
                    case 'select':
                        options[field_name] = field.options;
                        break;
                    default:
                        break;
                };
            });
            self.prompts = prompts;
            self.options = options;
            self.checkLists = checkLists;
        },
        setPrompts: function(show) {
            var self = this;
            _.each(self.prompts, function (prompt, index, list) {
                prompt.show = show;
            });
        },
        clearCheckLists: function() {
            var self = this;
            _.each(self.checkLists, function(checklist, index, list) {
                _.each(checklist, function(obj, index, list) { obj.value = false;});
            });
        },
        putCheckList: function(field_name){
            var stringList = '';
            var first = true;
            var options = this.checkLists[field_name];
            _.each(options, function(option, index, list) {
                if (option.value) {
                    if (!first) {
                        stringList = stringList.concat(',',option.name);
                    } else {
                        stringList = stringList.concat(option.name);
                        first = false;
                    }
                }
            });
            return stringList;
        },
        getCheckList: function(field_name, stringList) {
            if (stringList) {
                var item;
                var names = stringList.split(',');
                for (var i = 0; i < names.length; i++) {
                    item = _.findWhere(this.checkLists[field_name], {name: names[i].trim()});
                    if (item !== undefined) {
                        item.value = true;
                    }
                }
            }
        },
        getClientField:function (field_name, field_type) {
            switch(field_type) {
                case 'checkbox':
                    this.getCheckList(field_name,this.record.client[field_name]);
                    break;
                case 'date':
                    switch(field_name) {
                        case 'date_of_assignment':
                            this.current[field_name] = this.getDateFromDatabaseString(this.record.client[field_name]);
                            break;
                        default:
                            this.current[field_name] =  this.getDisplayDateString(this.record.client[field_name]);
                            break;
                    }
                    break;
                default:
                    this.current[field_name] = this.record.client[field_name];
                    break;
            }
        },
        putClientField:function (field_name, field_type) {
            switch(field_type) {
                case 'checkbox':
                    this.record.client[field_name] = this.putCheckList(field_name);
                    break;
                case 'date':
                    switch(field_name) {
                        case 'date_of_assignment':
                            this.record.client[field_name] = this.getDatabaseStringFromDate(this.current[field_name]);
                            break;
                        default:
                            this.record.client[field_name] =  this.getDatabaseDateString(this.current[field_name]);
                            break;
                    }
                    break;
                default:
                    this.record.client[field_name] = this.current[field_name];
                    break;
            }
        },
        getCaseField:function (field_name, field_type) {
            switch(field_type) {
                case 'checkbox':
                    if (this.record.case) {
                        this.getCheckList(field_name,this.record.case[field_name]);
                    }
                    break;
                case 'date':
                    this.current[field_name] = (this.record.case) ? this.getDateFromDatabaseString(this.record.case[field_name]) : null;
                    break;
                default:
                    this.current[field_name] = (this.record.case) ? this.record.case[field_name] : null;
                    break;
            }
        },
        bufferCaseField:function (buffer, field_name, field_type) {
            switch(field_type) {
                case 'checkbox':
                    buffer[field_name] = this.putCheckList(field_name);
                    break;
                case 'date':
                    buffer[field_name] = this.getDatabaseStringFromDate(this.current[field_name]);
                    break;
                default:
                    buffer[field_name] =this.current[field_name];
                    break;
            }
        },
        getFields:function() {
            var self = this;
            _.each(self.fields, function(field, index, list) {
                var name = field.name;
                var type = field.type;
                switch (field.table) {
                     case 'clients':
                         self.getClientField(name, type);
                         break;
                     case 'client_cases':
                         self.getCaseField(name, type);
                         break;
                     case 'client_details':
                         self.getDetail(name);
                         break;
                     default:
                         break;
                 }
            });
            this.getFirms();
            this.getAttorneys();
        },
        putFields:function() {
            var self = this;
            var buffer = {};
            _.each(self.fields, function(field, index, list) {
                var name = field.name;
                var type = field.type;
                switch (field.table) {
                    case 'clients':
                        self.putClientField(name, type);
                        break;
                    case 'client_cases':
                        self.bufferCaseField(buffer, name, type);
                        break;
                    case 'client_details':
                        self.putDetail(name);
                        break;
                    default:
                        break;
                }
            });
            this.writeCaseRecord(buffer);
            this.putFirms();
            this.putAttorneys();
            return this.record;
        },
        getFirms: function() {
            this.firms.get(this.record.firms);
        },
        putFirms: function() {
            this.record.firms = this.firms.put(this.current.id);
        },
        getAttorneys: function() {
            this.attorneys.get(this.record.attorneys);
        },
        putAttorneys: function() {
            this.record.attorneys = this.attorneys.put(this.current.id);
        },
        loadRecord:function (record) {
            this.record = record;
            this.current = null;
            this.clearCheckLists();
            if (this.record) {
                this.current = {};
                this.getFields();
                this.setMarriage();
                this.computeAge();
                this.setCitizenship();
            }
        },
        computeAge:function () {
            if (this.current.date_of_birth) {
                var now = new Date();
                var date_of_birth = this.getDateFromDatabaseString(this.getDatabaseDateString(this.current.date_of_birth));
                var age = now.getFullYear() - date_of_birth.getFullYear();
                var month_delta = now.getMonth() - date_of_birth.getMonth();
                if (month_delta < 0 || (month_delta === 0 && now.getDate() < date_of_birth.getDate())) {
                    age--;
                }
                this.current.age = age;
            }
        },
        writeCaseRecord:function (buffer) {
            if (buffer && this.record) {
                var date_of_filing = buffer.date_of_filing;
                var court_index_number = (buffer.court_index_number) ? buffer.court_index_number.trim(): '';
                var date_of_hearing = buffer.date_of_hearing;
                var judge = (buffer.judge) ? buffer.judge.trim() : '';
                var notes_hearing = (buffer.notes_hearing) ? buffer.notes_hearing.trim() : '';
                var char_count = court_index_number.length + judge.length + notes_hearing.length;
                if (date_of_filing || date_of_hearing || char_count > 0) {
                    this.record.case = {};
                    this.record.case.case_id = buffer.case_id;
                    this.record.case.date_of_filing = date_of_filing;
                    this.record.case.court_index_number = court_index_number;
                    this.record.case.date_of_hearing = date_of_hearing;
                    this.record.case.judge = judge;
                    this.record.case.notes_hearing = notes_hearing;
                }
            }
        },
        getDisplayDateString:function(date_string) {
            if (date_string) {
                var year = date_string.substr(0,4);
                var month = date_string.substr(5,2);
                var day = date_string.substr(8,2);
                return month + '/' + day + '/' + year;
            } else {
                return null;
            }
        },
        getDatabaseDateString:function(date_string) {
            if ( date_string ) {
                var index1 = date_string.indexOf('/');
                var index2 = date_string.lastIndexOf('/');
                var day_char_count = index2-index1-1;
                var year = date_string.substr(index2+1,4);
                var month;
                if (index1 === 1 ) {
                    month = '0'.concat(date_string.substr(0,1));
                } else {
                    month = date_string.substr(0,2);
                }
                var day;
                if ( day_char_count === 1 ) {
                    day = '0'.concat(date_string.substr(index1+1,day_char_count));
                } else {
                    day = date_string.substr(index1+1,day_char_count);
                }
                return year + '-' + month + '-' + day;
            } else {
                return null;
            }
        },
        getDateFromDatabaseString:function (date_string) {
            if (date_string) {
                var year = date_string.substr(0,4);
                var month = date_string.substr(5,2) - 1;
                var day = date_string.substr(8,2);
                return new Date(year,month,day);
            } else {
                return null;
            }
        },
        getDatabaseStringFromDate:function(date_object) {
            if (date_object) {
                var month = date_object.getMonth() + 1;
                if (month < 10) {
                    month = '0'.concat(month.toString());
                }
                var day =  date_object.getDate();
                if (day < 10) {
                    day = '0'.concat(day.toString());
                }
                return date_object.getFullYear()+ "-" + month + "-" + day;
            } else {
                return null;
            }
        },
        getDetail:function (field_name) {
            var field_text = '';
            var n = this.record.details.length;
            for (var i = 0; i < n; i++) {
                if (field_name.localeCompare(this.record.details[i].field_name) == 0) {
                    field_text = this.record.details[i].field_text;
                    break;
                }
            }
            this.current[field_name] = (field_text) ? field_text.trim() : '';
        },
        putDetail:function (field_name) {
            if (this.record) {
                var text = this.current[field_name];
                var found = false;
                var n = this.record.details.length;
                for (var i = 0; i < n; i++) {
                    if (field_name.localeCompare(this.record.details[i].field_name) == 0) {
                        this.record.details[i].field_text = (text) ? text.trim() : '';
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    var field_text = (text) ? text.trim() : '';
                    if (field_text.length > 0) {
                        var detail_record = {};
                        detail_record.id = null;
                        detail_record.field_name = field_name;
                        detail_record.field_text = field_text;
                        this.record.details.push(detail_record);
                    }
                }
            }
        },
        setCitizenship:function () {
            if (this.current && this.current.citizenship_status) {
                this.current.is_citizen = (this.current.citizenship_status.localeCompare('US Citizen: born in US/citizen parents') == 0 );
            }
        },
        putId:function(id){
            return (id >= 0) ? id : null;
        },
        setMarriage:function () {
            if (this.current && this.current.marital_status) {
                this.current.no_marriage = (this.current.marital_status.localeCompare('Single, never married') === 0);
            }
        },
        newRecord:function () {
            var self = this;
            self.record = {};
            self.record.id = null;
            self.record.client = {};
            self.record.case = null;
            self.record.details = [];
            self.record.firms = {};
            self.firms.reset();
            self.record.attorneys = {};
            self.attorneys.reset();
            self.current = {};
            _.each(self.fields, function(field, index, list) {
                var name = field.name;
                switch (name) {
                    case 'state':
                        self.current[name] = 'NY';
                        break;
                    case 'is_accepted':
                        self.current[name] = 'Yes';
                        break;
                    case 'on_hold':
                        self.current[name] = 0;
                        break;
                    case 'name_change_completed':
                        self.current[name] = 'No';
                        break;
                    case 'is_closed':
                        self.current[name] = 'No';
                        break;
                    case 'created_at':
                        self.current[name] = '0000-00-00 00:00:00';
                        break;
                    case 'updated_at':
                        self.current[name] = '0000-00-00 00:00:00';
                        break;
                    default:
                        self.current[name] = null;
                        break;
                }
            });
            self.clearCheckLists();
        },
        isNew:function () {
            return (this.record ? (this.record.id === null) : false);
        },
        putRecord:function () {
            return (this.record ? angular.toJson(this.putFields()) : null);
        },
        setCaseOpen: function () {
            this.current.is_closed = 'No';
            this.current.is_accepted = 'Yes';
            this.reasons_for_rejection = null;
        },
        removeListEntry:function (id) {
            if (this.list) {
                this.list = _.reject(this.list,function(item){ return item.id === id; });
            }
        }
    };
    $scope.displayError = "error";
    $scope.fieldData = ClientFieldsDataSource.list({}, function() {
        $scope.client.loadFields($scope.fieldData.fields);
    });
    $scope.referrerData = ReferrerDataSource.list({}, function () {
        $scope.referrers.loadRecord($scope.referrerData);
    });
    $scope.firmData = FirmDataSource.list({}, function () {
        $scope.firms.loadRecord($scope.firmData);
    });
    $scope.judgeData = JudgeDataSource.list({}, function () {
        $scope.judges.loadRecord($scope.judgeData);
    });
    $scope.closedClientFilter = {
        complete: 'No',
        accepted: 'Yes',
        intake: 'No',
        assigned: 'No',
        reset: function() {
            this.complete = 'No';
            this.accepted = 'Yes';
            this.intake = 'No';
            this.assigned = 'No';
        },
        getColumnDefs: function() {
            var result = [];
            result.push({field:'id', displayName:'ID', width:'50px'});
            result.push({field:'preferred_name', displayName:'Preferred Name'});
            result.push({field:'date_intake_completed', displayName:'Intake Date',width:'100px'});
            result.push({field:'date_of_assignment', displayName:'Assigned On',width:'100px'});
            result.push({field:'name_change_completed', displayName:'Completed',width:'80px'});
            result.push({field:'is_accepted', displayName:'Accepted',width:'80px'});
            result.push({field:'reasons_for_rejection', displayName:'Reason For Rejection'});
            result.push({field:'updated_at.date', displayName:'Last Update',width:'160px'});
            return result;
        },
        getQueryArgs: function() {
            var result = {};
            result.complete = this.complete;
            result.accepted = this.accepted;
            result.intake = this.intake;
            result.assigned = this.assigned;
            return result;
        }
    };
    $scope.selectedFirmId = 0;
    $scope.zoneFilter = 'All';
    $scope.firmFilter = '';
    $scope.filterClients = function() {
        $scope.client.clearDirectory();
        if ($scope.clientFilter === 'intake_list') {
            $scope.colDefs = $scope.client.columnDefs.intake_list;
            $scope.filterIntakeList();
        } else if ($scope.clientFilter === 'wait_list') {
            $scope.colDefs = $scope.client.columnDefs.wait_list;
            $scope.hold = 0;
            $scope.setCountyList();
        } else if ($scope.clientFilter === 'on_hold') {
            $scope.loading = true;
            $scope.colDefs = $scope.client.columnDefs.wait_list;
            $scope.hold = 1;
            $scope.setCountyListList();
        }
        else if ( $scope.clientFilter === 'in_progress') {
            $scope.loading = true;
            $scope.colDefs = $scope.client.columnDefs.in_progress;
            $scope.filterInProgress();
        } else if ( $scope.clientFilter === 'for_firm') {
            $scope.zoneFilter = 'All';
            $scope.attorneyFilter = '';
            $scope.colDefs = $scope.client.columnDefs.for_firm;
        } else if ($scope.clientFilter === 'closed_list') {
            $scope.zoneFilter = 'All';
            $scope.closedClientFilter.reset();
            $scope.filterClosedClients();
        } else if ( $scope.clientFilter === 'case_list') {
            $scope.zoneFilter = 'All';
            $scope.colDefs = $scope.client.columnDefs.case_list;
            $scope.loading = true;
            $scope.directory = ClientDataSource.caseList({}, function () {
                $scope.client.loadDirectory($scope.directory);
                $scope.loading = false;
            });
        } else if ( $scope.clientFilter === 'all_clients_alt') {
            $scope.zoneFilter = 'All';
            $scope.colDefs = $scope.client.columnDefs.all_clients_alt;
            $scope.loading = true;
            $scope.directory = ClientDataSource.list2({}, function () {
                $scope.client.loadDirectory($scope.directory);
                $scope.loading = false;
            });
        } else if ($scope.clientFilter === 'search') {
            $scope.zoneFilter = 'All';
            $scope.colDefs = $scope.client.columnDefs.search;
            if ($scope.enableSearch()) {
                $scope.applyFilters();
            }
        } else if ( $scope.clientFilter === 'all_clients') {
            $scope.zoneFilter = 'All';
            $scope.colDefs = $scope.client.columnDefs.all_clients;
            $scope.loading = true;
            $scope.directory = ClientDataSource.list({}, function () {
                $scope.client.loadDirectory($scope.directory);
                $scope.loading = false;
            });
        } else {
            $scope.zoneFilter = 'All';
            $scope.colDefs = $scope.client.columnDefs.all_clients;
            filterRecentClients();
        }
    };
    var filterRecentClients = function() {
        $scope.loading = true;
        $scope.directory = ClientDataSource.since({object:$scope.sinceDate}, function () {
            $scope.client.loadDirectory($scope.directory);
            $scope.loading = false;
        });
    }
    $scope.filterClosedClients = function() {
        $scope.colDefs = $scope.closedClientFilter.getColumnDefs();
        $scope.loading = true;
        $scope.directory = ClosedClients.query(
            $scope.closedClientFilter.getQueryArgs(), function () {
                $scope.client.loadDirectory($scope.directory);
                $scope.loading = false;
            });
    };
    $scope.notNYC = function(county) {
        var nyc_counties = [
            'Bronx County',
            'Kings County (Brooklyn)',
            'New York County (Manhattan)',
            'Queens County',
            'Richmond County (Staten Island)'
        ];
        var result = false;
        if ( $scope.zoneFilter === 'All') {
            result = ! (_.find(nyc_counties, function(borough){ return borough === county; }));
        }
        return result;
    };
    $scope.filterInProgress = function() {
        var zone = _.find($scope.zones, function(zone){ return $scope.zoneFilter === zone.name; });
        var firm = $scope.firms.getId($scope.firmFilter);
        $scope.directory = ClientsInProgress.query({zone:zone.id,firm:firm}, function () {
            $scope.client.loadDirectory($scope.directory);
            $scope.loading = false;
        });
    };
    $scope.filterIntakeList = function() {
        var zone = _.find($scope.zones, function(zone){ return $scope.zoneFilter === zone.name; });
        $scope.loading = true;
        $scope.directory = ClientDataSource.intakeList({object:zone.id}, function () {
            $scope.client.loadDirectory($scope.directory);
            $scope.loading = false;
        });
    };
    $scope.setCountyList = function() {
        var zone = _.find($scope.zones, function(zone){ return $scope.zoneFilter === zone.name; });
        $scope.county_for_zone_list = StateInfo.countiesforzone({object1:zone.id}, function() {
            $scope.countyFilter = 'All';
            $scope.counties_for_zone = $scope.county_for_zone_list.counties;
            $scope.filterWaitList();
        });
    };
    $scope.filterWaitList = function() {
        var zone = _.find($scope.zones, function(zone){ return $scope.zoneFilter === zone.name; });
        $scope.loading = true;
        $scope.directory = ClientsWaiting.query({zone:zone.id,county:$scope.countyFilter,hold:$scope.hold}, function () {
            $scope.client.loadDirectory($scope.directory);
            $scope.loading = false;
        });
    };
    $scope.firmFilter = '';
    $scope.filterFirm = function() {
        var zone = _.find($scope.zones, function(zone){ return $scope.zoneFilter === zone.name; });
        var firm_id = $scope.firms.getId($scope.firmFilter);
        if (firm_id) {
            $scope.loading = true;
            $scope.attorneys.data = FirmAttorneyDataSource.forFirm({firm_id:firm_id,zone_id:zone.id}, function () {
                $scope.attorneys.loadRecord($scope.attorneys.data);
            });
            $scope.directory = FirmClientDataSource.forFirm({firm_id:firm_id,zone_id:zone.id}, function () {
                $scope.client.loadDirectory($scope.directory);
                $scope.loading = false;
            });
        }
    };
    $scope.showFilters = true;
    $scope.filterCmd = "Hide";
    $scope.filterIcon = "glyphicon glyphicon-circle-arrow-down";
    $scope.toggleFilters = function() {
        $scope.showFilters = !$scope.showFilters;
        if ($scope.showFilters) {
            $scope.filterCmd = "Hide";
            $scope.filterIcon = "glyphicon glyphicon-circle-arrow-down";
        } else {
            $scope.filterCmd = "Show";
            $scope.filterIcon = "glyphicon glyphicon-circle-arrow-right";
        }
    };
    $scope.filterTypes = {
        boolean: [
            {label:"Yes",value:"true",inputs:0},
            {label:"No",value:"false",inputs:0}
        ],
        checkbox: [
            {label:"includes",value:"contains",inputs:1,options:[]},
            {label:"does not include",value:"does not contain",inputs:1,options:[]}
        ],
        date: [
            {label:"is",value:"is",inputs:1},
            {label:"is not",value:"is not",inputs:1},
            {label:"is after",value:"is after",inputs:1},
            {label:"is before",value:"is before",inputs:1},
            {label:"is in the range",value:"is in the range",inputs:2}
        ],
        email: [
            {label:"is",value:"is",inputs:1},
            {label:"is not",value:"is not",inputs:1},
            {label:"is empty",value:"is empty",inputs:0},
            {label:"is not empty",value:"is not empty",inputs:0},
            {label:"begins with",value:"begins with",inputs:1},
            {label:"ends with",value:"ends with",inputs:1},
            {label:"contains",value:"contains",inputs:1},
            {label:"does not contain",value:"does not contain",inputs:1}
        ],
        number: [
            {label:"is",value:"is",inputs:1},
            {label:"is not",value:"is not",inputs:1},
            {label:"is greater than",value:"is greater than",inputs:1},
            {label:"is less than",value:"is less than",inputs:1},
            {label:"is in the range",value:"is in the range",inputs:2}
        ],
        radio: [
            {label:"Yes",value:"yes",inputs:0},
            {label:"No",value:"no",inputs:0}
        ],
        select: [
            {label:"is",value:"is",inputs:1,options:[]},
            {label:"is not",value:"is not",inputs:1,options:[]}
        ],
        text: [
            {label:"is",value:"is",inputs:1},
            {label:"is not",value:"is not",inputs:1},
            {label:"is empty",value:"is empty",inputs:0},
            {label:"is not empty",value:"is not empty",inputs:0},
            {label:"begins with",value:"begins with",inputs:1},
            {label:"ends with",value:"ends with",inputs:1},
            {label:"contains",value:"contains",inputs:1},
            {label:"does not contain",value:"does not contain",inputs:1}
        ],
        textarea: [
            {label:"is empty",value:"is empty",inputs:0},
            {label:"is not empty",value:"is not empty",inputs:0},
            {label:"begins with",value:"begins with",inputs:1},
            {label:"ends with",value:"ends with",inputs:1},
            {label:"contains",value:"contains",inputs:1},
            {label:"does not contain",value:"does not contain",inputs:1}
        ],
        timestamp: [
            {label:"is",value:"is",inputs:1},
            {label:"is not",value:"is not",inputs:1},
            {label:"is after",value:"is after",inputs:1},
            {label:"is before",value:"is before",inputs:1},
            {label:"is in the range",value:"is in the range",inputs:2}
        ]
    };
    // Search filter

    $scope.filterText = null;
    var initCondition = function(condition) {
        condition.option = null;
        condition.options = [];
        condition.value1 = null;
        condition.value2 = null;
        condition.inputs = 0;
    };
    $scope.addCondition = function(filterGroup, afterCondition) {
        var newCondition = {};
        newCondition.fields = $scope.fields;
        newCondition.field = null;
        initCondition(newCondition);
        if (! afterCondition) {
            filterGroup.conditions.push(newCondition);
        } else {
            var i = _.indexOf(filterGroup.conditions, afterCondition);
            if (i >= 0) {
                var head = _.first(filterGroup.conditions,i+1);
                var tail = _.rest(filterGroup.conditions,i+1);
                head.push(newCondition);
                filterGroup.conditions = head.concat(tail);
            } else {
                filterGroup.conditions.push(newCondition);
            }
        }
        if ($scope.clientFilter === 'search' ) {
            $scope.filterText = null;
            $scope.client.clearDirectory();
        }
    };
    $scope.removeCondition = function(filterGroup, condition) {
        filterGroup.conditions = _.reject(filterGroup.conditions, function(elem){ return elem === condition; });
        $scope.filterText = null;
        $scope.client.clearDirectory();
        if (filterGroup.conditions.length === 0 ) {
            $scope.addCondition(filterGroup,null);
        }
    };
    $scope.changeConditionOption = function(condition) {
        if (condition) {
            condition.inputs = condition.option.inputs;
            $scope.filterText = null;
            $scope.client.clearDirectory();
        }
    };
    $scope.changeConditionField = function(condition) {
        if (condition) {
            initCondition(condition);
            condition.options = $scope.filterTypes[condition.field.type];
            $scope.filterText = null;
            $scope.client.clearDirectory();
        }
    };
    $scope.changeConditionValue = function(condition) {
        if (condition) {
            $scope.filterText = null;
            $scope.client.clearDirectory();
        }
    };
    var dbStringFromDate = function(date_object) {
        if (date_object) {
            var month = date_object.getMonth() + 1;
            if (month < 10) {
                month = '0'.concat(month.toString());
            }
            var day =  date_object.getDate();
            if (day < 10) {
                day = '0'.concat(day.toString());
            }
            return date_object.getFullYear()+ "-" + month + "-" + day;
        } else {
            return null;
        }
    };
    var getConditionObject = function(condition) {
        var obj = null;
        if ( condition.field && condition.option) {
            obj = {};
            obj.field = condition.field.name;
            obj.table = condition.field.table;
            obj.type = condition.field.type;
            obj.option = condition.option.value;
            if (obj.type === 'date') {
                obj.value1 = dbStringFromDate(condition.value1);
                obj.value2 = (condition.value2) ? dbStringFromDate(condition.value2) : null;
            } else if (obj.type === 'timestamp') {
                obj.value1 = dbStringFromDate(condition.value1).concat(' 00:00:00');
                obj.value2 = (condition.value2) ? dbStringFromDate(condition.value2).concat(' 23:59:99') : null;
            } else {
                obj.value1 = condition.value1;
                obj.value2 = condition.value2;
            }
        }
        return obj;
    };
    var getConditionList = function(conditions) {
        var result = [];
        for (var i = 0; i < conditions.length; i++ ) {
            var obj = getConditionObject(conditions[i]);
            if (obj) {
                result.push(obj);
            }
        }
        return result;
    };
    var validCondition = function(condition) {
        return (condition.field && condition.option);
    };
    var validateConditions = function(conditions) {
        var result = false;
        for (var i = 0; i < conditions.length; i++ ) {
            result = validCondition(conditions[i]);
        }
        return result;
    };
    $scope.enableSearch = function() {
        return validateGroup($scope.filterGroups[0]);
    };
    $scope.enableDownload = function() {
        return ($scope.filterText && ($scope.client.sizeOfDirectory() > 0));
    };
    var validateGroup = function(group) {
        var valid = false;
        if (group) {
            valid = validateConditions(group.conditions);
            if (valid) {
                for (var i = 0; i < group.groups.length; i++) {
                    valid = validateGroup(group.groups[i]);
                    if (!valid) break;
                }
            }
        }
        return valid;
    };
    var parseGroup = function(group) {
        var obj = null;
        if (group){
            obj = {};
            obj.id = group.name;
            obj.op = group.operator;
            obj.conditions = getConditionList(group.conditions);
            obj.groups = [];
            for (var i = 0; i < group.groups.length; i++) {
                var groupObj = parseGroup(group.groups[i]);
                if (groupObj) {
                    obj.groups.push(groupObj);
                }
            }
        }
        return obj;
    };
    $scope.filterGroups = [{name: "Group", operator: 'and', conditions: [], groups: []}];
    var filterFieldData = ClientFieldsDataSource.filters({}, function() {
        $scope.fields = filterFieldData.fields;
        $scope.addCondition($scope.filterGroups[0],null);

    });
    $scope.deleteFilterGroup = function(filterGroup) {
        filterGroup.groups = [];
    };
    $scope.addFilterGroup = function(filterGroup) {
        var id = filterGroup.groups.length + 1;
        var name = filterGroup.name + '-' + id;
        var newGroup = {name: name, operator: 'and', conditions: [], groups: []};
        $scope.addCondition(newGroup,null);
        filterGroup.groups.push(newGroup);
    };
    $scope.applyFilters = function() {
        var queryData = parseGroup($scope.filterGroups[0]);
        if (queryData) {
            $scope.filterText = angular.toJson(queryData);
            $scope.loading = true;
            $scope.directory = ClientDataSource.search(angular.toJson(queryData), function () {
                $scope.loading = false;
                $scope.client.loadDirectory($scope.directory);
                //TODO: Test form submit for csv downloads
                //  Also should probably cache results in temp file and return filename.
                //  That will necessitate adding a cron job to deleted expired temp files.
                //  When that functionality is in place, it will be possible to implement full csv downloads
                //  via post requests with query and field parameters.
                //  -> issue the request, server creates file and returns file name,
                //  -> on receipt of filename, bind it to scope variable and enable download button as in email download
            });
        }
    };
    $scope.setInputs = function(field) {
        var filter = angular.fromJson(field.filter);
        field.inputs = filter.inputs;
    };
    $scope.selectedClients = [];
    $scope.$watch('selectedClients[0]', function () {
        $scope.selectClient();
    });
    var clearGridSelectionPlugin = {
        init:function (scope, grid, services) {
            scope.$on('ClearGridSelection', function (e) {
                scope.toggleSelectAll(false);
            });
        }
    };
    $scope.gridOptions = {
        data:'client.list',
        showSelectionCheckbox:false,
        selectWithCheckboxOnly:false,
        keepLastSelected:false,
        enableSorting: true,
        showColumnMenu: true,
        showFilter: true,
        showFooter: true,
        footerRowHeight: 32,
        selectedItems:$scope.selectedClients,
        multiSelect:false,
        columnDefs:'colDefs',
        enableColumnResize: true,
        plugins:[clearGridSelectionPlugin]
    };
    $scope.selectedFirmAttorneys = [];
    var self = this;
    self.attorneyLayoutPlugin = new ngGridLayoutPlugin();
    $scope.firmAttorneyGridOptions = {
        data:'firmAttorneys',
        showSelectionCheckbox:true,
        selectWithCheckboxOnly:false,
        keepLastSelected:true,
        enableSorting: false,
        showColumnMenu: false,
        selectedItems:$scope.selectedFirmAttorneys,
        multiSelect:true,
        afterSelectionChange:function(rowItem, event) {
            if ($scope.isFlipped) {
                var attorney = $scope.firmAttorneys[rowItem.rowIndex];
                if (!rowItem.selected) {
                    $scope.client.attorneys.remove(attorney);
                } else {
                    $scope.client.attorneys.add(attorney);
                }
                isDirty = true;
            }
        },
        plugins:[self.attorneyLayoutPlugin],
        columnDefs:[
            {field:'id', displayName:'ID', width:'50px'},
            {field:'name', displayName:'Name'},
            {field:'phone', displayName:'Phone'},
            {field:'email', displayName:'Email'}
        ]
    };
    $scope.selectedFirms = [];
    self.firmLayoutPlugin = new ngGridLayoutPlugin();
    $scope.firmGridOptions = {
        data:'firms.list',
        showSelectionCheckbox:true,
        selectWithCheckboxOnly:false,
        keepLastSelected:true,
        enableSorting: true,
        showColumnMenu: false,
        selectedItems:$scope.selectedFirms,
        multiSelect:true,
        afterSelectionChange:function(rowItem, event) {
            if ($scope.isFlipped) {
                var firm = $scope.firms.list[rowItem.rowIndex];
                if (!rowItem.selected) {
                    $scope.client.firms.remove(firm);
                    clearFirmAttorneys(firm.id);
                } else {
                    $scope.client.firms.add(firm);
                }
                isDirty = true;
            }
        },
        plugins:[self.firmLayoutPlugin],
        columnDefs:[
            {field:'name', displayName:'Name'}
        ]
    };
    var clearFirmAttorneys = function(firm_id) {
        $scope.client.attorneys.removeFirm(firm_id);
        $scope.firm_name = $scope.client.firms.elemList.length ? $scope.client.firms.elemList[0].name : null;
        loadFirmAttorneys();
    };
    var loadFirmAttorneysForZone = function(zone_id) {
        var firm_id = ($scope.firm_name) ? $scope.firms.getId($scope.firm_name) : null;
        if (firm_id) {
            $scope.firmAttorneyList = FirmAttorneyDataSource.forFirm({firm_id:firm_id,zone_id:zone_id}, function () {
                $scope.firmAttorneys = $scope.firmAttorneyList.attorneys;
                selectAssignedAttorneys();
            });
        }
    };
    var loadFirmAttorneys = function() {
        $scope.firmAttorneys = null;
        var zone = _.find($scope.zones, function(zone){ return $scope.zoneFilter == zone.name; });
        loadFirmAttorneysForZone(zone.id);
    };
    var getZoneAttorneys = function(client) {
        var args = {object1:client.state,object2:client.county};
        var zones = StateInfo.zones(args, function() {
            angular.forEach(zones, function(value, key) {
                if (key == 'zones') {
                    loadFirmAttorneysForZone(value.id);
                }
            });
        });
    };
    var loadFirmAttorneysForClient = function(state,county){
        $scope.firmAttorneys = null;
        var args = {object1:state,object2:county};
        var zones = StateInfo.zones(args, function() {
            angular.forEach(zones, function(data, index) {
                loadFirmAttorneysForZone(data.zone_id);
            });
        });
    };
    var selectAssignedFirms = function() {
        if ($scope.client.current) {
            angular.forEach($scope.firms.list, function(data, index){
                if($scope.client.firms.hasId(data.id)){
                    $scope.firmGridOptions.selectItem(index, true);
                } else {
                    $scope.firmGridOptions.selectItem(index, false);
                }
            });
        }
    };
    var selectAssignedAttorneys = function() {
        if ($scope.client.current) {
            angular.forEach($scope.firmAttorneys, function(data, index){
                if($scope.client.attorneys.hasId(data.id)){
                    $scope.firmAttorneyGridOptions.selectItem(index, true);
                } else {
                    $scope.firmAttorneyGridOptions.selectItem(index, false);
                }
            });
        }
    };
    $scope.$on('ngGridEventData', function(e){
        if (e.targetScope.gridId === $scope.firmGridOptions.gridId) {
            selectAssignedFirms();
        } else if (e.targetScope.gridId === $scope.firmAttorneyGridOptions.gridId) {
            selectAssignedAttorneys();
        }
    });
    $scope.newAttorney = false;
    $scope.toggleNewAttorney = function() {
           $scope.newAttorney = !$scope.newAttorney;
           if ($scope.newAttorney) {
                var firm_id = $scope.firms.getId($scope.firm_name);
                $scope.new_attorney.openRecord(firm_id);
           } else {
                $scope.new_attorney.clearRecord();
           }
    };
    $scope.cancelNewAttorney = function() {
        $scope.new_attorney.clearRecord();
        $scope.newAttorney = false;
    };
    $scope.saveNewAttorney = function() {
        $scope.attorney_response = AttorneyDataSource.add($scope.new_attorney.putRecord(),function () {
            $scope.cancelNewAttorney();
            if ($scope.attorney_response.attorney) {
                var record = $scope.attorney_response.attorney;
                var attorney = {};
                attorney.id = record.id.toString();
                attorney.name = record.last_name.concat(', ',record.first_name) ;
                attorney.phone =  record.phone;
                attorney.email = record.email;
                attorney.firm_id = record.firm_id;
                attorney.firm_name = record.firm_name;
                $scope.client.attorneys.add(attorney);
                loadFirmAttorneys();
                isDirty = true;
            }
        });
    };
    $scope.firm_name = null;
    $scope.changeFirm = function() {
        getZoneAttorneys($scope.client.current);
    };
    $scope.changeReferrer = function () {
        if ($scope.client.current && $scope.referrers) {
            $scope.client.current.referrer_id = $scope.referrers.getId($scope.referrer_name);
        }
    };
    $scope.openClient = function() {
       $scope.client.setCaseOpen();
       isDirty = true;
    };
    $scope.showClient = function () {
        var show = (typeof($scope.client.current) !== 'undefined' && $scope.client.current !== null);
        if (!show)  {
            $('#client-list').trigger('resize');
        }
        return show;
    };
    $scope.noValidChanges = function() {
        return $scope.intake_form.$invalid ||
            ($scope.notes_form.$pristine && $scope.intake_form.$pristine
                && ($scope.case_form.$pristine || !$scope.case_form.new_attorney_form.$pristine) && !isDirty);
    };
    $scope.showDetails = function(){
        $scope.$emit('HideHeader');
        $scope.bannerVisible = "banner-show";
        $scope.setOffsets($scope.showNotes);
        $scope.assignAttorneys = false;
        $scope.attorneysIcon = 'glyphicon glyphicon-circle-arrow-right';
        $scope.assignFirms = false;
        $scope.firmsIcon = 'glyphicon glyphicon-circle-arrow-right';
        $scope.clientForm = $scope.clientForms[0];
        $scope.showForm();
    };
    $scope.hideDetails = function(){
        $scope.$emit('ShowHeader');
        $scope.bannerVisible = "banner-hide";
        $scope.setOffsets($scope.showNotes);
    };
    $scope.newClient = function () {
        $scope.$broadcast('ClearGridSelection');
        $scope.client.newRecord();
        $scope.getCounties($scope.client.current.state);
        $scope.firm_name = null;
        $scope.referrer_name = null;
        $scope.showDetails();
    };
    $scope.resetForms = function() {
        $scope.notes_form.$setPristine();
        $scope.intake_form.$setPristine();
        $scope.case_form.$setPristine();
        isDirty = false;
    };
    $scope.selectClient = function () {
        if ($scope.selectedClients[0]) {
            $scope.showDetails();
            $scope.resetForms();
            $scope.firm_name = null;
            $scope.firmAttorneys = null;
            $scope.record = ClientDataSource.query({object:$scope.selectedClients[0].id}, function () {
                $scope.client.loadRecord($scope.record);
                if ($scope.client.current && $scope.client.current.state) {
                    $scope.getCounties($scope.client.current.state);
                }
                if ($scope.client.current && $scope.firms) {
                    $scope.firm_name = ($scope.client.firms.elemList.length > 0) ? $scope.client.firms.elemList[0].name : null;
                    selectAssignedFirms();
                    loadFirmAttorneysForClient($scope.client.current.state,$scope.client.current.county);
                }
                $scope.referrer_name = ($scope.client.current && $scope.referrers) ? $scope.referrers.getName($scope.client.current.referrer_id) : null;
            });
        }
    };
    $scope.clearClient = function() {
        $scope.filterClients();
        $scope.$broadcast('ClearGridSelection');
        $scope.client.current = null;
        $scope.firm_name = null;
        $scope.firmAttorneys = null;
        $scope.referrer_name = null;
        $scope.cancelNewAttorney();
        $scope.hideDetails();
    };
    $scope.closeClient = function() {
        if ($scope.noValidChanges()){
            $scope.clearClient();
        } else {
            $scope.confirmClose();
        }
    };
    $scope.deleteClient = function () {
        $scope.status = ClientDataSource.delete($scope.client.putRecord(), function () {
            $scope.client.removeListEntry($scope.selectedClients[0].id);
            $scope.clearClient();
        });
    };
    var savePending = false;
    $scope.saveClient = function (done) {
        if (!savePending) {
            var callback;
            if (done) {
                callback = function () { $scope.client.loadRecord($scope.record); $scope.clearClient(); savePending = false;};
            } else {
                callback = function () { $scope.client.loadRecord($scope.record); $scope.resetForms(); savePending = false;};
            }
            savePending = true;
            if ($scope.client.isNew()) {
                $scope.record = ClientDataSource.add($scope.client.putRecord(), callback);
            } else {
                $scope.record = ClientDataSource.update($scope.client.putRecord(), callback);
            }
        }
    };
    $scope.toggleAttorneys = function() {
        $scope.assignAttorneys = !$scope.assignAttorneys;
        if ($scope.assignAttorneys) {
            $scope.attorneysIcon = 'glyphicon glyphicon-circle-arrow-down';
            $scope.changeFirm();
            self.attorneyLayoutPlugin.updateGridLayout();

        } else {
            $scope.attorneysIcon = 'glyphicon glyphicon-circle-arrow-right';
            $scope.newAttorney = false;
        }
        $('#attorney-list').trigger('resize');
    };
    $scope.toggleFirms = function() {
        $scope.assignFirms = !$scope.assignFirms;
        if ($scope.assignFirms) {
            $scope.firmsIcon = 'glyphicon glyphicon-circle-arrow-down';
            self.firmLayoutPlugin.updateGridLayout();
        } else {
            $scope.firmsIcon = 'glyphicon glyphicon-circle-arrow-right';
        }
        $('#firm-list').trigger('resize');
    };
    $scope.sendAgreement = function() {
        if (! $scope.emailInProgess ) {
            $scope.emailInProgress = true;
            var status = EmailAgreement.query({id:$scope.client.current.id}, function() {
                $scope.emailInProgress = false;
            })
        }
    };
    $scope.emailInProgress = false;
    $scope.sinceDate = moment().subtract('days', 14).format("YYYY-MM-DD").concat(' 00:00:00');
    $scope.clientFilter = 'recent_clients';
    $scope.filterClients();
}]);

ncpApp.controller('FirmsCtrl', ['$scope', '$modal', 'FirmDataSource', 'DonorLevelDataSource', function($scope, $modal, FirmDataSource, DonorLevelDataSource) {
    $scope.banner = "banner-hide";
    $scope.firm = {
        current:null,
        directory:null,
        errors: {
            phone:"Enter phone numbers as digits separated by dashes (e.g. 999-999-9999x9999). Extensions are optional and are preceded by an x followed by 1 to 4 digits.",
            url: "URL must begin with http://",
            zip:"Invalid zip code. (e.g. 99999 or 99999-9999)"
        },
        list:null,
        record:null,
        template:{
            id:null,
            donor_level_id:'1',
            short_name:'',
            full_name:'',
            phone:'',
            address_1:'',
            address_2:'',
            zip:'',
            is_active:'Yes',
            url:'',
            notes_general:'',
            created_at:'0000-00-00 00:00:00',
            updated_at:'0000-00-00 00:00:00'
        },
        loadDirectory:function (directory) {
            this.directory = directory;
            if (this.directory) {
                this.list = _.sortBy(this.directory.firms, function(item){
                    return item['name'].toLowerCase();
                });
            } else {
                this.list = null;
            }
        },
        loadRecord:function (record) {
            this.record = record;
            this.current = (this.record ? this.record.firm : null);
        },
        newRecord:function () {
            this.record = {};
            this.record.firm = angular.copy(this.template);
            this.current = this.record.firm;
        },
        isNew:function () {
            return (this.record ? (this.record.firm.id === null) : false);
        },
        putRecord:function () {
            return (this.current ? angular.toJson(this.current) : null);
        },
    };
    $scope.donorLevels = {
        data:null,
        list:null,
        getName:function (id) {
            return ((this.id_to_name) ? this.id_to_name[id] : null);
        },
        getId:function (name) {
            return ((this.name_to_id) ? this.name_to_id[name] : null);
        },
        loadRecord:function (data) {
            this.data = data;
            if (this.data) {
                this.list = _.sortBy(this.data.levels, function(item){
                    return item.rank;
                });
            } else {
                this.list = null;
            }
            if (this.list) {
                this.id_to_name = {};
                this.name_to_id = {};
                var n = this.list.length;
                for (var i = 0; i < n; i++) {
                    var id = this.list[i].id;
                    var name = this.list[i].name;
                    this.id_to_name[id] = name;
                    this.name_to_id[name] = id;
                }
                this.names = this.list.map(function (item) {
                    return item.name;
                });
            }
        }
    };
    $scope.donorLevelName = ' ';
    $scope.confirmDelete = function(){
        var modalInstance = $modal.open({
            templateUrl: 'confirmDelete.html',
            controller: 'ConfirmDeleteCtrl'
        });

        modalInstance.result.then(function (result) {
            if (result === 'delete') {
                $scope.deleteFirm();
            }
        });
    };
    $scope.noValidChanges = function() {
        return $scope.firm_form.$invalid || $scope.firm_form.$pristine;
    };
    $scope.closeFirm = function() {
        if ($scope.noValidChanges()){
            $scope.clearFirm();
        } else {
            $scope.confirmClose();
        }
    };
    $scope.confirmClose = function(){
        var modalInstance = $modal.open({
            templateUrl: 'confirmClose.html',
            controller: 'ConfirmCloseCtrl'
        });

        modalInstance.result.then(function (result) {
            if (result === 'close') {
                $scope.clearFirm();
            } else if (result === 'save') {
                $scope.saveFirm(true);
            }
        });
    };
    $scope.filterFirms = function () {
        $scope.loading = true;
        $scope.directory = FirmDataSource.sorted_list({}, function () {
            $scope.firm.loadDirectory($scope.directory)
            $scope.loading = false;
        });
    };
    $scope.changeDonorLevel = function () {
        if ($scope.firm.current && $scope.donorLevels) {
            $scope.firm.current.donor_level_id = $scope.donorLevels.getId($scope.donorLevelName);
        }
    };
    $scope.donorLevelData = DonorLevelDataSource.list({}, function () {
        $scope.donorLevels.loadRecord($scope.donorLevelData);
    });
    $scope.filterFirms();
    $scope.selectedFirms = [];
    $scope.$watch('selectedFirms[0]', function () {
        $scope.selectFirm();
    });
    var clearGridSelectionPlugin = {
        init:function (scope, grid, services) {
            scope.$on('ClearGridSelection', function (e) {
                scope.toggleSelectAll(false);
            });
        }
    };
    $scope.gridOptions = {
        data:'firm.list',
        displaySelectionCheckbox:false,
        selectWithCheckboxOnly:false,
        keepLastSelected:false,
        selectedItems:$scope.selectedFirms,
        enableSorting: true,
        showColumnMenu: true,
        showFilter: true,
        showFooter: true,
        footerRowHeight: 32,
        multiSelect:false,
        columnDefs:[
            {field:'id', displayName:'ID', width:'50px'},
            {field:'name', displayName:'Name'},
            {field:'donor_level_name', displayName:'Sponsorship', width:'100px'},
            {field:'is_active', displayName: 'Active', width:'100px'}
        ],
        plugins:[clearGridSelectionPlugin]
    };
    $scope.showFirm = function () {
        return (typeof($scope.firm.current) !== 'undefined' && $scope.firm.current !== null);
    };
    $scope.showDetails = function(){
        $scope.$emit('HideHeader');
        $scope.banner = "banner-show";
    };
    $scope.hideDetails = function(){
        $scope.$emit('ShowHeader');
        $scope.banner = "banner-hide";
    };
    $scope.newFirm = function () {
        $scope.$broadcast('ClearGridSelection');
        $scope.firm.newRecord();
        $scope.donorLevelName = ' ';
        $scope.showDetails();
    };
    $scope.selectFirm = function () {
        if ($scope.selectedFirms[0]) {
            $scope.showDetails();
            $scope.record = FirmDataSource.query({object:$scope.selectedFirms[0].id}, function () {
                $scope.firm.loadRecord($scope.record);
                $scope.donorLevelName = ($scope.firm.current && $scope.donorLevels) ? $scope.donorLevels.getName($scope.firm.current.donor_level_id) : null;
            });
        }
    };
    $scope.clearFirm = function () {
        $scope.$broadcast('ClearGridSelection');
        $scope.firm.current = null;
        $scope.donorLevelName = ' ';
        $scope.hideDetails();
        $scope.filterFirms();
    };
    $scope.deleteFirm = function () {
        $scope.status = FirmDataSource.delete($scope.firm.putRecord(), function () {
            $scope.clearFirm();
        });
    };
    $scope.resetForms = function() {
        $scope.firm_form.$setPristine();
    };
    var saveFirmPending;
    $scope.saveFirm = function (done) {
        if (!saveFirmPending) {
            var callback;
            callback = function () { $scope.firm.loadRecord($scope.record); $scope.clearFirm(); saveFirmPending = false;};
            if (done) {
                callback = function () { $scope.firm.loadRecord($scope.record); $scope.clearFirm(); saveFirmPending = false;};
            } else {
                callback = function () { $scope.firm.loadRecord($scope.record); $scope.resetForms(); saveFirmPending = false;};
            }
            saveFirmPending = true;
            if ($scope.firm.isNew()) {
                $scope.record = FirmDataSource.add($scope.firm.putRecord(), callback);
            } else {
                $scope.record = FirmDataSource.update($scope.firm.putRecord(), callback);
            }
        }
    };
}]);


ncpApp.controller('AttorneysCtrl',['$scope', '$modal', 'FirmDataSource',  'AttorneyDataSource', 'FirmAttorneyDataSource', 'ZoneInfo', function($scope, $modal, FirmDataSource, AttorneyDataSource, FirmAttorneyDataSource, ZoneInfo) {
    var ALL = "All";
    $scope.banner = "banner-hide";
    $scope.firm = {
        data:null,
        list:null,
        id_to_name:null,
        name_to_id:null,
        names:null,
        options:null,
        selected_id:null,
        selected_name:null,
        loadData:function (data) {
            this.data = data;
            if (this.data) {
                this.list = _.sortBy(this.data.firms, function(item){
                    return item['name'].toLowerCase();
                });
            } else {
                this.list = null;
            }
            if (this.list) {
                this.id_to_name = {};
                this.name_to_id = {};
                var n = this.list.length;
                for (var i = 0; i < n; i++) {
                    var id = this.list[i].id;
                    var name = this.list[i].name;
                    this.id_to_name[id] = name;
                    this.name_to_id[name] = id;
                }
                this.names = this.list.map(function (firm) {
                    return firm.name;
                });
                this.options = [ALL].concat(this.names);
            }
        },
        id:function (name) {
            return this.name_to_id[name];
        },
        name:function (id) {
            return this.id_to_name[id];
        },
        select:function (name) {
            this.selected_id = null;
            this.selected_name = null;
            if (name === ALL) {
                this.selected_id = ALL;
            } else {
                this.selected_id = this.name_to_id[name];
                this.selected_name = name;
            }
        },
        valid:function () {
            return (this.selected_id && this.selected_id !== ALL);
        }
    };

    $scope.attorney = {
        current:null,
        directory:null,
        errors: {
            phone:"Enter phone numbers as digits separated by dashes (e.g. 999-999-9999x9999). Extensions are optional and are preceded by an x followed by 1 to 4 digits.",
            cell: "Enter cell numbers as digits separated by dashes (e.g. 999-999-9999).",
            fax: "Enter fax numbers as digits separated by dashes (e.g. 999-999-9999).",
            email:"Email address must be of the form: someone@someplace.xxx where xxx is com, org, or some other two or three character domain name.",
            url: "URL must begin with http://"
        },
        firm_name:null,
        list:null,
        record:null,
        template:{
            id:null,
            last_name:'',
            first_name:'',
            middle_name:'',
            nick_name:'',
            title:'',
            is_firm_contact: 0,
            is_active: 0,
            phone:'',
            cell:'',
            fax:'',
            email:'',
            url:'',
            firm_id:null,
            zone_id:null,
            notes_general:'',
            created_at:'0000-00-00 00:00:00',
            updated_at:'0000-00-00 00:00:00'
        },
        loadDirectory:function (directory) {
            this.directory = directory;
            if (this.directory) {
                this.list = this.directory;
            } else {
                this.list = null;
            }
        },
        loadRecord:function (record, firm) {
            this.record = record;
            this.current = (this.record ? this.record.attorney : null);
            if (this.current) {
                this.firm_name = firm.name(this.current.firm_id);
            }
        },
        newRecord:function () {
            this.record = {};
            this.record.attorney = angular.copy(this.template);
            this.current = this.record.attorney;
        },
        isNew:function () {
            return (this.current) ? (this.current.id === null) : false;
        },
        putRecord:function () {
            return (this.current ? angular.toJson(this.current) : null);
        },
        changeFirm:function (firm) {
            if (this.current) {
                this.current.firm_id = firm.id(this.firm_name);
            }
        }
    };
    $scope.loading = true;
    $scope.zoneFilter = 'All';
    $scope.getZones = function() {
        $scope.zoneList = ZoneInfo.zoneList({}, function () {
            $scope.zones = $scope.zoneList.zones;
        });
    };
    $scope.getZoneName = function(zone_id) {
        var result = null;
        var n = $scope.zones.length;
        for ( var i = 0; i < n; i++ ) {
            if ($scope.zones[i].id === zone_id) {
                result = $scope.zones[i].name;
            }
        }
        return result;
    };
    $scope.getZoneId = function(zone_name) {
        var result = null;
        var n = $scope.zones.length;
        for ( var i = 0; i < n; i++ ) {
            if (zone_name.localeCompare($scope.zones[i].name) === 0) {
                result = $scope.zones[i].id;
            }
        }
        return result;
    };
    $scope.changeZone = function() {
        if ($scope.attorney.current) {
            $scope.attorney.current.zone_id = $scope.getZoneId($scope.zone_name);
        }
    };
    $scope.getZones();
    $scope.selectedFirm = '';
    $scope.selectedZone = 0;
    $scope.firmData = FirmDataSource.list({}, function () {
        $scope.firm.loadData($scope.firmData);
    });
    $scope.filterAttorneys = function() {
        $scope.loading = true;
        $scope.directory = FirmAttorneyDataSource.forFirm({firm_id:$scope.firm.selected_id,zone_id:$scope.selectedZone}, function () {
            $scope.attorney.loadDirectory($scope.directory.attorneys);
            $scope.loading = false;
        });
    };
    $scope.filterFirmByZone = function() {
        $scope.selectedZone = $scope.getZoneId($scope.zoneFilter);
        $scope.filterAttorneys();
    };
    $scope.selectFirm = function () {
        $scope.clearAttorney();
        $scope.firm.select($scope.selectedFirm);
        $scope.filterAttorneys();
    };
    $scope.filterAttorneys();
    $scope.selectedAttorneys = [];
    $scope.$watch('selectedAttorneys[0]', function () {
        $scope.selectAttorney();
    });
    var clearGridSelectionPlugin = {
        init:function (scope, grid, services) {
            scope.$on('ClearGridSelection', function (e) {
                scope.toggleSelectAll(false);
            });
        }
    };
    $scope.attorneyGridOptions = {
        data:'attorney.list',
        displaySelectionCheckbox:false,
        selectWithCheckboxOnly:false,
        enableSorting: true,
        showColumnMenu: true,
        showFilter: true,
        showFooter: true,
        footerRowHeight: 32,
        keepLastSelected:false,
        selectedItems:$scope.selectedAttorneys,
        multiSelect:false,
        columnDefs:[
            {field:'id', displayName:'ID', width:'50px'},
            {field:'last_name', displayName:'Last Name'},
            {field:'first_name', displayName:'First Name'},
            {field:'email', displayName:'Email'},
            {field:'location', displayName:'Location'},
            {field:'is_active', displayName:'Has Clients',width:'150px'}
        ],
        plugins:[clearGridSelectionPlugin]
    };

    $scope.changeFirm = function () {
        $scope.attorney.changeFirm($scope.firm);
    };
    $scope.confirmDelete = function(){
        var modalInstance = $modal.open({
            templateUrl: 'confirmDelete.html',
            controller: 'ConfirmDeleteCtrl'
        });

        modalInstance.result.then(function (result) {
            if (result === 'delete') {
                $scope.deleteFirm();
            }
        });
    };
    $scope.noValidChanges = function() {
        return $scope.attorney_form.$invalid || $scope.attorney_form.$pristine;
    };
    $scope.closeAttorney = function() {
        if ($scope.noValidChanges()){
            $scope.clearAttorney();
        } else {
            $scope.confirmClose();
        }
    };
    $scope.confirmClose = function(){
        var modalInstance = $modal.open({
            templateUrl: 'confirmClose.html',
            controller: 'ConfirmCloseCtrl'
        });

        modalInstance.result.then(function (result) {
            if (result === 'close') {
                $scope.clearAttorney();
            } else if (result === 'save') {
                $scope.saveAttorney(true);
            }
        });
    };

    $scope.showAttorney = function () {
        return (typeof($scope.attorney.current) !== 'undefined' && $scope.attorney.current !== null);
    };
    $scope.showDetails = function(){
        $scope.$emit('HideHeader');
        $scope.banner = "banner-show";
    };
    $scope.hideDetails = function(){
        $scope.$emit('ShowHeader');
        $scope.banner = "banner-hide";
    };
    $scope.newAttorney = function () {
        $scope.$broadcast('ClearGridSelection');
        $scope.attorney.newRecord();
        $scope.attorney.firm_name = $scope.firm.selected_name;
        $scope.attorney.changeFirm($scope.firm);
        $scope.showDetails();
    };
    $scope.selectAttorney = function () {
        if ($scope.selectedAttorneys[0]) {
            $scope.showDetails();
            $scope.record = AttorneyDataSource.query({object:$scope.selectedAttorneys[0].id}, function () {
                $scope.attorney.loadRecord($scope.record, $scope.firm);
                $scope.zone_name = $scope.getZoneName($scope.attorney.current.zone_id);
            });
        }
    };
    $scope.clearAttorney = function () {
        $scope.$broadcast('ClearGridSelection');
        $scope.attorney.current = null;
        $scope.hideDetails();
        $scope.filterFirmByZone();
    };
    $scope.deleteAttorney = function () {
        $scope.status = AttorneyDataSource.delete($scope.attorney.putRecord(), function () {
            $scope.filterAttorneys();
            $scope.clearAttorney();
        });
    };
    $scope.resetForms = function() {
        $scope.attorney_form.$setPristine();
    };
    var saveAttorneyPending;
    $scope.saveAttorney = function (done) {
        if (!saveAttorneyPending) {
            var callback;
            callback = function () { $scope.attorney.loadRecord($scope.record, $scope.firm); $scope.clearAttorney(); saveAttorneyPending = false;};
            if (done) {
                callback = function () { $scope.attorney.loadRecord($scope.record, $scope.firm); $scope.clearAttorney(); saveAttorneyPending = false;};
            } else {
                callback = function () { $scope.attorney.loadRecord($scope.record, $scope.firm); $scope.resetForms(); saveAttorneyPending = false;};
            }
            saveAttorneyPending = true;
            if ($scope.attorney.isNew()) {
                $scope.record = AttorneyDataSource.add($scope.attorney.putRecord(), callback);
            } else {
                $scope.record = AttorneyDataSource.update($scope.attorney.putRecord(), callback);
            }
        }
    };
}]);


ncpApp.controller('JudgesCtrl',['$scope', 'JudgeDataSource', function($scope, JudgeDataSource) {
    $scope.banner = "banner-hide";
    $scope.judge = {
        current:null,
        directory:null,
        list:null,
        record:null,
        template:{
            id:null,
            name:'',
            notes_general:'',
            created_at:'0000-00-00 00:00:00',
            updated_at:'0000-00-00 00:00:00'
        },
        loadDirectory:function (directory) {
            this.directory = directory;
            if (this.directory) {
                this.list = _.sortBy(this.directory.judges, function(item){
                    return item['name'].toLowerCase();
                });
            } else {
                this.list = null;
            }
        },
        loadRecord:function (record) {
            this.record = record;
            this.current = (this.record ? this.record.judge : null);
        },
        newRecord:function () {
            this.record = {};
            this.record.judge = angular.copy(this.template);
            this.current = this.record.judge;
        },
        isNew:function () {
            return (this.record ? (this.record.judge.id === null) : false);
        },
        putRecord:function () {
            return (this.current ? angular.toJson(this.current) : null);
        },
        addListEntry:function () {
            if (this.list && this.current) {
                var entry = {};
                entry.id = this.current.id;
                entry.name = this.current.name;
                this.list.push(entry);
                this.list = _.sortBy(this.list, function(item){
                    return item['name'].toLowerCase();
                });
            }
        },
        removeListEntry:function (id) {
            if (this.list) {
                this.list = _.reject(this.list,function(item){ return item['id'] == id; });
            }
        }
    };
    $scope.directory = JudgeDataSource.list({}, function () {
        $scope.judge.loadDirectory($scope.directory);
    });
    $scope.selectedJudges = [];
    $scope.$watch('selectedJudges[0]', function () {
        $scope.selectJudge();
    });
    var clearGridSelectionPlugin = {
        init:function (scope, grid, services) {
            scope.$on('ClearGridSelection', function (e) {
                scope.toggleSelectAll(false);
            });
        }
    };
    $scope.gridOptions = {
        data:'judge.list',
        displaySelectionCheckbox:false,
        selectWithCheckboxOnly:false,
        keepLastSelected:false,
        enableSorting: false,
        showColumnMenu: false,
        selectedItems:$scope.selectedJudges,
        multiSelect:false,
        columnDefs:[
            {field:'id', displayName:'ID', width:'50px'},
            {field:'name', displayName:'Name'}
        ],
        plugins:[clearGridSelectionPlugin]
    };
    $scope.showJudge = function () {
        return (typeof($scope.judge.current) !== 'undefined' && $scope.judge.current !== null);
    };
    $scope.showDetails = function(){
        $scope.$emit('HideHeader');
        $scope.banner = "banner-show";
    };
    $scope.hideDetails = function(){
        $scope.$emit('ShowHeader');
        $scope.banner = "banner-hide";
    };
    $scope.newJudge = function () {
        $scope.$broadcast('ClearGridSelection');
        $scope.judge.newRecord();
        $scope.showDetails();
    };
    $scope.selectJudge = function () {
        if ($scope.selectedJudges[0]) {
            $scope.showDetails();
            $scope.record = JudgeDataSource.query({object:$scope.selectedJudges[0].id}, function () {
                $scope.judge.loadRecord($scope.record);
            });
        }
    };
    $scope.clearJudge = function() {
        $scope.$broadcast('ClearGridSelection');
        $scope.judge.current = null;
        $scope.hideDetails();
    };
    $scope.deleteJudge = function () {
        $scope.status = JudgeDataSource.delete($scope.judge.putRecord(), function () {
            $scope.judge.removeListEntry($scope.selectedJudges[0].id);
            $scope.clearJudge();
        });
    };
    $scope.saveJudge = function () {
        if ($scope.judge.isNew()) {
            $scope.record = JudgeDataSource.add($scope.judge.putRecord(), function () {
                $scope.judge.loadRecord($scope.record);
                $scope.judge.addListEntry();
                $scope.clearJudge();
            });
        } else {
            $scope.record = JudgeDataSource.update($scope.judge.putRecord(), function () {
                $scope.judge.loadRecord($scope.record);
                // First remove the original item, then add the updated item.
                $scope.judge.removeListEntry($scope.selectedJudges[0].id);
                $scope.judge.addListEntry();
                $scope.clearJudge();
            });
        }
    };
}]);


ncpApp.controller('ReferrersCtrl',['$scope', 'ReferrerDataSource', function($scope, ReferrerDataSource) {
    $scope.banner = "banner-hide";
    $scope.referrer = {
        current:null,
        directory:null,
        list:null,
        record:null,
        template:{
            id:null,
            name:'',
            notes_general:'',
            created_at:'0000-00-00 00:00:00',
            updated_at:'0000-00-00 00:00:00'
        },
        loadDirectory:function (directory) {
            this.directory = directory;
            if (this.directory) {
                this.list = _.sortBy(this.directory.referrers, function(item){
                    return item['name'].toLowerCase();
                });
            } else {
                this.list = null;
            }
        },
        loadRecord:function (record) {
            this.record = record;
            this.current = (this.record ? this.record.referrer : null);
        },
        newRecord:function () {
            this.record = {};
            this.record.referrer = angular.copy(this.template);
            this.current = this.record.referrer;
        },
        isNew:function () {
            return (this.record ? (this.record.referrer.id === null) : false);
        },
        putRecord:function () {
            return (this.current ? angular.toJson(this.current) : null);
        },
        addListEntry:function () {
            if (this.list && this.current) {
                var entry = {};
                entry.id = this.current.id;
                entry.name = this.current.name;
                this.list.push(entry);
                this.list = _.sortBy(this.list, function(item){
                    return item['name'].toLowerCase();
                });
            }
        },
        removeListEntry:function (id) {
            if (this.list) {
                this.list = _.reject(this.list,function(item){ return item['id'] == id; });
            }
        }
    };
    $scope.directory = ReferrerDataSource.list({}, function () {
        $scope.referrer.loadDirectory($scope.directory);
    });
    $scope.selectedReferrers = [];
    $scope.$watch('selectedReferrers[0]', function () {
        $scope.selectReferrer();
    });
    var clearGridSelectionPlugin = {
        init:function (scope, grid, services) {
            scope.$on('ClearGridSelection', function (e) {
                scope.toggleSelectAll(false);
            });
        }
    };
    $scope.gridOptions = {
        data:'referrer.list',
        displaySelectionCheckbox:false,
        selectWithCheckboxOnly:false,
        keepLastSelected:false,
        enableSorting: false,
        showColumnMenu: false,
        selectedItems:$scope.selectedReferrers,
        multiSelect:false,
        columnDefs:[
            {field:'id', displayName:'ID', width:'50px'},
            {field:'name', displayName:'Name'}
        ],
        plugins:[clearGridSelectionPlugin]
    };
    $scope.showReferrer = function () {
        return (typeof($scope.referrer.current) !== 'undefined' && $scope.referrer.current !== null);
    };
    $scope.showDetails = function(){
        $scope.$emit('HideHeader');
        $scope.banner = "banner-show";
    };
    $scope.hideDetails = function(){
        $scope.$emit('ShowHeader');
        $scope.banner = "banner-hide";
    };
    $scope.newReferrer = function () {
        $scope.$broadcast('ClearGridSelection');
        $scope.referrer.newRecord();
        $scope.showDetails();
    };
    $scope.selectReferrer = function () {
        if ($scope.selectedReferrers[0]) {
            $scope.showDetails();
            $scope.record = ReferrerDataSource.query({object:$scope.selectedReferrers[0].id}, function () {
                $scope.referrer.loadRecord($scope.record);
            });
        }
    };
    $scope.clearReferrer = function() {
        $scope.$broadcast('ClearGridSelection');
        $scope.referrer.current = null;
        $scope.hideDetails();
    };
    $scope.deleteReferrer = function () {
        $scope.status = ReferrerDataSource.delete($scope.referrer.putRecord(), function () {
            $scope.referrer.removeListEntry($scope.selectedReferrers[0].id);
            $scope.clearReferrer();
        });
    };
    $scope.saveReferrer = function () {
        if ($scope.referrer.isNew()) {
            $scope.record = ReferrerDataSource.add($scope.referrer.putRecord(), function () {
                $scope.referrer.loadRecord($scope.record);
                $scope.referrer.addListEntry();
                $scope.clearReferrer();
            });
        } else {
            $scope.record = ReferrerDataSource.update($scope.referrer.putRecord(), function () {
                $scope.referrer.loadRecord($scope.record);
                // First remove the original item, then add the updated item.
                $scope.referrer.removeListEntry($scope.selectedReferrers[0].id);
                $scope.referrer.addListEntry();
                $scope.clearReferrer();
            });
        }
    };
}]);


ncpApp.controller('UsersCtrl',['$scope','UserDataSource',function($scope, UserDataSource) {
    $scope.banner = "banner-hide";
    $scope.user = {
        current:null,
        directory:null,
        list:null,
        record:null,
        roles:['Administrator', 'Editor', 'User'],
        template:{
            id:null,
            username:'',
            password:'Secret',
            role:"user",
            active:'Yes',
            created_at:'0000-00-00 00:00:00',
            updated_at:'0000-00-00 00:00:00'
        },
        setPassword: function (password1, password2) {
            if ((password1 && password2) && (password1.localeCompare(password2) === 0)) {
                this.current.password = angular.copy(password1);
            }
        },
        loadDirectory:function (directory) {
            this.directory = directory;
            if (this.directory) {
                this.list = _.sortBy(this.directory.users, function(item){
                    return item['name'].toLowerCase();
                });
            } else {
                this.list = null;
            }
        },
        loadRecord:function (record) {
            this.record = record;
            this.current = (this.record ? this.record.user : null);
            if (this.current) {
                this.current.password = null;
            }
        },
        newRecord:function () {
            this.record = {};
            this.record.user = angular.copy(this.template);
            this.current = this.record.user;
            this.current.password = null;
        },
        isNew:function () {
            return (this.record ? (this.record.user.id === null) : false);
        },
        putRecord:function () {
            return (this.current ? angular.toJson(this.current) : null);
        },
        addListEntry:function () {
            if (this.list && this.current) {
                var entry = {};
                entry.id = this.current.id;
                entry.name = this.current.username;
                this.list.push(entry);
                this.list = _.sortBy(this.list, function(item){
                    return item['name'].toLowerCase();
                });
            }
        },
        removeListEntry:function (id) {
            if (this.list) {
                this.list = _.reject(this.list,function(item){ return item['id'] == id; });
            }
        }
    };
    $scope.togglePassword = function() {
        $scope.enablePassword(!$scope.editPassword);
    };
    $scope.enablePassword = function (enable) {
        $scope.editPassword = enable;
        if ($scope.editPassword) {
            $scope.passwordAction = "Lock";
            $scope.passwordIcon = "glyphicon glyphicon-lock";
        } else {
            $scope.passwordAction = "Edit";
            $scope.passwordIcon = "glyphicon glyphicon-edit";
        }
    };
    $scope.resetPassword = function() {
        $scope.password1 = null;
        $scope.password2 = null;
    };
    $scope.resetPassword();
    $scope.enablePassword(false);
    $scope.directory = UserDataSource.list({}, function () {
        $scope.user.loadDirectory($scope.directory);
    });
    $scope.selectedUsers = [];
    $scope.$watch('selectedUsers[0]', function () {
        $scope.selectUser();
    });
    var clearGridSelectionPlugin = {
        init:function (scope, grid, services) {
            scope.$on('ClearGridSelection', function (e) {
                scope.toggleSelectAll(false);
            });
        }
    };
    $scope.gridOptions = {
        data:'user.list',
        displaySelectionCheckbox:false,
        selectWithCheckboxOnly:false,
        keepLastSelected:false,
        enableSorting: false,
        showColumnMenu: false,
        selectedItems:$scope.selectedUsers,
        multiSelect:false,
        columnDefs:[
            {field:'id', displayName:'ID', width:'50px'},
            {field:'name', displayName:'Name'}
        ],
        plugins:[clearGridSelectionPlugin]
    };
    $scope.showUser = function () {
        return (typeof($scope.user.current) !== 'undefined' && $scope.user.current !== null);
    };
    $scope.showDetails = function(){
        $scope.$emit('HideHeader');
        $scope.banner = "banner-show";
    };
    $scope.hideDetails = function(){
        $scope.$emit('ShowHeader');
        $scope.banner = "banner-hide";
    };
    $scope.newUser = function () {
        $scope.$broadcast('ClearGridSelection');
        $scope.user.newRecord();
        $scope.showDetails();
        $scope.enablePassword(true);
    };
    $scope.selectUser = function () {
        if ($scope.selectedUsers[0]) {
            $scope.showDetails();
            $scope.record = UserDataSource.query({object:$scope.selectedUsers[0].id}, function () {
                $scope.user.loadRecord($scope.record);
            });
        }
    };
    $scope.clearUser = function () {
        $scope.$broadcast('ClearGridSelection');
        $scope.user.current = null;
        $scope.hideDetails();
        $scope.resetPassword();
        $scope.enablePassword(false);
    };
    $scope.deleteUser = function () {
        $scope.status = UserDataSource.delete($scope.user.putRecord(), function () {
            $scope.user.removeListEntry($scope.selectedUsers[0].id);
            $scope.clearUser();
        });
    };
    $scope.saveUser = function () {
        $scope.user.setPassword($scope.password1,$scope.password2);
        if ($scope.user.isNew()) {
            $scope.record = UserDataSource.add($scope.user.putRecord(), function () {
                $scope.user.loadRecord($scope.record);
                $scope.user.addListEntry();
                $scope.clearUser();
            });
        } else {
            $scope.record = UserDataSource.update($scope.user.putRecord(), function () {
                $scope.user.loadRecord($scope.record);
                // First remove the original item, then add the updated item.
                $scope.user.removeListEntry($scope.selectedUsers[0].id);
                $scope.user.addListEntry();
                $scope.clearUser();
            });
        }
    };
}]);


ncpApp.controller('SummaryCtrl',['$scope', '$location', '$filter', 'ReportDataSource', 'ReportDataSourceOld', 'ReportEmail', 'ZoneInfo', function($scope, $location, $filter, ReportDataSource, ReportDataSourceOld, ReportEmail, ZoneInfo) {
    $scope.ready = false;
    $scope.optionsDisplay = 'table';
    $scope.chartData = null;
    $scope.end_date = new Date();
    $scope.end_date_income = new Date();
    $scope.end_date_age = new Date();
    var year = $scope.end_date.getFullYear();
    $scope.start_date = new Date(year,0,1,0,0,0);
    $scope.start_date_income = new Date(year,0,1,0,0,0);
    $scope.start_date_age = new Date(year,0,1,0,0,0);
    $scope.emails = [
        {address:'msilverman@transgenderlegal.org',name:'Michael Silverman'},
        {address:'pharrington@transgenderlegal.org',name:'Patricia Harrington'},
        {address:'namechange@transgenderlegal.org',name:'Intake Coordinator'}
    ];
    $scope.emailInProgress = false;
    $scope.emailAddress = ' ';
    $scope.getZones = function() {
        $scope.zoneList = ZoneInfo.zoneList({}, function () {
            $scope.zones = $scope.zoneList.zones;
        });
    };
    $scope.getZones();
    $scope.zone_name = 'All';
    $scope.age_bins = '17,18,25,30,40,50,60';
    $scope.income_bins = '10000,10001,20000,30000,40000,50000,60000';
    $scope.data = ReportDataSourceOld.summary({}, function() {
        $scope.setActive('summary');
        $location.path("/summary");
        $scope.summary = $scope.data.summary;
        $scope.chartData = $scope.setChartData();
        $scope.ready = true;
    });
    $scope.active = {
        summary: '',
        by_county: '',
        by_firm: '',
        by_referrer: '',
        by_interviewer: '',
        by_gender: ''
    };
    $scope.now_active = null;
    $scope.setActive = function (new_active) {
        if ($scope.now_active) {
            $scope.active[$scope.now_active] = '';
        }
        $scope.active[new_active] = 'active';
        $scope.now_active = new_active;
        $scope.optionsDisplay = 'table';
    };
    $scope.$on('$routeChangeSuccess', function(){
        var path = $location.path();
        $scope.setActive(path.substr(1));
    });
    $scope.setChartData = function() {
        var chartData = {};
        if ($scope.summary) {
            chartData.summary = null;
            chartData.by_firm = $scope.getDataObj($scope.summary.firms,
                'name', 'total_clients', '' );
            chartData.by_county = $scope.getDataObj($scope.summary.counties,
                'county', 'total_clients', 'state' );
//            chartData.by_referrer = $scope.getDataObj($scope.summary.referrers,
//                'name', 'total_clients', '' );
            chartData.by_referrer = null;
//            chartData.by_interviewer = $scope.getDataObj($scope.summary.interviewers,
//                'name', 'total_clients','' );
            chartData.by_inerviewer = null;
            chartData.by_gender = $scope.getDataObj($scope.summary.gender_transitions,
                 'gender_transition', 'total_clients','' );
        }
        return chartData;
    };
    $scope.dateOptions = {
        changeYear: true,
        changeMonth: true,
        yearRange: '2000:-0'
    };
    $scope.getDataObj = function(tableData, rowLabel, rowValue, tagLabel) {
        var dataObj = {};
        dataObj._type = "terms";
        dataObj.missing = 0;
        dataObj.total = 0;
        dataObj.other = 0;
        dataObj.terms = [];
        angular.forEach(tableData, function(value, key) {
            if (value[rowLabel] !== 'No Entry') {
                var item = {};
                item.term = (tagLabel === '') ? value[rowLabel] : value[rowLabel].concat(' (',value[tagLabel],')');
                item.count = value[rowValue];
                dataObj.terms.push(item);
                dataObj.total += item.count;
            }
        });
        return dataObj;
    };
    $scope.ageReport = function(start_date,end_date,zone_name) {
        var from_date = $filter('date')(start_date, 'yyyy-MM-dd');
        var to_date = $filter('date')(end_date,'yyyy-MM-dd');
        $scope.ageData = ReportDataSource.ages({'age_bins':'17,18,25,30,40,50,60','start-date':from_date,'end-date':to_date,'zone-name':zone_name},function(){

        });
    };
    $scope.incomeReport = function(start_date,end_date,zone_name) {
        var from_date = $filter('date')(start_date, 'yyyy-MM-dd');
        var to_date = $filter('date')(end_date,'yyyy-MM-dd');
        $scope.incomeData = ReportDataSource.incomes({'income_bins':'10000,10001,20000,30000,40000,50000,60000','start-date':from_date,'end-date':to_date,'zone-name':zone_name},function(){

        });
    };
    $scope.ethnicityReport = function(start_date,end_date,zone_name) {
        var from_date = $filter('date')(start_date, 'yyyy-MM-dd');
        var to_date = $filter('date')(end_date,'yyyy-MM-dd');
        $scope.ethnicityData = ReportDataSource.ethnicity({'start-date':from_date,'end-date':to_date,'zone-name':zone_name},function(){

        });
    };
    $scope.educationReport = function(start_date,end_date,zone_name) {
        var from_date = $filter('date')(start_date, 'yyyy-MM-dd');
        var to_date = $filter('date')(end_date,'yyyy-MM-dd');
        $scope.educationData = ReportDataSource.education({'start-date':from_date,'end-date':to_date,'zone-name':zone_name},function(){

        });
    };
    $scope.publicAssistanceReport = function(start_date,end_date,zone_name) {
        var from_date = $filter('date')(start_date, 'yyyy-MM-dd');
        var to_date = $filter('date')(end_date,'yyyy-MM-dd');
        $scope.publicAssistanceData = ReportDataSource.public_assistance({'start-date':from_date,'end-date':to_date,'zone-name':zone_name},function(){

        });
    };
    $scope.medicaidReport = function(start_date,end_date,zone_name) {
        var from_date = $filter('date')(start_date, 'yyyy-MM-dd');
        var to_date = $filter('date')(end_date,'yyyy-MM-dd');
        $scope.medicaidData = ReportDataSource.medicaid({'start-date':from_date,'end-date':to_date,'zone-name':zone_name},function(){

        });
    };
    $scope.sexAssignedAtBirthReport = function(start_date,end_date,zone_name) {
        var from_date = $filter('date')(start_date, 'yyyy-MM-dd');
        var to_date = $filter('date')(end_date,'yyyy-MM-dd');
        $scope.sexAssignedAtBirthData = ReportDataSource.sex_assigned_at_birth({'start-date':from_date,'end-date':to_date,'zone-name':zone_name},function(){

        });
    };
    $scope.demographicsReport = function(start_date,end_date,zone_name) {
        var from_date = $filter('date')(start_date, 'yyyy-MM-dd');
        var to_date = $filter('date')(end_date,'yyyy-MM-dd');
        $scope.demographicsData = ReportDataSource.demographics(
            {
                'start-date':from_date,
                'end-date':to_date,
                'zone-name':zone_name,
                'age_bins':'17,18,25,30,40,50,60',
                'income_bins':'10000,10001,20000,30000,40000,50000,60000'
            },
            function(){}
        );
    };
    $scope.emailReport = function(emailAddress) {
        var from_date = $filter('date')($scope.start_date, 'yyyy-MM-dd');
        var to_date = $filter('date')($scope.end_date,'yyyy-MM-dd');
        var mail_to_name = 'Test';
        var mail_to_address = emailAddress;
        var item = _.findWhere($scope.emails, {address: mail_to_address});
        if (item) {
            mail_to_name = item.name;
        }
        if (! $scope.emailInProgress ) {
            $scope.emailInProgress = true;
            $scope.emailStatus = ReportEmail.status({object1:from_date,object2:to_date,object3:mail_to_address,object4:mail_to_name}, function() {
                $scope.emailInProgress = false;
            });
        }
    };
}]);

ncpApp.controller('ConfirmCloseCtrl',['$scope', '$modalInstance', function($scope, $modalInstance) {

    $scope.close = function () {
        $modalInstance.close('close');
    };

    $scope.save = function () {
        $modalInstance.close('save');
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);


ncpApp.controller('ConfirmDeleteCtrl',['$scope', '$modalInstance', function($scope, $modalInstance) {

    $scope.delete = function () {
        $modalInstance.close('delete');
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}]);