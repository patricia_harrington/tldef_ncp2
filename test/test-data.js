// add_users
{ "userdata" :
    [
        {
            "username":"moe", "password":"stooge", "email":"moe@columbia.com", "role":"1"
        },
        {
            "username":"larry", "password":"stooge", "email":"moe@columbia.com", "role":"1"
        },
        {
            "username":"curly", "password":"stooge", "email":"curly@columbia.com", "role":"1"
        }
    ]
}

// get_users
{ "all": true }
{ "ids": ["3"] }

// update_users
{"userdata":
    [
        {
            "id": "3", "email": "larry@columbia.com", "role": "1"
        }
    ]}

// remove_users
{ "ids": ["2","3","4"] }

